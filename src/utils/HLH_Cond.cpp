/*******************************************************************************
 *    File Name : HLH_Cond.cpp
 * 
 *       Author : Henry He
 * Created Time : 2009-10-8 11:46:29
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "utils/HLH_Cond.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/







/******************************************************************************
 * Desc : Member Functions
 ******************************************************************************/




/******************************************************************************
 * Desc : Constructor / Deconstructor
 ******************************************************************************/




/******************************************************************************
 * Func : HLH_Cond
 * Desc : Constructor of HLH_Cond
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_Cond::HLH_Cond ()
{
  // Notify that this object not initialized
  m_bInited = false;
}



/******************************************************************************
 * Func : ~HLH_Cond
 * Desc : Deconstructor of HLH_Cond
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_Cond::~ HLH_Cond ()
{
  Destroy ();
}




/******************************************************************************
 * Desc : Operations
 ******************************************************************************/



/******************************************************************************
 * Func : Init
 * Desc : Initialize this condition
 * Args : NONE
 * Outs : if success, return 0, otherwise error code will be return
 ******************************************************************************/
int HLH_Cond::Init ()
{

  int ret;

  // Check if already initialized
  if (m_bInited) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("already inited") );
    return HLH_COND_ERR_ALREADY_INITED;
  }

  // Initialize this condition
  ret = pthread_cond_init (&m_cCond, NULL);
  if (ret < 0) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("can't init") );
    ret = HLH_COND_ERR_CANT_INIT;
    goto fail;
  }

  // Notify that this condition is initialized
  m_bInited = true;
  return 0; 

fail:
  return ret;

}



/******************************************************************************
 * Func : HLH_Cond::Destory
 * Desc : Destory this condition
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void HLH_Cond::Destroy ()
{

  if (m_bInited) {
    // Destory this condition
    pthread_cond_destroy (&m_cCond);
    // Notify that this condition not initialized
    m_bInited = false;
  }

}


/******************************************************************************
 * Func : Wait
 * Desc : Wait for this condition, called with zhmMutex locked
 * Args : zhmMutex          Mutex for this condition
 * Outs : If success, return 0, otherwise return error code
 ******************************************************************************/
int HLH_Cond::Wait (HLH_Mutex &zhmMutex)
{
  int nRetVal;

  if (!m_bInited)
    return HLH_COND_ERR_NOT_INIT;

  // Wait for this condition
  nRetVal = pthread_cond_wait ( &m_cCond, &zhmMutex.m_mMutex );
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("pthread_cond_wait failed") );
    return HLH_COND_ERR_FAILED;
  }

  return 0;
}


/******************************************************************************
 * Func : TimeWait
 * Desc : Wait for condition until time reach, called with zhmMutex locked
 * Args : zhmMutex          Mutex for this condition
 *        zhtTime           Max time will be waited
 * Outs : If success, return 0; otherwise return error code
 ******************************************************************************/
int HLH_Cond::TimeWait (HLH_Mutex &zhmMutex, const HLH_Time &zhtTime)
{
  int             nRetVal;
  struct timespec tsWait;
	HLH_Time				zhtAbs;

  if (!m_bInited) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("not init") );
    return HLH_COND_ERR_NOT_INIT;
  }

  // Set wait time
	zhtAbs = HLH_Time::GetCurrentTime () + zhtTime;
  tsWait.tv_sec  = (time_t) zhtAbs.GetSeconds ();
  tsWait.tv_nsec = (time_t) (zhtAbs.GetMicroSeconds () * 1000);

  // Wait until timeout
  nRetVal = pthread_cond_timedwait (&m_cCond, &zhmMutex.m_mMutex, &tsWait);
  if (nRetVal != 0) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("pthread_cond_timedwait failed") );
    return HLH_COND_ERR_FAILED;
  }

  if (tsWait.tv_sec == 0 && tsWait.tv_nsec == 0) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("timeout") );
  }

  return 0;
}


/******************************************************************************
 * Func : Signal
 * Desc : Signal one waiter that the condition has reached
 * Args : NONE
 * Outs : If success, return 0, otherwise return error code
 ******************************************************************************/
int HLH_Cond::Signal ()
{
  int nRetVal;

  // Check if initialized
  if (!m_bInited) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("not init") );
    return HLH_COND_ERR_NOT_INIT;
  }

  // Signal this condition
  nRetVal = pthread_cond_signal (&m_cCond);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("pthread_cond_signal failed") );
    return HLH_COND_ERR_FAILED;
  }

  return 0;
}

/******************************************************************************
 * Func : Broadcast
 * Desc : Broadcast to all waiter that the condition has reached
 * Args : NONE
 * Outs : If success, return 0, otherwise return error code
 ******************************************************************************/
int HLH_Cond::Broadcast ()
{
  int nRetVal;

  // Check if initialized
  if (!m_bInited) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("not init") );
    return HLH_COND_ERR_NOT_INIT;
  }

  // Broadcast this condition
  nRetVal = pthread_cond_broadcast (&m_cCond);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("pthread_cond_broadcast failed") );
    return HLH_COND_ERR_FAILED;
  }

  return 0;
}








