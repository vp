/*************************************************************************
 *         File : typedef.h
 * 
 *       Author : Henry He
 *      Created : 2009-9-17 16:04:46
 *  Description : 
 ************************************************************************/

#ifndef __TYPEDEF_INC_20090917_160446_HENRY__
#define __TYPEDEF_INC_20090917_160446_HENRY__

/******************************************************************************
 * Desc : basic types one
 ******************************************************************************/
typedef signed char         I8;
typedef signed short        I16;
typedef signed int          I32;
typedef signed long int     I64;

typedef unsigned char       U8;
typedef unsigned short      U16;
typedef unsigned int        U32;
typedef unsigned long int   U64;


/******************************************************************************
 * Desc : basic types two
 ******************************************************************************/
typedef signed char         INT8;
typedef signed short        INT16;
typedef signed int          INT32;
typedef signed long int     INT64;

typedef unsigned char       UINT8;
typedef unsigned short      UINT16;
typedef unsigned int        UINT32;
typedef unsigned long int   UINT64;


/******************************************************************************
 * Desc : Included Files
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

#include <pthread.h>

#include <sys/time.h>
#include <time.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <list>

#include <sys/soundcard.h>
#include <stropts.h>



/******************************************************************************
 * Desc : Debug
 ******************************************************************************/
#include "utils/config.h"
#include "utils/debug.h"



#endif /* __TYPEDEF_INC_20090917_160446_HENRY__ */
