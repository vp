/*******************************************************************************
 *    File Name : HLH_Thread.cpp
 * 
 *       Author : Henry He
 * Created Time : 2009-10-8 11:18:18
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/

#include <sys/time.h>
#include <time.h>

#include "utils/HLH_Thread.h"



/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/





/******************************************************************************
 * Desc : Member Functions
 ******************************************************************************/




/******************************************************************************
 * Func : HLH_Thread::HLH_Thread
 * Desc : Constructor of HLH_Thread
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_Thread::HLH_Thread ()
{
  m_zhmRunMutex.Init ();
  m_zhmContinueMutex.Init ();
  m_zhmContinueMutex2.Init ();

  m_pvRetval  = NULL;
  m_bRunning  = false;
  m_bStopping = false;
}


/******************************************************************************
 * Func : HLH_Thread::~HLH_Thread
 * Desc : Deconstructor of HLH_Thread
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_Thread::~HLH_Thread ()
{
  Kill();
}


/******************************************************************************
 * Func : HLH_Thread::Start
 * Desc : Start a thread calling \c ztfThreadFunc (pvThreadParam)
 * Args : ztfThreadFunc               Function called in this thread
 * Outs : If success return 0, otherwise return error code.
 ******************************************************************************/
int HLH_Thread::Start (ThreadFunc ztfThreadFunc, void *pvThreadParam)
{
  int nRetval;

  // Force TheThread () to wait before call m_ztfThreadFunc ():
  //    TheThread () will set m_bRunning 
  //    and wait Start () to confirm before it call m_ztfThreadFunc ()
  m_zhmContinueMutex.Lock ();

  // Check if already running
  m_zhmRunMutex.Lock ();
  if (m_bRunning) {
    m_zhmRunMutex.Unlock ();
    return HLH_THREAD_ERR_ALREADY_RUNNING;
  }
  m_zhmRunMutex.Unlock ();

  // Set Thread Function
  m_ztfThreadFunc = ztfThreadFunc;
  m_pvThreadParam = pvThreadParam;

  pthread_attr_t attr;
  pthread_attr_init (&attr);
  pthread_attr_setdetachstate (&attr,PTHREAD_CREATE_DETACHED);

  nRetval = pthread_create (&m_zpThreadId, &attr, TheThread, this); 
  pthread_attr_destroy (&attr);
  if (nRetval != 0) {
    m_zhmContinueMutex.Unlock ();
    return HLH_THREAD_ERR_CANT_START_THREAD;
  }

  /* Wait until 'm_bRunning' is set */

  m_zhmRunMutex.Lock ();
  while (!m_bRunning)
  {
    m_zhmRunMutex.Unlock ();

    struct timespec tsReq;
    struct timespec tsRem;

    tsReq.tv_sec = 0;
    tsReq.tv_nsec = 1000000;
    nanosleep (&tsReq, &tsRem);

    m_zhmRunMutex.Lock();
  }
  m_zhmRunMutex.Unlock();

  // Unlock to let TheThread () continue and call m_ztfThreadFunc ()
  m_zhmContinueMutex.Unlock();

  // Make sure that m_ztfThreadFunc () has call ThreadStarted ()
  m_zhmContinueMutex2.Lock();
  m_zhmContinueMutex2.Unlock();

  return 0;
}


/******************************************************************************
 * Func : HLH_Thread::Stop
 * Desc : Stop this thread
 * Args : NONE
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int HLH_Thread::Stop ()
{
  struct timespec tsReq;
  struct timespec tsRem;


  m_zhmRunMutex.Lock ();      

  // If the thread isn't running, abort
  if (!m_bRunning)
  {
    m_zhmRunMutex.Unlock();
    return HLH_THREAD_ERR_NOT_RUNNING;
  }

  if (m_bStopping) {
    m_zhmRunMutex.Unlock();
    return HLH_THREAD_ERR_ALREADY_STOPPING;
  }

  // Send stop request to m_ztfThreadFunc ()
  m_bStopping = true;

  m_zhmRunMutex.Unlock ();

  // Wait until 'm_bRunning' is unset

  m_zhmRunMutex.Lock ();      
  while (m_bRunning)
  {
    m_zhmRunMutex.Unlock ();

    tsReq.tv_sec  = 0;
    tsReq.tv_nsec = 1000000;

    // Wait for a while
    nanosleep (&tsReq, &tsRem);

    m_zhmRunMutex.Lock ();
  }
  m_zhmRunMutex.Unlock ();

  return 0;
}

/******************************************************************************
 * Func : HLH_Thread::Kill
 * Desc : Kill this thread
 * Args : NONE
 * Outs : If success return 0, otherwise return error code.
 ******************************************************************************/
int HLH_Thread::Kill ()
{
  m_zhmRunMutex.Lock ();      

  if (!m_bRunning)
  {
    m_zhmRunMutex.Unlock();
    return HLH_THREAD_ERR_NOT_RUNNING;
  }

  pthread_cancel (m_zpThreadId);

  m_bRunning  = false;
  m_bStopping = false;
  m_pvRetval  = NULL;

  m_zhmRunMutex.Unlock();

  return 0;
}

/******************************************************************************
 * Func : HLH_Thread::IsRunning
 * Desc : Whether this thread is running
 * Args : NONE
 * Outs : If running return true, otherwise return false.
 ******************************************************************************/
bool HLH_Thread::IsRunning ()
{
  bool bRunning;

  m_zhmRunMutex.Lock();      
  bRunning = m_bRunning;
  m_zhmRunMutex.Unlock();

  return bRunning;
}


/******************************************************************************
 * Func : HLH_Thread::IsStopping
 * Desc : Whether this thread is requested to stop
 * Args : NONE
 * Outs : If requested to stop return true, otherwise return false
 ******************************************************************************/
bool HLH_Thread::IsStopping ()
{
  bool bStopping;

  m_zhmRunMutex.Lock();      
  bStopping = m_bStopping;
  m_zhmRunMutex.Unlock();

  return bStopping;
}


/******************************************************************************
 * Func : HLH_Thread::GetReturnValue
 * Desc : Get return value of this thread
 * Args : NONE
 * Outs : return return value of this thread in last run
 ******************************************************************************/
void * HLH_Thread::GetReturnValue ()
{
  void *pvVal;

  m_zhmRunMutex.Lock ();
  pvVal = m_pvRetval;
  m_zhmRunMutex.Unlock ();

  return pvVal;
}

/******************************************************************************
 * Func : HLH_Thread::TheThread
 * Desc : Thread callback function of this thread
 * Args : pvThis                  pointer to this instance
 * Outs : Always return NULL.
 ******************************************************************************/
void * HLH_Thread::TheThread (void *pvThis)
{
  HLH_Thread *pzhThread;
  void *pvRet;

  ASSERT (pvThis != NULL);
  pzhThread = (HLH_Thread *)pvThis;

  pzhThread->m_zhmContinueMutex2.Lock ();

  // Identify that this thread is running
  pzhThread->m_zhmRunMutex.Lock ();
  pzhThread->m_bRunning  = true;
  pzhThread->m_bStopping = false;
  pzhThread->m_zhmRunMutex.Unlock ();

  // Wait until Start () confirm that m_bRunning is set
  pzhThread->m_zhmContinueMutex.Lock ();
  pzhThread->m_zhmContinueMutex.Unlock ();

  // pzhThread->m_zhmContinueMutex2 will be unlock here
  pvRet = pzhThread->m_ztfThreadFunc ( *pzhThread, pzhThread->m_pvThreadParam );

  // Identify that this thread is stopped
  pzhThread->m_zhmRunMutex.Lock ();
  pzhThread->m_bRunning  = false;
  pzhThread->m_bStopping = false;
  pzhThread->m_pvRetval  = pvRet;
  pzhThread->m_zhmRunMutex.Unlock ();

  return NULL;
}

/******************************************************************************
 * Func : HLH_Thread::ThreadStarted
 * Desc : Called by m_ztfThreadFunc () to notify \c Start ()
 *            that \c m_ztfThreadFunc () started
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void HLH_Thread::ThreadStarted ()
{
  m_zhmContinueMutex2.Unlock ();
}


