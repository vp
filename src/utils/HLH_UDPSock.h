/*******************************************************************************
 *         File : HLH_UDPSock.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-10 16:35:35
 *  Description : 
 ******************************************************************************/

#ifndef __HLH_UDPSOCK_INC_20091010_163535_HENRY__
#define __HLH_UDPSOCK_INC_20091010_163535_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "utils/typedef.h"
#include "utils/HLH_Thread.h"
#include "utils/HLH_Sock.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// HLH_UDPSock Error

#define HLH_UDPSOCK_ERR_FAILED                      (-1)
#define HLH_UDPSOCK_ERR_CANT_CREATE                 (-2)
#define HLH_UDPSOCK_ERR_NOT_CREATED                 (-3)
#define HLH_UDPSOCK_ERR_ALREADY_CREATED             (-4)


////////////////////////////////////////////////////////////////////////////////
// Default Values

#define HLH_UDPSOCK_DEFAULT_POLLWAIT_SECONDS        1


////////////////////////////////////////////////////////////////////////////////
// Max Values

#define HLH_UDPSOCK_MAX_RECEIVE_LENGTH              1536


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Handler callback functions

typedef void (* OnRecvFunc) (void *pvOnRecvParam,
    void *pvBuf, UINT32 unLen,
    HLH_SockAddr &zhsSockAddr);


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Receive Event Handler

class RecvHandler
{
  public:
    bool operator == (const RecvHandler &zrhRecvHandler) const {
      return ( m_zorOnRecv == zrhRecvHandler.m_zorOnRecv
          && m_pvOnRecvParam == zrhRecvHandler.m_pvOnRecvParam );
    }

  public:
    OnRecvFunc    m_zorOnRecv;
    void         *m_pvOnRecvParam;
};


////////////////////////////////////////////////////////////////////////////////
// class to process UDP transmit

class HLH_UDPSock : public HLH_Sock
{

  public:
    // Constructor
    HLH_UDPSock ();

    // Deconstructor
    ~HLH_UDPSock ();


  public:
    // Whether this instance created
    bool IsCreated ();

    // Create UDP socket and bind it to zhsBindAddr,
    //    then create thread to handle receive event
    int Create (const HLH_SockAddr &zhsBindAddr);

    // Destroy this UDP socket
    void Destroy ();

    // Reload of HLH_Sock::Connect ()
		int Connect (const HLH_SockAddr &zhsSockAddr);

    // Get the address of peer if connected
    int GetConnectAddr (HLH_SockAddr & zhsConnectAddr);

    // Add receive handler to this socket
    void AddRecvHandler (OnRecvFunc zorOnRecv, void *pvOnRecvParam);

    // Delete receive handler from receive handler list
    void DelRecvHandler (OnRecvFunc zorOnRecv, void *pvOnRecvParam);

    // Clear the receive handler list
    void ClearRecvHandlerList ();

  private:
    // Wrapper of __RecvThreadFunc
    static void * RecvThreadFunc (HLH_Thread &zhtRecvThread, void *pvThis);

    // Thread function that call receiver handler when data received
    void __RecvThreadFunc (HLH_Thread &zhtRecvThread);

  private:
    // Mutex of this instance
    HLH_Mutex       m_zhmMutex;

    // Whether this instance created
    bool            m_bCreated;

    // Whether this UDP connected
    bool            m_bConnected;
    HLH_SockAddr    m_zhsConnectAddr;

    // Receive event handler list
    std::list <RecvHandler>     m_slRecvHandlerList;

    // Threads
    HLH_Thread      m_zhtRecvThread;

};




#endif /* __HLH_UDPSOCK_INC_20091010_163535_HENRY__ */
