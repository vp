/*******************************************************************************
 *         File : HLH_Cond.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-8 11:46:20
 *  Description : 
 ******************************************************************************/

#ifndef __HLH_COND_INC_20091008_114620_HENRY__
#define __HLH_COND_INC_20091008_114620_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "utils/typedef.h"
#include "utils/HLH_RoundU32.h"
#include "utils/HLH_Time.h"
#include "utils/HLH_Mutex.h"

/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Error Codes

#define HLH_COND_ERR_FAILED                 (-1)
#define HLH_COND_ERR_ALREADY_INITED         (-2)
#define HLH_COND_ERR_NOT_INIT               (-3)
#define HLH_COND_ERR_CANT_INIT              (-4)


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/





/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/

class HLH_Cond
{

  public:
    /******************************************************************************
     * Desc : Constructor / Deconstructor
     ******************************************************************************/
    HLH_Cond ();
    ~ HLH_Cond ();

  public:
    /******************************************************************************
     * Desc : Operations
     ******************************************************************************/

    // Init this condition
    int Init ();

    // Destroy this condition
    void Destroy ();

    // Wait for this condition
    int Wait (HLH_Mutex &zhmMutex);

    // Wait for this condition util time reach
    int TimeWait (HLH_Mutex &zhmMutex, const HLH_Time &zhtTime);

    // Signal condition is reach
    int Signal ();

    // Signal all the waiter condition is reach
    int Broadcast ();

  private:
    /******************************************************************************
     * Desc : Private Data
     ******************************************************************************/

    // Whether this instance initialized
    bool              m_bInited;

    // Internal condtion of HLH_Cond
    pthread_cond_t    m_cCond;

};






#endif /* __HLH_COND_INC_20091008_114620_HENRY__ */
