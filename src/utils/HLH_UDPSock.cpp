/*******************************************************************************
 *    File Name : HLH_UDPSock.cpp
 * 
 *       Author : Henry He
 * Created Time : 2009-10-10 16:36:17
 *  Description : 
 ******************************************************************************/




/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/

#include "utils/HLH_UDPSock.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/






/******************************************************************************
 * Desc : Member Functions
 ******************************************************************************/





/******************************************************************************
 * Func : HLH_UDPSock::HLH_UDPSock
 * Desc : Constuctor of HLH_UDPSock
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_UDPSock::HLH_UDPSock ()
{
  m_zhmMutex.Init ();
  m_bCreated = false;
}


/******************************************************************************
 * Func : HLH_UDPSock::~HLH_UDPSock
 * Desc : Deconstructor of HLH_UDPSock
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_UDPSock::~HLH_UDPSock ()
{
  Destroy ();
}





/******************************************************************************
 * Desc : Operations
 ******************************************************************************/





/******************************************************************************
 * Func : HLH_UDPSock::Create
 * Desc : Create UDP socket and bind it to zhsBindAddr,
 *        then create thread to handle receive event
 * Args : zhsBindAddr               address of bindding
 * Outs : If success return 0, otherwise return error code.
 ******************************************************************************/
int HLH_UDPSock::Create (const HLH_SockAddr &zhsBindAddr)
{

  int nRetVal;

  // Lock before process
  m_zhmMutex.Lock ();

  // Check if this instance created
  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

		HLH_DEBUG ( HLH_DEBUG_MAIN, ("already created") );
    return HLH_UDPSOCK_ERR_ALREADY_CREATED;
  }

  // Init supper class HLH_Sock first
  nRetVal = HLH_Sock::Create (HLH_SOCK_TYPE_DGRAM, zhsBindAddr, false);
  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("create HLH_Sock failed") );
    goto fail;
  }

  // Clear the receive handler list
  m_slRecvHandlerList.clear ();

  // Start receive thread
  nRetVal = m_zhtRecvThread.Start (RecvThreadFunc, this);
  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("start received thread failed") );
    goto fail;
  }

  // Notify that this instance created
  m_bCreated   = true;
  m_bConnected = false;

  // Unlock after process
  m_zhmMutex.Unlock ();

  return 0;

fail:

  // Close the socket if needed
  if ( HLH_Sock::IsCreated () ) {
    HLH_Sock::Destroy ();
  }

  // Unlock after process
  m_zhmMutex.Unlock ();

  return HLH_UDPSOCK_ERR_CANT_CREATE;
}


/******************************************************************************
 * Func : HLH_UDPSock::Destroy
 * Desc : Destroy this UDP socket
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void HLH_UDPSock::Destroy ()
{
  // Stop receive thread
  m_zhtRecvThread.Stop ();

  // Lock this instance
  m_zhmMutex.Lock ();


  // Check if this instance created
  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

    return;
  }

  // Clear receiver list
  m_slRecvHandlerList.clear ();

  // Destroy this socket
  HLH_Sock::Destroy ();

  m_bCreated = false;
  m_bConnected = false;

  // Unlock this instance
  m_zhmMutex.Unlock ();
}


/******************************************************************************
 * Func : HLH_UDPSock::IsCreated
 * Desc : Whether this instance created
 * Args : NONE
 * Outs : If success return 0, otherwise return error code.
 ******************************************************************************/
bool HLH_UDPSock::IsCreated ()
{
  bool bCreated;

  m_zhmMutex.Lock ();
  bCreated = m_bCreated;
  m_zhmMutex.Unlock ();

  return bCreated;
}




/******************************************************************************
 * Func : HLH_UDPSock::AddRecvHandler
 * Desc : Add receive handler to this socket
 * Args : zorOnRecv       Funtion to handle receive event
 *        pvOnRecvParam   Parameter to \c zorOnRecv ()
 * Outs : if success, return 0, otherwise return error code
 ******************************************************************************/
void HLH_UDPSock::AddRecvHandler (OnRecvFunc zorOnRecv, void *pvOnRecvParam)
{

  RecvHandler   zrhRecvHandler;

  // Lock before process
  m_zhmMutex.Lock ();

  // Put this handler into the list
  zrhRecvHandler.m_zorOnRecv   = zorOnRecv;
  zrhRecvHandler.m_pvOnRecvParam = pvOnRecvParam;

  m_slRecvHandlerList.push_back (zrhRecvHandler);

  // Lock after process
  m_zhmMutex.Unlock ();
}


/******************************************************************************
 * Func : HLH_UDPSock::DelRecvHandler
 * Desc : Delete receive handler from receive handler list
 * Args : zorOnRecv       Funtion to handle receive event
 *        pvOnRecvParam   Parameter to \c zorOnRecv ()
 * Outs : NONE
 ******************************************************************************/
void HLH_UDPSock::DelRecvHandler (OnRecvFunc zorOnRecv, void *pvOnRecvParam)
{

  RecvHandler   zrhRecvHandler;

  // Lock before process
  m_zhmMutex.Lock ();

  // Remove this receiver handler
  zrhRecvHandler.m_zorOnRecv   = zorOnRecv;
  zrhRecvHandler.m_pvOnRecvParam = pvOnRecvParam;

  m_slRecvHandlerList.remove (zrhRecvHandler);

  // Unlock after process
  m_zhmMutex.Unlock ();
}


/******************************************************************************
 * Func : HLH_UDPSock::ClearRecvHandlerList
 * Desc : Clear the receive handler list
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void HLH_UDPSock::ClearRecvHandlerList ()
{
  // Lock before process
  m_zhmMutex.Lock ();

  // Clear the receive handler list
  m_slRecvHandlerList.clear ();

  // Unlock after process
  m_zhmMutex.Unlock ();
}

/******************************************************************************
 * Func : HLH_UDPSock::__RecvThreadFunc
 * Desc : Thread function to call receive handler functions
 * Args : zhtRecvThread         Thread that call \c RecvThreadFunc ()
 * Outs : 
 ******************************************************************************/
void HLH_UDPSock::__RecvThreadFunc ( HLH_Thread &zhtRecvThread )
{

  int           nRetVal;

  UINT32        unLen;
  UINT32        unPollType;
  HLH_Time      zhtPollTime;

  UINT8         acRecvBuf [HLH_UDPSOCK_MAX_RECEIVE_LENGTH];
  HLH_SockAddr  zhsPeerAddr;
  HLH_SockAddr  zhsConnAddr;

  std::list<RecvHandler>::iterator slIt;

  // Signal to zhtRecvThread that this thread functions runned
  zhtRecvThread.ThreadStarted ();



  // Can't process socket not created
  if ( !IsCreated () ) {
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("not created") );
    return;
  }


  do {

    // Poll for receive events
    unPollType = HLH_SOCK_POLL_READ;
    zhtPollTime.SetTime (HLH_UDPSOCK_DEFAULT_POLLWAIT_SECONDS, 0);

    nRetVal = PollWait (unPollType, zhtPollTime);

    // If there is any data received
    if ( nRetVal > 0 ) {

      // Receive the data
      nRetVal = RecvFrom (acRecvBuf, sizeof(acRecvBuf), zhsPeerAddr);
      if (nRetVal < 0) {
        // XXX: do nothing if receive error
        goto check_stop;
      }
      unLen = nRetVal;

      // If connected, check the source ip, otherwise accept all
      nRetVal = GetConnectAddr (zhsConnAddr);
      if ( nRetVal < 0 || zhsConnAddr == zhsPeerAddr ) {
        // Call receive handler call back function
        for ( slIt = m_slRecvHandlerList.begin ();
            slIt != m_slRecvHandlerList.end (); slIt ++ ) {
          slIt->m_zorOnRecv (slIt->m_pvOnRecvParam, acRecvBuf, unLen, zhsPeerAddr);
        }
      } else {
        HLH_DEBUG ( HLH_DEBUG_MAIN, ("ignore package") );
      }

    }
		
check_stop:
    // Check if stop request
    if ( zhtRecvThread.IsStopping () ) {
      return;
    }

  } while (1);

}


/******************************************************************************
 * Func : HLH_UDPSock::RecvThreadFunc
 * Desc : Wrapper of __RecvThreadFunc
 * Args : zhtRecvThread         Thread that call this function
 * Outs : Always return NULL.
 ******************************************************************************/
void * HLH_UDPSock::RecvThreadFunc (HLH_Thread &zhtRecvThread, void *pvThis)
{
  ASSERT (pvThis != NULL);
  ( (HLH_UDPSock*) pvThis )->__RecvThreadFunc (zhtRecvThread);
  return NULL;
}


/******************************************************************************
 * Func : HLH_UDPSock::Connect
 * Desc : Reload of HLH_Sock::Connect ()
 * Args : zhsPeekAddr             address to connect
 * Outs : if success, return 0, otherwise return error code
 ******************************************************************************/
int HLH_UDPSock::Connect (const HLH_SockAddr &zhsSockAddr)
{
  int nRetVal;

  // Lock this instance
  m_zhmMutex.Lock ();

  nRetVal = HLH_Sock::Connect (zhsSockAddr);
  if (nRetVal < 0) {
    goto failed;
  }

  // Notify that this instance is created
  m_bConnected = true;
  m_zhsConnectAddr = zhsSockAddr;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

  return HLH_UDPSOCK_ERR_FAILED;
}

/******************************************************************************
 * Func : HLH_UDPSock::GetConnectAddr
 * Desc : Get the address of peer if connected
 * Args : zhsConnectAddr      Space to save the address get
 * Outs : If success return 0, otherwise return error code.
 ******************************************************************************/
int HLH_UDPSock::GetConnectAddr (HLH_SockAddr & zhsConnectAddr)
{
  // Lock this instance
  m_zhmMutex.Lock ();
  if (!m_bCreated || !m_bConnected) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_UTILS, ("not connected") );
    return HLH_UDPSOCK_ERR_NOT_CREATED;
  }

  zhsConnectAddr = m_zhsConnectAddr;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;
}


