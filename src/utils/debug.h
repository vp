/*******************************************************************************
 *         File : debug.h
 * 
 *       Author : Henry He
 *      Created : Thu 22 Oct 2009 03:29:31 PM CST
 *  Description : 
 ******************************************************************************/

#ifndef __DEBUG_INC_20091022_152931_HENRY__
#define __DEBUG_INC_20091022_152931_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include <assert.h>

#include "utils/typedef.h"
#include "utils/config.h"
#include "utils/HLH_Mutex.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


//===========================  Assertion  ======================================

#if (__USE_ASSERT__ != 0)
#   define ASSERT(cond)                                            \
  do {                                                             \
    if ( !(cond) ) {                                               \
      HLH_DEBUG (HLH_DEBUG_MAIN, ("assert [ %s ] failed", #cond)); \
      while (1);                                                   \
    }                                                              \
  } while (0)
#else
#   define ASSERT(cond)
#endif


//===========================  Debug  ==========================================


// 'info' must be surrounded by '()'

#if (__USE_HLH_DEBUG__ != 0)

# define HLH_DEBUG(flags, info)                                  \
  do {                                                           \
    if ( (flags) & HLH_DEBUG_FLAGS ) {                           \
      Printf ("%s: %s: %d: ", __FILE__, __FUNCTION__, __LINE__); \
      Printf info;                                               \
      Printf ("\n"); \
    }                                                            \
  } while (0)

#else
# define HLH_DEBUG(flags, info)
#endif

#if (__TRACE_CALL__ != 0)

# define HLH_ENTER_FUNC(flags)                                   \
  do {                                                           \
    HLH_DEBUG ( flags, ("Enter-----------------------------") ); \
  } while (0)

# define HLH_EXIT_FUNC(flags)                                    \
  do {                                                           \
    HLH_DEBUG ( flags, ("Exit -----------------------------") ); \
  } while (0)

#else

# define HLH_ENTER_FUNC(flags)
# define HLH_EXIT_FUNC(flags)

#endif


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/


/*******************************************************************************
 * Desc : Extern Variables
 ******************************************************************************/
extern HLH_AutoMutex g_zhmPrintMutex;


/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/
extern int Printf (const char * pcFormat, ...);



#endif /* __DEBUG_INC_20091022_152931_HENRY__ */
