/*******************************************************************************
 *         File : HLH_Thread.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-8 11:14:55
 *  Description : 
 ******************************************************************************/

#ifndef __HLH_THREAD_INC_20091008_111455_HENRY__
#define __HLH_THREAD_INC_20091008_111455_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "utils/typedef.h"
#include "utils/HLH_Mutex.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/
#define HLH_THREAD_ERR_FAILED                    (-1)
#define HLH_THREAD_ERR_CANT_START_THREAD         (-2)
#define HLH_THREAD_ERR_NOT_RUNNING               (-3)
#define HLH_THREAD_ERR_ALREADY_RUNNING           (-4)
#define HLH_THREAD_ERR_ALREADY_STOPPING          (-5)



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/
class HLH_Thread;
typedef void * (*ThreadFunc) (HLH_Thread &zhtThread, void *pvParam);


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/

class HLH_Thread
{

public:
  /******************************************************************************
   * Desc : Constructor / Deconstructor
   ******************************************************************************/
  HLH_Thread ();
  virtual ~HLH_Thread ();

public:
  /******************************************************************************
   * Desc : Operations
   ******************************************************************************/

  // ztfThreadFunc () should call zhtThread.ThreadStarted () after thread prepared
  //    and should handle the stop request ( zhtThread.IsStopping() == ture )
  int Start (ThreadFunc ztfThreadFunc, void *pvThreadParam);
  int Stop ();
  int Kill ();


  // Status
  bool IsRunning ();
  bool IsStopping ();
  void *GetReturnValue ();


  // Should be call by ThreadFunc () to notify that it has start
  void ThreadStarted ();

private:
  /******************************************************************************
   * Desc : Private Operations
   ******************************************************************************/
  static void * TheThread (void *pvThis);

private:
  /******************************************************************************
   * Desc : Private Data
   ******************************************************************************/

  // Thread info
  pthread_t     m_zpThreadId;


  // Functions to execute during thread
  ThreadFunc    m_ztfThreadFunc;

  // Parameter for m_ztfThreadFunc ()
  void         *m_pvThreadParam;


  // Return value of \c m_ztfThreadFunc ()
  void         *m_pvRetval;

  // Whether this thread is running
  bool          m_bRunning;

  // Whether stop is requested
  bool          m_bStopping;

  // Mutexs
  HLH_Mutex     m_zhmRunMutex;
  HLH_Mutex     m_zhmContinueMutex;
  HLH_Mutex     m_zhmContinueMutex2;

};




#endif /* __HLH_THREAD_INC_20091008_111455_HENRY__ */
