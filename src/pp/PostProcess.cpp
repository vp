/*******************************************************************************
 *    File Name : src/pp/PostProcess.cpp
 * 
 *       Author : Henry He
 * Created Time : Fri 30 Oct 2009 01:03:05 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  <poll.h>

#include  "pp/PostProcess.h"



/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/




// ====================  LIFECYCLE    ========================================= 




/******************************************************************************
 * Func : PostProcessor::PostProcessor
 * Desc : Constructor of PostProcessor
 * Args : 
 * Outs : 
 ******************************************************************************/
PostProcessor::PostProcessor ()
{
  m_zhmMutex.Init ();
  m_bCreated  = false;
}   // -----  end of method PostProcessor::PostProcessor  (constructor)  ----- 


/******************************************************************************
 * Func : PostProcessor::~PostProcessor
 * Desc :  Deconstructor of PostProcessor
 * Args : 
 * Outs : 
 ******************************************************************************/
PostProcessor::~PostProcessor ()
{
  Destroy ();
  m_zhmMutex.Unlock ();
}   // -----  end of method PostProcessor::~PostProcessor  (deconstructor)  ----- 





// ====================  OPERATIONS   ========================================= 




/******************************************************************************
 * Func : PostProcessor::Create
 * Desc : Create instance of PostProcessor
 * Args : pcDevPath             Path of pp device
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int PostProcessor::Create (const char *pcDevPath)
{

  ASSERT (pcDevPath != NULL);

  // Lock this instance
  m_zhmMutex.Init ();

  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_PP|HLH_DEBUG_MAIN, ("already created") );
    return POST_PROCESSOR_ERR_ALREADY_CREATED;
  }

  // Open post process device
  m_fdPP = open ( pcDevPath, O_RDWR|O_NONBLOCK );
  if (m_fdPP < 0) {
    HLH_DEBUG ( HLH_DEBUG_PP|HLH_DEBUG_MAIN, ("can't open %s", pcDevPath) );
    goto failed;
  }

  // Notify that this instance is created
  m_bCreated = true;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

  return POST_PROCESSOR_ERR_FAILED;

}


/******************************************************************************
 * Func : PostProcessor::Destroy
 * Desc : Destroy this instance
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void PostProcessor::Destroy ()
{

  // Lock this instance
  m_zhmMutex.Lock ();

  if(!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

    HLH_DEBUG ( HLH_DEBUG_PP, ("not created") );
    return;
  }

  // Close post process device
  close (m_fdPP);
  m_fdPP = -1;

  // Notify this instance not created or set parameter
  m_bSetParam = false;
  m_bCreated = false;

  // Unlock this instance
  m_zhmMutex.Unlock ();

}





/******************************************************************************
 * Func : PostProcessor::SetParam
 * Desc : Set most parameter of /dev/misc/s3c-pp
 * Args : unSrcWidthBuf               Width of source buffer
 *        unSrcHeightBuf              Heigth of source buffer
 *        unSrcX                      X of source buffer to convert
 *        unSrcY                      Y of source buffer to convert
 *        unSrcWidth                  Width of source buffer to convert
 *        unSrcHeight                 Height of source buffer to convert
 *        unSrcColorSpace             Color space of source buffer to convert
 *
 *        unDstWidthBuf               Width of dst buffer
 *        unDstHeightBuf              Heigth of dst buffer
 *        unDstX                      X of dst buffer to convert
 *        unDstY                      Y of dst buffer to convert
 *        unDstWidth                  Width of dst buffer to convert
 *        unDstHeight                 Height of dst buffer to convert
 *        unDstColorSpace             Color space of dst buffer to convert
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int PostProcessor::SetParam (
    UINT32 unSrcWidthBuf, UINT32 unSrcHeightBuf,
    UINT32 unSrcX, UINT32 unSrcY,
    UINT32 unSrcWidth, UINT32 unSrcHeight,
    UINT32 unSrcColorSpace,

    UINT32 unDstWidthBuf, UINT32 unDstHeightBuf,
    UINT32 unDstX, UINT32 unDstY,
    UINT32 unDstWidth, UINT32 unDstHeight,
    UINT32 unDstColorSpace )
{
  int nRetVal;

  // Lock this instance
  m_zhmMutex.Lock ();

  // Check if instance created
  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_PP|HLH_DEBUG_MAIN, ("not created") );
    return POST_PROCESSOR_ERR_NOT_CREATED;
  }

  // Set post processor configuration
  m_zspParam.src_full_width  = unSrcWidthBuf;
  m_zspParam.src_full_height = unSrcHeightBuf;
  m_zspParam.src_start_x     = unSrcX;
  m_zspParam.src_start_y     = unSrcY;
  m_zspParam.src_width       = unSrcWidth;
  m_zspParam.src_height      = unSrcHeight;
  m_zspParam.src_color_space = (s3c_color_space_t) unSrcColorSpace;
  m_zspParam.dst_full_width  = unDstWidthBuf;
  m_zspParam.dst_full_height = unDstHeightBuf;
  m_zspParam.dst_start_x     = unDstX;
  m_zspParam.dst_start_y     = unDstY;
  m_zspParam.dst_width       = unDstWidth;
  m_zspParam.dst_height      = unDstHeight;
  m_zspParam.dst_color_space = (s3c_color_space_t) unDstColorSpace;
  m_zspParam.out_path        = DMA_ONESHOT;

  nRetVal = ioctl (m_fdPP, S3C_PP_SET_PARAMS, &m_zspParam);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_PP|HLH_DEBUG_MAIN, ("can't set parameter") );
    goto failed;
  }

  // Notify that the parameter of this instance has been set.
  m_bSetParam = true;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return POST_PROCESSOR_ERR_FAILED;


}


/******************************************************************************
 * Func : PostProcessor::Process
 * Desc : Convert from pvSrcBuf to pvDstBuf (must be DMA memory space)
 * Args : 
 * Outs : 
 ******************************************************************************/
int PostProcessor::Process (void * pvSrcBuf, void *pvDstBuf)
{

  int nRetVal;
  struct pollfd zpfPoll;

  // Lock this instance
  m_zhmMutex.Lock ();

  // Check if instance created
  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

    HLH_DEBUG ( HLH_DEBUG_PP|HLH_DEBUG_MAIN, ("not created") );
    return POST_PROCESSOR_ERR_NOT_CREATED;
  }

  // Check if parameter set
  if (!m_bSetParam) {
    HLH_DEBUG ( HLH_DEBUG_PP|HLH_DEBUG_MAIN, ("process before set parameter") );
    goto failed;
  }

  // Set source address
  m_zspParam.src_buf_addr_phy = (UINT32)pvSrcBuf;
  nRetVal = ioctl (m_fdPP, S3C_PP_SET_SRC_BUF_ADDR_PHY, &m_zspParam);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_PP|HLH_DEBUG_MAIN, ("ioctl set source address failed") );
    goto failed;
  }

  m_zspParam.dst_buf_addr_phy = (UINT32)pvDstBuf;
  nRetVal = ioctl (m_fdPP, S3C_PP_SET_DST_BUF_ADDR_PHY, &m_zspParam);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_PP|HLH_DEBUG_MAIN, ("ioctl set dst address failed") );
    goto failed;
  }

  // Wait for ready ?
  zpfPoll.fd = m_fdPP;
  zpfPoll.events = POLLOUT | POLLERR;

  nRetVal = poll (&zpfPoll, 1, 30);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_PP|HLH_DEBUG_MAIN, ("poll failed") );
    goto failed;
  }

  // Start post process
  nRetVal = ioctl (m_fdPP, S3C_PP_START);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_PP|HLH_DEBUG_MAIN, ("pp start failed") );
    goto failed;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

  return POST_PROCESSOR_ERR_FAILED;

}




