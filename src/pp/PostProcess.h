/*******************************************************************************
 *         File : src/pp/PostProcess.h
 * 
 *       Author : Henry He
 *      Created : Fri 30 Oct 2009 01:03:11 PM CST
 *  Description : 
 ******************************************************************************/

#ifndef __POSTPROCESS_INC_20091030_130311_HENRY__
#define __POSTPROCESS_INC_20091030_130311_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "utils/common.h"
#include  "pp/s3c_pp.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


//===========================  Error Code  =====================================

#define POST_PROCESSOR_ERR_FAILED          (-1)
#define POST_PROCESSOR_ERR_NOT_CREATED     (-2)
#define POST_PROCESSOR_ERR_ALREADY_CREATED (-3)
#define POST_PROCESSOR_ERR_CANT_CREATE     (-4)



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/




/******************************************************************************
 * Class : PostProcessor
 * Desc  : Simple wrapper of /dev/misc/s3c-pp
 ******************************************************************************/

class PostProcessor
{

  public:

    // ====================  LIFECYCLE    ========================================= 

    // Constructor of PostProcessor
    PostProcessor ();

    // Decontructor of PostProcessor
    ~ PostProcessor ();


  public:

    // ====================  OPERATORS    ========================================= 

    // ====================  OPERATIONS   ========================================= 

    // Create instance of PostProcessor
    int Create (const char *pcDevPath);

    // Destroy this instance
    void Destroy ();

    // Set most parameter of /dev/misc/s3c-pp
    int SetParam (
        UINT32 unSrcWidthFull, UINT32 unSrcHeightFull,
        UINT32 unSrcX, UINT32 unSrcY,
        UINT32 unSrcWidth, UINT32 unSrcHeight, UINT32 unSrcColorSpace,
        UINT32 unDstWidthFull, UINT32 unDstHeightFull,
        UINT32 unDstX, UINT32 unDstY,
        UINT32 unDstWidth, UINT32 unDstHeight, UINT32 unDstColorSpace);

    // Convert and copy from pvSrcBuf to pvDstBuf (must be DMA memory space)
    int Process (void * pvSrcBuf, void *pvDstBuf);

    // ====================  ACCESS       ========================================= 


  private:

    // ====================  OPERATIONS   ========================================= 


  private:

    // ====================  MEMBER DATA  ========================================= 

    // Mutex of this instance
    HLH_Mutex                 m_zhmMutex;

    // Whether this instance created
    bool                      m_bCreated;

    // File discriptor of /dev/misc/s3c-pp
    int                       m_fdPP;

    // Whether parameter has been set
    bool                      m_bSetParam;

    // Parameter of /dev/misc/s3c-pp
    s3c_pp_params_t           m_zspParam;


};  // -----  end of class  PostProcessor  ----- 



#endif /* __POSTPROCESS_INC_20091030_130311_HENRY__ */
