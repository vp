/*******************************************************************************
 *         File : ICCard.h
 * 
 *       Author : Henry He
 *      Created : Fri 20 Nov 2009 08:31:40 AM CST
 *  Description : 
 ******************************************************************************/

#ifndef __ICCARD_INC_20091120_083140_HENRY__
#define __ICCARD_INC_20091120_083140_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/
#if !defined (__PACKED__)
#define __PACKED__    __attribute__((__packed__))
#endif


//===========================  IC Card Status  =================================

#define IC_CARD_STATUS_MI_OK             ( (UINT8)(  0) )
#define IC_CARD_STATUS_COMM_ERR          ( (UINT8)(255) )
#define IC_CARD_STATUS_MI_NOTAGERR       ( (UINT8)(- 1) )
#define IC_CARD_STATUS_CHK_FAILED        ( (UINT8)(- 1) )
#define IC_CARD_STATUS_CRC_ERR           ( (UINT8)(- 2) )
#define IC_CARD_STATUS_MIS_CHK_COMP_ERR  ( (UINT8)(- 2) )
#define IC_CARD_STATUS_MI_EMPTY          ( (UINT8)(- 3) )
#define IC_CARD_STATUS_MI_AUTH_ERR       ( (UINT8)(- 4) )
#define IC_CARD_STATUS_MI_PARITY_ERR     ( (UINT8)(- 5) )
#define IC_CARD_STATUS_MI_CODE_ERR       ( (UINT8)(- 6) )
#define IC_CARD_STATUS_MI_SENDR_ERR      ( (UINT8)(- 8) )
#define IC_CARD_STATUS_MI_KEY_ERR        ( (UINT8)(- 9) )
#define IC_CARD_STATUS_MI_NOT_AUTH_ERR   ( (UINT8)(-10) )
#define IC_CARD_STATUS_MI_BIT_COUNT_ERR  ( (UINT8)(-11) )
#define IC_CARD_STATUS_MI_BYTE_COUNT_ERR ( (UINT8)(-12) )
#define IC_CARD_STATUS_MI_TRANS_ERR      ( (UINT8)(-14) )
#define IC_CARD_STATUS_MI_WRITE_ERR      ( (UINT8)(-15) )
#define IC_CARD_STATUS_MI_INCR_ERR       ( (UINT8)(-16) )
#define IC_CARD_STATUS_MI_DECR_ERR       ( (UINT8)(-17) )
#define IC_CARD_STATUS_MI_READ_ERR       ( (UINT8)(-18) )
#define IC_CARD_STATUS_MI_UNKNOWN_CMD    ( (UINT8)(-23) )
#define IC_CARD_STATUS_MI_COLL_ERR       ( (UINT8)(-24) )
#define IC_CARD_STATUS_MI_ACCESS_TIMEOUT ( (UINT8)(-27) )
#define IC_CARD_STATUS_MI_QUIT           ( (UINT8)(-30) )




//===========================  IC Card Command  ================================

#define IC_CARD_CMD_READ_MODULE_ID       ( (UINT8)(228) )
#define IC_CARD_CMD_READ_CARD_SN         ( (UINT8)(  1) )
#define IC_CARD_CMD_LOAD_PASS            ( (UINT8)( 16) )
#define IC_CARD_CMD_READ_BLOCK           ( (UINT8)( 44) )
#define IC_CARD_CMD_WRITE_BLOCK          ( (UINT8)( 45) )
#define IC_CARD_CMD_INCR_VALUE           ( (UINT8)( 46) )
#define IC_CARD_CMD_DECR_VALUE           ( (UINT8)( 47) )
#define IC_CARD_CMD_COPY_BLOCK           ( (UINT8)( 48) )
#define IC_CARD_CMD_HALT_CARD            ( (UINT8)(  8) )



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/

// IC card command words
typedef struct __tagICCardCmd
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucCmd;
  UINT8       ucLen;
  UINT8       aucData [4];
} ICCardCmd;

// IC card repond words
typedef struct __tagICCardRespond
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucStatus;
  UINT8       ucLen;
  UINT8       aucData [4];
} ICCardRespond;




// IC card read RFID module command
typedef struct __tagICCardReadMIDCmd
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucCmd;
  UINT8       ucLen;
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardReadMIDCmd;

// IC card read RFID module repond
typedef struct __tagICCardReadMIDRespond
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucStatus;
  UINT8       ucLen;
  UINT8       aucData[2];
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardReadMIDRespond;




// IC card read card SN command
typedef struct __tagICCardReadCSNCmd
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucCmd;
  UINT8       ucLen;
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardReadCSNCmd;

// IC card read card SN repond
typedef struct __tagICCardReadCSNRespond
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucStatus;
  UINT8       ucLen;
  UINT32      unSN;
  UINT8       ucMemLen;
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardReadCSNRespond;





// IC card load pass command
typedef struct __tagICCardLoadPassCmd
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucCmd;
  UINT8       ucLen;
  UINT8       ucSection;
  UINT8       ucABSel;
  UINT8       aucAPass [6];
  UINT8       aucBPass [6];
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardLoadPassCmd;

// IC card load pass repond
typedef struct __tagICCardLoadPassRespond
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucStatus;
  UINT8       ucLen;
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardLoadPassRespond;





// IC card read block command
typedef struct __tagICCardReadBlockCmd
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucCmd;
  UINT8       ucLen;
  UINT8       ucSection;
  UINT8       ucSubSection;
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardReadBlockCmd;

// IC card read block repond
typedef struct __tagICCardReadBlockRespond
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucStatus;
  UINT8       ucLen;
  UINT8       aucData[16];
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardReadBlockRespond;



// IC card write block command
typedef struct __tagICCardWriteBlockCmd
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucCmd;
  UINT8       ucLen;
  UINT8       ucSection;
  UINT8       ucSubSection;
  UINT8       aucData[16];
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardWriteBlockCmd;

// IC card write block repond
typedef struct __tagICCardWriteBlockRespond
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucStatus;
  UINT8       ucLen;
  UINT8       ucSection;
  UINT8       ucSubSection;
  UINT8       aucData[16];
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardWriteBlockRespond;





// IC card increase value command
typedef struct __tagICCardIncValCmd
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucCmd;
  UINT8       ucLen;
  UINT8       ucSection;
  UINT8       ucSubSection;
  UINT8       aucData[4];
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardIncValCmd;

// IC card increase value repond
typedef struct __tagICCardIncValRespond
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucStatus;
  UINT8       ucLen;
  UINT8       aucData[16];
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardIncValRespond;




// IC card decrease value command
typedef struct __tagICCardDecValCmd
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucCmd;
  UINT8       ucLen;
  UINT8       ucSection;
  UINT8       ucSubSection;
  UINT8       aucData[4];
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardDecValCmd;

// IC card decrease value repond
typedef struct __tagICCardDecValRespond
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucStatus;
  UINT8       ucLen;
  UINT8       aucData[16];
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardDecValRespond;




// IC card copy block command
typedef struct __tagICCardCopyBlockCmd
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucCmd;
  UINT8       ucLen;
  UINT8       ucSection;
  UINT8       ucSrcSubSection;
  UINT8       ucDstSubSection;
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardCopyBlockCmd;

// IC card copy block repond
typedef struct __tagICCardCopyBlockRespond
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucStatus;
  UINT8       ucLen;
  UINT8       ucSection;
  UINT8       ucSubSection;
  UINT8       aucData[16];
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardCopyBlockRespond;



// IC card halt card command
typedef struct __tagICCardHaltCardCmd
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucCmd;
  UINT8       ucLen;
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardHaltCardCmd;

// IC card halt card repond
typedef struct __tagICCardHaltCardRespond
{
  UINT8       ucStart;
  UINT8       ucAddr;
  UINT8       ucStatus;
  UINT8       ucLen;
  UINT8       ucCRCHigh;
  UINT8       ucCRCLow;
  UINT8       ucEnd;
} __PACKED__ ICCardHaltCardRespond;




/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/



/******************************************************************************
 * Class : ICCard
 * Desc  : 
 ******************************************************************************/

class ICCard
{

  public:

    // ====================  LIFECYCLE    ========================================= 

    // Constructor of ICCard
    ICCard ();

    // Decontructor of ICCard
    ~ ICCard ();


  public:

    // ====================  OPERATORS    ========================================= 

    // ====================  OPERATIONS   ========================================= 

    // ====================  ACCESS       ========================================= 


  private:

    // ====================  OPERATIONS   ========================================= 


  private:

    // ====================  MEMBER DATA  ========================================= 


};  // -----  end of class  ICCard  ----- 





/******************************************************************************
 * Func : ICCard::ICCard
 * Desc : Constructor of ICCard
 * Args : 
 * Outs : 
 ******************************************************************************/
ICCard::ICCard ()
{
}   // -----  end of method ICCard::ICCard  (constructor)  ----- 


/******************************************************************************
 * Func : ICCard::~ICCard
 * Desc :  Deconstructor of ICCard
 * Args : 
 * Outs : 
 ******************************************************************************/
ICCard::~ICCard ()
{
}   // -----  end of method ICCard::~ICCard  (deconstructor)  ----- 



#endif /* __ICCARD_INC_20091120_083140_HENRY__ */
