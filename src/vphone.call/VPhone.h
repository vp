/*******************************************************************************
 *         File : VPhone.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-18 18:07:19
 *  Description : 
 ******************************************************************************/

#ifndef __VIDEOPHONE_INC_20091018_180719_HENRY__
#define __VIDEOPHONE_INC_20091018_180719_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "src/common.h"

/*******************************************************************************
 * Desc : Configure
 ******************************************************************************/

//===========================  Transfer Option==================================
#define __USE_AUDIO_RECEIVER__      0
#define __USE_AUDIO_SENDER__        0

#define __USE_VIDEO_RECEIVER__      1
#define __USE_VIDEO_SENDER__        0



//===========================  H264 Encoder Configure  =========================
#define VPHONE_H264_ENC_WIDTH            320
#define VPHONE_H264_ENC_HEIGHT           240
#define VPHONE_H264_ENC_FRAME_RATE       15
#define VPHONE_H264_ENC_BIT_RATE         400 /* kbps */
#define VPHONE_H264_ENC_GOP_NUM          15


////////////////////////////////////////////////////////////////////////////////
// Timeout of VideoPhoneCommand

#define VPHONE_TIMEOUT_RECV_COMMAND_SECS 1
#define VPHONE_TIMEOUT_SEND_COMMAND_SECS 1


////////////////////////////////////////////////////////////////////////////////
// Timeout of VPhone

#define VPHONE_TIMEOUT_RECV_CMD_SECS     1
#define VPHONE_TIMEOUT_SEND_CMD_SECS     1
#define VPHONE_TIMEOUT_LISTEN_SECS       1
#define VPHONE_TIMEOUT_ACTIVE_CMD_US     (300*1000)

#define VPHONE_TIMEOUT_POLL_CAMERA_READ  (500*1000)


////////////////////////////////////////////////////////////////////////////////
// Socket port of VPhone

#define VPHONE_PORT_LISTEN               5100
#define VPHONE_PORT_CMD                  5101
#define VPHONE_PORT_AVDATA               5102


////////////////////////////////////////////////////////////////////////////////
// Audio delay in package
#define VPHONE_AUDIO_EXPECT_PKG_DELAY    (8000/160 * 1/3)

////////////////////////////////////////////////////////////////////////////////
// Audio LudpReceiver of VPhone

#define VPHONE_ALR_PKG_NUM               (VPHONE_AUDIO_EXPECT_PKG_DELAY * 15/10)
#define VPHONE_ALR_TIMETICK_US           (10 * 1000)
#define VPHONE_ALR_TIMOUT_IN_TIMETICK    5
#define VPHONE_ALR_MAX_RETRY             3

////////////////////////////////////////////////////////////////////////////////
// Audio JitterBuffer of VPhone
#define VPHONE_AJB_PER_TIMESTAMP         1
#define VPHONE_AJB_JITTER_TIMESTAMP      \
	(VPHONE_AUDIO_EXPECT_PKG_DELAY * VPHONE_AJB_PER_TIMESTAMP * 18/10)

////////////////////////////////////////////////////////////////////////////////
// Audio LudpSender of VPhone

#define VPHONE_ALS_PKG_NUM               (VPHONE_AUDIO_EXPECT_PKG_DELAY * 20/10)



////////////////////////////////////////////////////////////////////////////////
// Video delay in package
#define VPHONE_VIDEO_PKG_DELAY_EXPECT    (25 * 1/3)


////////////////////////////////////////////////////////////////////////////////
// Video LudpReceiver of VPhone

#define VPHONE_VLR_PKG_NUM               (VPHONE_VIDEO_PKG_DELAY_EXPECT * 15/10)
#define VPHONE_VLR_TIMETICK_US           (20 * 1000)
#define VPHONE_VLR_TIMOUT_IN_TIMETICK    5
#define VPHONE_VLR_MAX_RETRY             3

////////////////////////////////////////////////////////////////////////////////
// Video JitterBuffer of VPhone
#define VPHONE_VJB_PER_TIMESTAMP         1
#define VPHONE_VJB_JITTER_TIMESTAMP      (VPHONE_VIDEO_PKG_DELAY_EXPECT * 18/10)


////////////////////////////////////////////////////////////////////////////////
// Video LudpSender of VPhone

#define VPHONE_VLS_PKG_NUM               (VPHONE_VIDEO_PKG_DELAY_EXPECT * 20/10)





/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


////////////////////////////////////////////////////////////////////////////////
// Error Code of VideoPhoneCommand

#define VPHONE_COMMAND_ERR_FAILED    (-1)

////////////////////////////////////////////////////////////////////////////////
// Error Code of VPhone

#define VPHONE_ERR_FAILED            (-1)
#define VPHONE_ERR_NOT_CREATED       (-2)
#define VPHONE_ERR_ALREADY_CREATED   (-3)
#define VPHONE_ERR_WRONG_STATE       (-4)
#define VPHONE_ERR_CANNT_CONNECT     (-5)


////////////////////////////////////////////////////////////////////////////////
// Video Phone Commands

#define VPHONE_CMD_START             0x0001
#define VPHONE_CMD_STOP              0x0002
#define VPHONE_CMD_START_ACK         0x0003
#define VPHONE_CMD_START_NACK        0x0004
#define VPHONE_CMD_STOP_ACK          0x0005
#define VPHONE_CMD_STOP_NACK         0x0006
#define VPHONE_CMD_KEEPALIVE         0x0007


////////////////////////////////////////////////////////////////////////////////
// HLH_Time from us

#define HLH_TIME_FROM_USECS(unUSecs) ( HLH_Time (unUSecs/1000000, unUSecs%1000000) )


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


////////////////////////////////////////////////////////////////////////////////
// State of VPhone
typedef enum tagVideoPhoneState
{
  VPHONE_STATE_IDLE = 0,
  VPHONE_STATE_CONNECTED,
  VPHONE_STATE_STOPPING
} VideoPhoneState ;


////////////////////////////////////////////////////////////////////////////////
// Command word of VPhone
typedef struct tagVideoPhoneCmd
{
  UINT16        m_usCmd;                  // Command word
  UINT16        m_usLen;                  // Length of \c m_aucData
  UINT8         m_aucData [16];           // Data of command
} VideoPhoneCmd;

#define VPHONE_CMD_LENGTH(pzvpCmd)           \
  (  sizeof(VideoPhoneCmd) - sizeof((pzvpCmd)->m_aucData) + (pzvpCmd)->m_usLen  )

/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Class to manager VPhone command
class VideoPhoneCommand
{

  public: 
    /******************************************************************************
     * Desc : Constructor / Deconstructor
     ******************************************************************************/
    // Constructor of VideoPhoneCommand
    VideoPhoneCommand (HLH_Sock & zhsCmdSock) {
      m_pzhsCmdSock = & zhsCmdSock;
    }

    // Deconstructor of VideoPhoneCommand
    ~ VideoPhoneCommand () {
    }

  public:
    /******************************************************************************
     * Desc : Operations
     ******************************************************************************/
    int RecvCommand (VideoPhoneCmd &zvpCmd);
    int SendCommand (VideoPhoneCmd &zvpCmd);

    int SendStartCommand ();
    int SendStartACKCommand ();
    int SendStartNACKCommand ();
    int SendKeepAliveCommand ();
    int SendStopCommand ();
    int SendStopACKCommand ();
    int SendStopNACKCommand ();

  private:
    /******************************************************************************
     * Desc : Private Data
     ******************************************************************************/
    HLH_Sock      * m_pzhsCmdSock;

};



////////////////////////////////////////////////////////////////////////////////
// Class to manager Video Phone communication

class VPhone
{

  public: 
    /******************************************************************************
     * Desc : Constructor / Deconstructor
     ******************************************************************************/
    VPhone ();
    ~ VPhone ();


  public:
    /******************************************************************************
     * Desc : Operations
     ******************************************************************************/
    // Create a VPhone
    int Create ();

    // Destroy a VPhone
    void Destroy ();

    // Start a communication to peer of address \c zhsPeerAddr
    int Start ( HLH_SockAddr & zhsPeerAddr, UINT32 unTimestampFirst );

    // Stop the communication with peer
    void Stop ();


  private:
    /******************************************************************************
     * Desc : Private Operations
     ******************************************************************************/
    // Start a communication when start requst by peer
    int OnStart ( HLH_SockAddr zhsPeerAddr, UINT32 unTimestampFirst );

    // Stop a communication when stop request by peer
    void OnStop ();


  private:
    /******************************************************************************
     * Desc : Callbacks
     ******************************************************************************/
    // Thread to listen on server port to process connection request
    void __ListenThreadFunc (
        HLH_Thread &zhtListenThread);

    // Wrapper of __ListenThreadFunc
    static void * ListenThreadFunc (
        HLH_Thread &zhtListenThread,
        void *pvThis);

    // Thread to process commands and check for keep-alive state
    void __CmdThreadFunc (
        HLH_Thread & zhtCmdThread);

    // Wrapper of \c __CmdThreadFunc
    static void * CmdThreadFunc (
        HLH_Thread  & zhtCmdThread,
        void        * pvThis);

    // Thread to keep alive connection
    void __KeepAliveThreadFunc (
        HLH_Thread &zhtKeepAliveThread);

    // Wrapper of \c __KeepAliveThreadFunc
    static void * KeepAliveThreadFunc (
        HLH_Thread  & zhtKeepAliveThread,
        void        * pvThis );


    // Thread to listen on 
    // Callback called when audio data ready
    void __OnAudioRead (void *pvBuf, UINT32 unLen);

    // Wrapper of \c __OnAudioRead ()
    static void OnAudioRead (void *pvThis, void *pvBuf, UINT32 unLen);

    // Callback called when JitterBuffer ready
    void __OnAudioJitterBufferGet (JPackage & zjpPkg);

    // Wrapper of __OnJitterBufferGet
    static void OnAudioJitterBufferGet (void * pvThis, JPackage & zjpPkg);

    // Callback called when Ludp receive package
    void __OnAudioLudpRecv (ReceiverPackage &zrpPkg);

    // Wrapper of __OnLudpRecv ()
    static void OnAudioLudpRecv (void *pvThis, ReceiverPackage &zrpPkg);



    // Handler of packages received by video ludp receiver
    void __OnVideoLudpRecv (ReceiverPackage &zrpPkg);

    // Wrapper of __OnVideoLudpRecv ()
    static void OnVideoLudpRecv (void *pvThis, ReceiverPackage &zrpPkg);

    // Handler for jitter buffer package
    void __OnVideoJitterBufferGet (JPackage & zjpPkg);

    // Wrapper of __OnVideoJitterBufferGet
    static void OnVideoJitterBufferGet (void * pvThis, JPackage & zjpPkg);

    // Thread function to capture the video, encode and send it.
    void __CaptureThreadFunc (HLH_Thread & zhtCaptureThread);

    // Wrapper of __CaptureThreadFunc
    static void * CaptureThreadFunc (HLH_Thread &zhtCaptureThread, void *pvThis);

    // Start thread to stop session
    void StopSession ();

    // Thread function to stop session
    static void * StopThreadFunc (HLH_Thread &zhtStopThread, void *pvThis);


  private:
    /******************************************************************************
     * Desc : Private Data
     ******************************************************************************/

    ////////////////////////////////////////////////////////////////////////////////
    // Mutex of this instance
    HLH_Mutex               m_zhmMutex;

    // Whether this instance created
    bool                    m_bCreated;


    ////////////////////////////////////////////////////////////////////////////////
    // Socket listen for connection
    HLH_Sock                m_zhsListenSock;

    // Listen thread
    HLH_Thread              m_zhtListenThread;


    ////////////////////////////////////////////////////////////////////////////////
    // Socket connect to peer
    HLH_Sock                m_zhsCmdSock;

    // Command thread
    HLH_Thread              m_zhtCmdThread;

    // Keep alive thread
    HLH_Thread              m_zhtKeepAliveThread;


    // State of connection
    VideoPhoneState         m_zvpState;

    // Use ot manage command word
    VideoPhoneCommand       m_zvpCommand;


    ////////////////////////////////////////////////////////////////////////////////
    // Communication Related members



    // Socket for audio and video
    HLH_UDPSock             m_zhuAVSock;

    // Thread to stop session
    HLH_Thread              m_zhtStopThread;


    //===========================  Audio  ==========================================



    // JitterBuffer for LudpReceiver
    JitterBuffer            m_zjbAudioBuf;

    // LudpReceiver for audio
    LudpReceiver            m_zlrAudioReceiver;

    // LudpSender for audio
    LudpSender              m_zlsAudioSender;

    // Audio device
    HLH_Audio               m_zhaAudioDev;



    //===========================  Video  ==========================================



    // Frame size
    UINT32                  m_unFrameSize;

		// Frame buffer
		UINT8                   m_aucCameraBuf [VPHONE_H264_ENC_WIDTH * VPHONE_H264_ENC_HEIGHT * 3 / 2];

		// Decode buffer
		UINT8                   m_aucDecodeBuf [VPHONE_H264_ENC_WIDTH * VPHONE_H264_ENC_HEIGHT * 3 / 2];

    // Camera for video
    HLH_Video               m_zhvCamera;

    // Capture thread for camera
    HLH_Thread              m_zhtCapture;

    // H264 Encoder
    H264Encoder             m_zheEncoder;
    UINT8                 * m_pucEncInBuf;

    // LudpSender for video
    LudpSender              m_zlsVideoSender;



    // LudpReceiver for video
    LudpReceiver            m_zlrVideoReceiver;

    // JitterBuffer for video
    JitterBuffer            m_zjbVideoBuf;

    // H264 Decoder
    H264Decoder             m_zhdDecoder;
    UINT8                 * m_pucDecInBuf;

    // Post Processor
    PostProcessor           m_zppPostProcessor;

    // Framebuffer
    FrameBuffer             m_zfbFrameBuffer;


};


#endif /* __VIDEOPHONE_INC_20091018_180719_HENRY__ */
