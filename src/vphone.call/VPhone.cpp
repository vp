/*******************************************************************************
 *    File Name : VPhone.cpp
 * 
 *       Author : Henry He
 * Created Time : 2009-10-18 18:07:05
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "vphone/VPhone.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/



/******************************************************************************
 * Desc : Operations
 ******************************************************************************/




/******************************************************************************
 * Func : VideoPhoneCommand::RecvCommand
 * Desc : Receive command from socket
 * Args : zvpCmd              
 * Outs : if success return 0, otherwise return error code
 ******************************************************************************/
int VideoPhoneCommand::RecvCommand (VideoPhoneCmd &zvpCmd)
{
  int           nRetVal;
  HLH_Time      zhtWait;
  UINT32        unPollType;

  // Read fix part of command
  unPollType = HLH_SOCK_POLL_READ;
  zhtWait.SetTime (VPHONE_TIMEOUT_RECV_COMMAND_SECS, 0);
  nRetVal = m_pzhsCmdSock->PollWait (unPollType, zhtWait);
  if (nRetVal <= 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("poll fix part of command failed") );
    goto failed;
  }

  nRetVal = m_pzhsCmdSock->Recv ( &zvpCmd, sizeof(VideoPhoneCmd)-sizeof(zvpCmd.m_aucData)  );
  if ( nRetVal != sizeof(VideoPhoneCmd)-sizeof(zvpCmd.m_aucData) ) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("read fix part of command failed return %d", nRetVal) );
    goto failed;
  }

  // Read var part of command
  if (zvpCmd.m_usLen != 0) {
    unPollType = HLH_SOCK_POLL_READ;
    zhtWait.SetTime (VPHONE_TIMEOUT_RECV_COMMAND_SECS, 0);
    nRetVal = m_pzhsCmdSock->PollWait (unPollType, zhtWait);
    if (nRetVal <= 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("poll command data failed") );
      goto failed;
    }

    nRetVal = m_pzhsCmdSock->Recv ( &(zvpCmd.m_aucData), zvpCmd.m_usLen  );
    if ( nRetVal != zvpCmd.m_usLen ) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("read command data failed") );
      goto failed;
    }
  }

  return 0;

failed:
  return VPHONE_COMMAND_ERR_FAILED;

}




/******************************************************************************
 * Func : VideoPhoneCommand::SendCommand
 * Desc : Send command via socket
 * Args : zvpCmd                      Command to send
 * Outs : If success return 0, otherwise return error code.
 ******************************************************************************/
int VideoPhoneCommand::SendCommand (VideoPhoneCmd &zvpCmd)
{

  int       nRetVal;
  UINT32    unPollType;
  HLH_Time  zhtWait;

  // Poll for send
  unPollType = HLH_SOCK_POLL_WRITE;
  zhtWait.SetTime (VPHONE_TIMEOUT_SEND_COMMAND_SECS);
  nRetVal = m_pzhsCmdSock->PollWait (unPollType, zhtWait);
  if (nRetVal <= 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("poll socket for send failed") );
    goto failed;
  }

  nRetVal = m_pzhsCmdSock->Send ( &zvpCmd, VPHONE_CMD_LENGTH (&zvpCmd) );
  if (nRetVal <= 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("send command failed") );
    goto failed;
  }

  return 0;

failed:
  return VPHONE_COMMAND_ERR_FAILED;

}



/******************************************************************************
 * Func : VideoPhoneCommand::SendStartCommand
 * Desc : Send start command via socket
 * Args : NONE
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int VideoPhoneCommand::SendStartCommand ()
{

  VideoPhoneCmd     zvpCmd;

  zvpCmd.m_usCmd = VPHONE_CMD_START;
  zvpCmd.m_usLen = 0;

  return SendCommand (zvpCmd);

}



/******************************************************************************
 * Func : VideoPhoneCommand::SendStartACKCommand
 * Desc : Send start ACK command via socket
 * Args : NONE
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int VideoPhoneCommand::SendStartACKCommand ()
{

  VideoPhoneCmd     zvpCmd;

  zvpCmd.m_usCmd = VPHONE_CMD_START_ACK;
  zvpCmd.m_usLen = 0;

  return SendCommand (zvpCmd);

}



/******************************************************************************
 * Func : VideoPhoneCommand::SendStartNACKCommand
 * Desc : Send start NACK command via socket
 * Args : NONE
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int VideoPhoneCommand::SendStartNACKCommand ()
{

  VideoPhoneCmd     zvpCmd;

  zvpCmd.m_usCmd = VPHONE_CMD_START_NACK;
  zvpCmd.m_usLen = 0;

  return SendCommand (zvpCmd);

}





/******************************************************************************
 * Func : VideoPhoneCommand::SendKeepAliveCommand
 * Desc : Send keep-alive command to peer
 * Args : NONE
 * Outs : If success return true, otherwise return error code
 ******************************************************************************/
int VideoPhoneCommand::SendKeepAliveCommand ()
{

  VideoPhoneCmd     zvpCmd;

  zvpCmd.m_usCmd = VPHONE_CMD_KEEPALIVE;
  zvpCmd.m_usLen = 0;

  return SendCommand (zvpCmd);

}

/******************************************************************************
 * Func : VideoPhoneCommand::SendStopCommand
 * Desc : Send stop command via socket
 * Args : NONE
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int VideoPhoneCommand::SendStopCommand ()
{

  VideoPhoneCmd     zvpCmd;

  zvpCmd.m_usCmd = VPHONE_CMD_STOP;
  zvpCmd.m_usLen = 0;

  return SendCommand (zvpCmd);

}



/******************************************************************************
 * Func : VideoPhoneCommand::SendStopACKCommand
 * Desc : Send stop ACK command via socket
 * Args : NONE
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int VideoPhoneCommand::SendStopACKCommand ()
{

  VideoPhoneCmd     zvpCmd;

  zvpCmd.m_usCmd = VPHONE_CMD_STOP_ACK;
  zvpCmd.m_usLen = 0;

  return SendCommand (zvpCmd);

}



/******************************************************************************
 * Func : VideoPhoneCommand::SendStopNACKCommand
 * Desc : Send stop NACK command via socket
 * Args : NONE
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int VideoPhoneCommand::SendStopNACKCommand ()
{

  VideoPhoneCmd     zvpCmd;

  zvpCmd.m_usCmd = VPHONE_CMD_STOP_NACK;
  zvpCmd.m_usLen = 0;

  return SendCommand (zvpCmd);

}







/******************************************************************************
 * Desc : Constructor / Deconstructor
 ******************************************************************************/






/******************************************************************************
 * Func : VPhone::VPhone
 * Desc : Constructor of Constructor
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
  VPhone::VPhone ()
: m_zvpCommand (m_zhsCmdSock)
{
  m_zhmMutex.Init ();
  m_bCreated = false;
}


/******************************************************************************
 * Func : VPhone::~ VPhone
 * Desc : Deconstructor of VPhone
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
VPhone::~ VPhone ()
{
  Destroy ();
}





/******************************************************************************
 * Desc : Operations
 ******************************************************************************/




/******************************************************************************
 * Func : VPhone::Create
 * Desc : Create a VPhone
 * Args : zhsListenAddr                 address VPhone listen on
 * Outs : If success return 0, otherwise return error code.
 ******************************************************************************/
int VPhone::Create ()
{
  int nRetVal;

  HLH_SockAddr zhsListenAddr;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("already created") );
    return VPHONE_ERR_ALREADY_CREATED;
  }

  // Create listen socket
  zhsListenAddr.SetAddr (INADDR_ANY, VPHONE_PORT_LISTEN);
  nRetVal = m_zhsListenSock.Create (HLH_SOCK_TYPE_STREAM, zhsListenAddr);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create listen socket failed") );
    goto failed;
  }

  // Listen on that socket
  nRetVal = m_zhsListenSock.Listen ();
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("listen failed") );
    goto failed;
  }

  // Start listen thread
  nRetVal = m_zhtListenThread.Start (ListenThreadFunc, this);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("start listen thread failed") );
    goto failed;
  }

  // Notify that this instance created
  m_bCreated = true;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:

  // Destroy listen socket if failed
  if ( m_zhsListenSock.IsCreated () ) {
    m_zhsListenSock.Destroy ();
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return VPHONE_ERR_FAILED;
}


/******************************************************************************
 * Func : VPhone::Destroy
 * Desc : Destroy a VPhone
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void VPhone::Destroy ()
{

  // Stop listen thread
  m_zhtListenThread.Stop ();

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }


  // Destroy socket
  m_zhsListenSock.Destroy ();

  // Stop communication if started
  if (m_zvpState == VPHONE_STATE_CONNECTED) {
    OnStop ();
  } else {
    // Unlock this instance
    m_zhmMutex.Unlock ();
  }

}







/******************************************************************************
 * Func : VPhone::Start
 * Desc : Start a communication to peer of address \c zhsPeerAddr
 * Args : zhsPeerAddr                   Address of peer
 *        unTimestampFirst              First timestamp for sender
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int VPhone::Start (HLH_SockAddr  & zhsPeerAddr, UINT32 unTimestampFirst )
{

  int                 nRetVal;
  HLH_SockAddr        zhsCmdAddr;
  VideoPhoneCmd       zvpCmd;

  // Lock this instance
  m_zhmMutex.Lock ();

  // Can't start if not in idle
  if (m_zvpState != VPHONE_STATE_IDLE) {
    nRetVal = VPHONE_ERR_WRONG_STATE;
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("start when not in idle state") );
    goto failed;
  }

#if 1
  // Create socket for connection
  zhsCmdAddr.SetAddr (INADDR_ANY, VPHONE_PORT_CMD);
  nRetVal = m_zhsCmdSock.Create (HLH_SOCK_TYPE_STREAM, zhsCmdAddr);
  if (nRetVal < 0) {
    nRetVal = VPHONE_ERR_CANNT_CONNECT;
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create command socket failed") );
    goto failed;
  }

  // Connect to peer
  zhsPeerAddr.SetAddrPort (VPHONE_PORT_LISTEN);
  nRetVal = m_zhsCmdSock.Connect (zhsPeerAddr);
  if (nRetVal < 0) {
    nRetVal = VPHONE_ERR_CANNT_CONNECT;
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("connect to peer failed") );
    goto failed;
  }

  // Send command to start communication
  nRetVal = m_zvpCommand.SendStartCommand ();
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("send start command failed") );
    goto failed;
  }

  // Wait for communication established
  nRetVal = m_zvpCommand.RecvCommand (zvpCmd);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("receive command ack failed") );
    goto failed;
  }
  // Check if remote ACK
  if (zvpCmd.m_usCmd != VPHONE_CMD_START_ACK) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("invalid ack") );
    goto failed;
  }
#else
	(void)zvpCmd;
#endif


  // Do things to start the communication
  nRetVal = OnStart (zhsPeerAddr, unTimestampFirst);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("can't start communication") );
    goto failed;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:
#if 1
  // Destroy socket if failed
  if ( m_zhsCmdSock.IsCreated () ) {
    m_zhsCmdSock.Destroy ();
  }
#endif

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return VPHONE_ERR_FAILED;

}


/******************************************************************************
 * Func : VPhone::Stop
 * Desc : Stop the communication with peer
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void VPhone::Stop ()
{

  int nRetVal;
  VideoPhoneCmd       zvpCmd;

  // Lock this instance
  m_zhmMutex.Lock ();

  // Can't stop if not connected
  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("stop when not in connected state") );
    goto failed;
  }

#if 1
  // Send command to start communication
  nRetVal = m_zvpCommand.SendStopCommand ();
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("send stop command failed") );
    goto failed;
  }

  // Wait for stop ack
  nRetVal = m_zvpCommand.RecvCommand (zvpCmd);
  if (nRetVal < 0) {
    // XXX: ignore this
    // goto failed;
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("read stop ack failed, ignored") );
  }

  // Check if remote stop ACK
  if (zvpCmd.m_usCmd != VPHONE_CMD_STOP_ACK) {
    // XXX: ignore this
    // goto failed;
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("not stop ack, ignored") );
  }
#endif

  // Do things to stop the communication
  OnStop ();

  return;

failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

}




/******************************************************************************
 * Desc : Private Operations
 ******************************************************************************/




/******************************************************************************
 * Func : VPhone::OnStart
 * Desc : Start a communication when start requst
 *            (called with this instance locked)
 * Args : zhsPeerAddr               address of peer, its port will be modified
 *        unTimestampFirst          
 * Outs : if success return 0, otherwise return error code
 ******************************************************************************/
int VPhone::OnStart (HLH_SockAddr zhsPeerAddr, UINT32 unTimestampFirst)
{
  int               nRetVal;

  HLH_SockAddr      zhsAVAddr;

  void            * pvEncOutBuf;
  UINT32            unEncOutSize;
  void            * pvDecOutBuf;



  // Create socket connection

  zhsAVAddr.SetAddr (INADDR_ANY, VPHONE_PORT_AVDATA);
  nRetVal = m_zhuAVSock.Create (zhsAVAddr);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create socket for media failed") );
    goto failed;
  }

  zhsPeerAddr.SetAddrPort (VPHONE_PORT_AVDATA);
  nRetVal = m_zhuAVSock.Connect (zhsPeerAddr);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("connect to peer media port failed") );
    goto failed;
  }


  // Create audio communication

#if (__USE_AUDIO_RECEIVER__ > 0)
  // JitterBuffer for audio
  nRetVal = m_zjbAudioBuf.Create (
      VPHONE_AJB_PER_TIMESTAMP, VPHONE_AJB_JITTER_TIMESTAMP,
      OnAudioJitterBufferGet, this );

  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create jitters buffer for audio failed") );
    goto failed;
  }

  // LudpReceiver for audio
  nRetVal = m_zlrAudioReceiver.Create (
      m_zhuAVSock, LUDP_SLICE_BASE_TYPE_AUDIO,
      OnAudioLudpRecv, this,
      VPHONE_ALR_PKG_NUM,
      VPHONE_ALR_TIMETICK_US,
      VPHONE_ALR_TIMOUT_IN_TIMETICK,
      VPHONE_ALR_MAX_RETRY );

  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create ludp receiver for audio failed") );
    goto failed;
  }
#endif

#if (__USE_AUDIO_SENDER__ > 0)
  // LudpSender for audio
  nRetVal = m_zlsAudioSender.Create ( m_zhuAVSock,
      LUDP_SLICE_BASE_TYPE_AUDIO,
      unTimestampFirst,
      VPHONE_ALS_PKG_NUM);

  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create ludp sender for audio failed") );
    goto failed;
  }
#endif


#if (__USE_AUDIO_RECEIVER__ > 0)
  // Audio device (8bits, 8000 sample/s, read callback, 8 x (2^8) bytes buffer )
  nRetVal = m_zhaAudioDev.Create ( OnAudioRead, this, AFMT_U8, 8000, 1, 8, 8 );
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create audio device failed") );
    goto failed;
  }
#elif (__USE_AUDIO_SENDER__ > 0)
  // Audio device (8bits, 8000 sample/s, read callback, 8 x (2^8) bytes buffer )
  nRetVal = m_zhaAudioDev.Create ( NULL, NULL, AFMT_U8, 8000, 1, 8, 8 );
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create audio device failed") );
    goto failed;
  }
#endif




  ////////////////////////////////////////////////////////////////////////////////
  // Create video communication
  ////////////////////////////////////////////////////////////////////////////////





#if (__USE_VIDEO_SENDER__ > 0 || __USE_VIDEO_RECEIVER__ > 0)

  // Calculate frame size
  m_unFrameSize = VPHONE_H264_ENC_WIDTH * VPHONE_H264_ENC_HEIGHT * 3 / 2;


  //===========================  Encoder  ========================================

  // Create H264 encoder
  nRetVal = m_zheEncoder.Create (
      VPHONE_H264_ENC_WIDTH, VPHONE_H264_ENC_HEIGHT,
      VPHONE_H264_ENC_FRAME_RATE, VPHONE_H264_ENC_BIT_RATE,
      VPHONE_H264_ENC_GOP_NUM);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create H264 encoder failed") );
    goto failed;
  }


  memset (m_aucCameraBuf, 0, sizeof(m_aucCameraBuf));

  pvEncOutBuf = m_zheEncoder.Encode (m_aucCameraBuf, m_unFrameSize, unEncOutSize);
  if (pvEncOutBuf == NULL) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("encode first frame failed") );
    goto failed;
  }


  //===========================  Decoder  ========================================


  // Create H264 decoder
  nRetVal = m_zhdDecoder.Create (pvEncOutBuf, unEncOutSize);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("create H264 decoder failed") );
    goto failed;
  }

	pvDecOutBuf = m_zhdDecoder.Decode (pvEncOutBuf, unEncOutSize);
	if (pvDecOutBuf == NULL) {
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("decode first frame failed") );
    goto failed;
	}

#endif


  //===========================  Close encoder/decoder not needed  ===============

#if (__USE_VIDEO_RECEIVER__ <= 0)
  m_zhdDecoder.Destroy ();
#endif

#if (__USE_VIDEO_SENDER__ <= 0)
  m_zheEncoder.Destroy ();
#endif



  //===========================  Others for Receiver  ============================


#if (__USE_VIDEO_RECEIVER__ > 0)
  // JitterBuffer for video
  nRetVal = m_zjbVideoBuf.Create (
      VPHONE_VJB_PER_TIMESTAMP, VPHONE_VJB_JITTER_TIMESTAMP,
      OnVideoJitterBufferGet, this );

  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create jitters buffer for video failed") );
    goto failed;
  }

  // LudpReceiver for video
  nRetVal = m_zlrVideoReceiver.Create (
      m_zhuAVSock, LUDP_SLICE_BASE_TYPE_VIDEO,
      OnVideoLudpRecv, this,
      VPHONE_VLR_PKG_NUM,
      VPHONE_VLR_TIMETICK_US,
      VPHONE_VLR_TIMOUT_IN_TIMETICK,
      VPHONE_VLR_MAX_RETRY );

  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create ludp receiver for video failed") );
    goto failed;
  }

  // Frame buffer
  nRetVal = m_zfbFrameBuffer.Create ("/dev/fb0");
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create frame buffer failed") );
    goto failed;
  }

  // Post processor
  nRetVal = m_zppPostProcessor.Create ("/dev/misc/s3c-pp");
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create post processor failed") );
    goto failed;
  }

  nRetVal = m_zppPostProcessor.SetParam (
      VPHONE_H264_ENC_WIDTH, VPHONE_H264_ENC_HEIGHT,
      0, 0, VPHONE_H264_ENC_WIDTH, VPHONE_H264_ENC_HEIGHT, YC420,
      m_zfbFrameBuffer.GetVirtualWidth (), m_zfbFrameBuffer.GetVirtualHeight (),
      0, 0, VPHONE_H264_ENC_WIDTH, VPHONE_H264_ENC_HEIGHT,
      m_zfbFrameBuffer.GetColor () );
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("set parameter of post process failed") );
    goto failed;
  }

#endif



  //===========================  Others for Sender  ==============================



#if (__USE_VIDEO_SENDER__ > 0)
  // LudpSender for video
  nRetVal = m_zlsVideoSender.Create ( m_zhuAVSock,
      LUDP_SLICE_BASE_TYPE_VIDEO,
      unTimestampFirst,
      VPHONE_VLS_PKG_NUM);

  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create ludp sender for video failed") );
    goto failed;
  }

  // Camera for video
  nRetVal = m_zhvCamera.Create (HLH_VIDEO_V4L2_DEV_CODEC, V4L2_PIX_FMT_YUV411P,
      VPHONE_H264_ENC_WIDTH, VPHONE_H264_ENC_HEIGHT);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create video device failed") );
    goto failed;
  }

  // Capture thread for camera
  nRetVal = m_zhtCapture.Start (CaptureThreadFunc, this);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("start capture thread failed") );
    goto failed;
  }

#endif


  //===========================  Threads  ========================================



  // Create keep-alive thread and command process thread
  m_zhtKeepAliveThread.Start (KeepAliveThreadFunc, this);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("start keep alive thread failed") );
    goto failed;
  }

  m_zhtCmdThread.Start (CmdThreadFunc, this);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("start command process thread failed") );
    goto failed;
  }


  // Notify that this instance connected
  m_zvpState = VPHONE_STATE_CONNECTED;

  return  0;

failed:
  // Stop thread if started
  m_zhtCmdThread.Stop ();
  m_zhtKeepAliveThread.Stop ();

  // Socket connection
  m_zhuAVSock.Destroy ();

#if (__USE_AUDIO_RECEIVER__ > 0)
  // JitterBuffer for audio
  m_zjbAudioBuf.Destroy ();

  // LudpReceiver for audio
  m_zlrAudioReceiver.Destroy ();
#endif

#if (__USE_AUDIO_SENDER__ > 0)
  // LudpSender for audio
  m_zlsAudioSender.Destroy ();
#endif

#if (__USE_AUDIO_SENDER__ > 0 || __USE_AUDIO_RECEIVER__ > 0)
  // Audio device
  m_zhaAudioDev.Destroy ();
#endif

#if (__USE_VIDEO_RECEIVER__ > 0)
  m_zjbVideoBuf.Destroy ();
  m_zlrVideoReceiver.Destroy ();
#endif

#if (__USE_VIDEO_SENDER__ > 0)
  m_zlsVideoSender.Destroy ();
#endif

  return  VPHONE_ERR_FAILED;

}


/******************************************************************************
 * Func : VPhone::StopSession
 * Desc : Start thread to stop session
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void VPhone::StopSession ()
{
  int nRetVal;
  nRetVal = m_zhtStopThread.Start (StopThreadFunc, (void*)this);
}



/******************************************************************************
 * Func : VPhone::StopThreadFunc
 * Desc : Thread function to stop session
 * Args : zhtStopThread           Thread to call this function
 * Outs : always return NULL.
 ******************************************************************************/
void * VPhone::StopThreadFunc (HLH_Thread &zhtStopThread, void *pvThis)
{
  ASSERT (pvThis != NULL);

  // Notify that this thread started
  zhtStopThread.ThreadStarted ();

  // Lock this instance
  ( (VPhone*)pvThis )->m_zhmMutex.Lock ();

  // Stop session
  ( (VPhone*)pvThis )->OnStop ();

  return NULL;
}

/******************************************************************************
 * Func : VPhone::OnStop
 * Desc : Stop a communication when stop request by peer
 *            (called with this instance locked, unlock after stop)
 * Args : 
 * Outs : NONE
 ******************************************************************************/
void VPhone::OnStop ()
{
  HLH_ENTER_FUNC (HLH_DEBUG_VPHONE);

  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("already stopping") );
    return;
  }

  // Prevent others to stop again
  m_zvpState = VPHONE_STATE_STOPPING;
  m_zhmMutex.Unlock ();


  // Stop thread if started
  m_zhtCmdThread.Stop ();
  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after stop command thread") );

  m_zhtKeepAliveThread.Stop ();
  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after stop keep alive thread") );

  // Destroy the connect socket
  m_zhsCmdSock.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy command socket") );


  // Destroy the audio/video socket
  m_zhuAVSock.Destroy ();

  // Destroy audio communication

#if (__USE_AUDIO_RECEIVER__ > 0)
  // JitterBuffer for audio
  m_zjbAudioBuf.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy audio jitter buffer") );

  // LudpReceiver for audio
  m_zlrAudioReceiver.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy audio receiver") );
#endif

#if (__USE_AUDIO_SENDER__ > 0)
  // LudpSender for audio
  m_zlsAudioSender.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy audio sender") );

  // Audio device (8bits, 8000 sample/s, read callback, 8 x (2^8) bytes buffer )
  m_zhaAudioDev.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy audio device") );
#endif


#if (__USE_VIDEO_RECEIVER__ > 0)
  m_zjbVideoBuf.Destroy ();
  m_zlrVideoReceiver.Destroy ();

  m_zhdDecoder.Destroy ();
  m_zppPostProcessor.Destroy ();
  m_zfbFrameBuffer.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy video receiver") );
#endif

#if (__USE_VIDEO_SENDER__ > 0)
  m_zlsVideoSender.Destroy ();
  m_zhvCamera.Destroy ();

  m_zheEncoder.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy video sender") );
#endif

  // Notify this instance stopped
  m_zhmMutex.Lock ();
  m_zvpState = VPHONE_STATE_IDLE;
  m_zhmMutex.Unlock ();

  HLH_EXIT_FUNC (HLH_DEBUG_VPHONE);
}





/******************************************************************************
 * Desc : Callbacks
 ******************************************************************************/




/******************************************************************************
 * Func : VPhone::__ListenThreadFunc
 * Desc : Thread to listen on server port to process connection request
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void VPhone::__ListenThreadFunc (HLH_Thread &zhtListenThread)
{

  int                 nRetVal;
  UINT32              unPollType;
  HLH_SockAddr        zhsPeerAddr;
  HLH_Sock            zhsSock;
  VideoPhoneCmd       zvpCmd;
  HLH_Time            zhtTime;

  UINT32              unTimestampFirst = 0; // XXX: use rand timestamp instead

  // Notify that this thread started
  zhtListenThread.ThreadStarted ();

  // Lock this instance
  m_zhmMutex.Lock ();
  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("listen when this instance not created") );
    return;
  }
  // Unlock this instance
  m_zhmMutex.Unlock ();

  do {

    ////////////////////////////////////////////////////////////////////////////////
    // Check for connection request

    unPollType = HLH_SOCK_POLL_READ;
    zhtTime = HLH_Time (VPHONE_TIMEOUT_LISTEN_SECS, 0);
    nRetVal = m_zhsListenSock.PollWait (
        unPollType, zhtTime );
    if (nRetVal <= 0) {
      //HLH_DEBUG ( HLH_DEBUG_VPHONE, ("timeout and no connection") );
      goto check_stop;
    }

    // Connection established
    nRetVal = m_zhsListenSock.Accept (zhsPeerAddr);
    if (nRetVal == -1) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("accept peer connection failed") );
      goto check_stop;
    }

    ////////////////////////////////////////////////////////////////////////////////
    // Verify this connection

    // Lock this instance
    m_zhmMutex.Lock ();

    // Check if already connected
    if (m_zvpState == VPHONE_STATE_IDLE) {
      // Just save this connection
      nRetVal = m_zhsCmdSock.Create (nRetVal);
      if (nRetVal < 0) {
        HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create socket failed") );
        goto cmd_err;
      }
    } else {
      HLH_Sock          zhsCmdSock;
      VideoPhoneCommand zvpCommand(zhsCmdSock);

      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("connection come in but we are busy now") );

      // Create temp socket
      nRetVal = zhsCmdSock.Create (nRetVal);
      if (nRetVal < 0) {
        HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create socket failed") );
        goto cmd_err;
      }

      // Send NACK when busy
      nRetVal = zvpCommand.SendStartNACKCommand ();
      if (nRetVal < 0) {
        HLH_DEBUG ( HLH_DEBUG_VPHONE, ("send start NACK failed") );
        goto cmd_err;
      }

      // Destroy temp socket
      m_zhsCmdSock.Destroy ();

      // Unlock this instance
      m_zhmMutex.Unlock ();

      goto cmd_err;
    }

    // Receive start command from peer
    nRetVal = m_zvpCommand.RecvCommand (zvpCmd);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("receive start command failed") );
      goto cmd_err;
    }
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("cmd = %u, len = %u", zvpCmd.m_usCmd, zvpCmd.m_usLen) );

    // Check if start command
    if ( zvpCmd.m_usCmd != VPHONE_CMD_START ) {
      // Send NACK to unknown command
      m_zvpCommand.SendStartNACKCommand ();
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("not start command") );
      goto cmd_err;
    }

    // Send ACK command
    nRetVal = m_zvpCommand.SendStartACKCommand ();
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("send start ack failed") );
      goto cmd_err;
    }

    // Start the communication
    nRetVal = OnStart (zhsPeerAddr, unTimestampFirst);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("start communication failed") );
      goto cmd_err;
    }

    // Notify that connection established
    m_zvpState = VPHONE_STATE_CONNECTED;

    // Unlock this instance
    m_zhmMutex.Unlock ();

    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("connection established") );

    goto check_stop;



cmd_err:
    if (m_zvpState == VPHONE_STATE_IDLE) {
      // Destroy the socket created
      m_zhsCmdSock.Destroy ();
    }

    // Unlock this instance
    m_zhmMutex.Unlock ();



check_stop:
    // Check exit condition
    if (zhtListenThread.IsStopping ()) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("stop on request") );
      break;
    }

  } while (1);

}


/******************************************************************************
 * Func : VPhone::ListenThreadFunc
 * Desc : Wrapper of \c __ListenThreadFunc
 * Args : pvThis                    pointer of this instance
 * Outs : Always return (void*) 0.
 ******************************************************************************/
void * VPhone::ListenThreadFunc (HLH_Thread &zhtListenThread,
    void *pvThis)
{
  ASSERT (pvThis != NULL);

  ( (VPhone*) pvThis )->__ListenThreadFunc (zhtListenThread);

  return  (void *) 0;
}


/******************************************************************************
 * Func : VPhone::__CmdThreadFunc
 * Desc : Thread to process commands and keep-alive commands
 * Args : zhtCmdThread            Thread that call this function
 * Outs : NONE
 ******************************************************************************/
void VPhone::__CmdThreadFunc (
    HLH_Thread  & zhtCmdThread )
{
  int               nRetVal;
  VideoPhoneCmd     zvpCmd;

  // Notify that this thread started
  zhtCmdThread.ThreadStarted ();

  // Lock this instance
  m_zhmMutex.Lock ();
  if ( !m_bCreated || m_zvpState == VPHONE_STATE_IDLE ) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("start when this instance not connected") );
    return;
  }
  // Unlock this instance
  m_zhmMutex.Unlock ();

  // Check command and keep alive state
  do {
    // Receive Command
    nRetVal = m_zvpCommand.RecvCommand (zvpCmd);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("stop when receive command failed") );
      goto stop_connect;
    }

    switch (zvpCmd.m_usCmd) {
      case VPHONE_CMD_STOP:
        m_zvpCommand.SendStopACKCommand ();
        HLH_DEBUG ( HLH_DEBUG_VPHONE, ("stop request by peer") );
        goto stop_connect;

      case VPHONE_CMD_KEEPALIVE:
        // If we can't receive any command during a while,
        //      then we will quit this communication
        //HLH_DEBUG ( HLH_DEBUG_VPHONE, ("keep alive by peer") );
        break;

      default:
        HLH_DEBUG ( HLH_DEBUG_VPHONE, ("unknown command") );
        // In fact, any unknown command can also active this communication
        break;
    }

    // Check if stop request
    if (  zhtCmdThread.IsStopping ()  ) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("stop request by user") );
      return;
    }

  } while (1);


stop_connect:
  // Do things to stop the communication
  StopSession ();

}

/******************************************************************************
 * Func : VPhone::CmdThreadFunc
 * Desc : Wrapper of \c __CmdThreadFunc
 * Args : zhtCmdThread            Thread that call this function
 * Outs : Always return (void*) 0.
 ******************************************************************************/
void * VPhone::CmdThreadFunc (
    HLH_Thread  & zhtCmdThread,
    void        * pvThis )
{
  ASSERT (pvThis != NULL);

  ( (VPhone*) pvThis )->__CmdThreadFunc (zhtCmdThread);

  return (void*) 0;
}



/******************************************************************************
 * Func : VPhone::__KeepAliveThreadFunc
 * Desc : Thread to keep alive connection
 * Args : zhtKeepAliveThread                keep-alive thread
 * Outs : NONE
 ******************************************************************************/
void VPhone::__KeepAliveThreadFunc (
    HLH_Thread &zhtKeepAliveThread)
{
  int       nRetVal;
  HLH_Time  zhtTime;

  // Notify that this thread started
  zhtKeepAliveThread.ThreadStarted ();

  // Lock this instance
  m_zhmMutex.Lock ();
  if ( !m_bCreated || m_zvpState == VPHONE_STATE_IDLE ) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("start when this instance not connected") );
    return;
  }
  // Unlock this instance
  m_zhmMutex.Unlock ();


  // Send keep alive command frequencely
  do {

    // Send keep alive command
    //HLH_DEBUG ( HLH_DEBUG_VPHONE, ("send keep alive command") );
    nRetVal = m_zvpCommand.SendKeepAliveCommand ();
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("stop as send keep alive command failed") );
      goto stop_connect;
    }

    zhtTime = HLH_TIME_FROM_USECS (VPHONE_TIMEOUT_ACTIVE_CMD_US);
    HLH_Time::Wait (zhtTime);

    // Check for stop request
    if ( zhtKeepAliveThread.IsStopping () ) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("stop on request") );
      return;
    }
  } while (1);


stop_connect:
  // Do things to stop the communication
  StopSession ();

}


/******************************************************************************
 * Func : VPhone::KeepAliveThreadFunc
 * Desc : Wrapper of \c __KeepAliveThreadFunc
 * Args : zhtKeepAliveThread                keep-alive thread
 *        pvThis                            pointer of this instance
 * Outs : Always return 0.
 ******************************************************************************/
void * VPhone::KeepAliveThreadFunc (
    HLH_Thread  & zhtKeepAliveThread,
    void        * pvThis )
{
  ASSERT (pvThis != NULL);

  ( (VPhone *) pvThis )->__KeepAliveThreadFunc (zhtKeepAliveThread);

  return (void *) 0;
}



/******************************************************************************
 * Func : VPhone::__OnAudioRead
 * Desc : Callback called when audio data ready
 * Args : pvBuf                     pointer to audio data read
 *        unLen                     length of audio data read
 * Outs : NONE
 ******************************************************************************/
void VPhone::__OnAudioRead (void *pvBuf, UINT32 unLen)
{

  // Lock this instance
  m_zhmMutex.Lock ();
  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Send this audio data if connected
  m_zlsAudioSender.SendPackage ( (UINT8*)pvBuf, unLen );

  // Unlock this instance
  m_zhmMutex.Unlock ();

}


/******************************************************************************
 * Func : OnAudioRead
 * Desc : Wrapper of \c __OnAudioRead ()
 * Args : pvThis                    pointer to this instance
 *        pvBuf                     pointer to audio data read
 *        unLen                     length of audio data read
 * Outs : NONE
 ******************************************************************************/
void VPhone::OnAudioRead (void *pvThis, void *pvBuf, UINT32 unLen)
{
  ASSERT (pvThis != NULL);

  ( (VPhone*) pvThis )->__OnAudioRead (pvBuf, unLen);
}


/******************************************************************************
 * Func : VPhone::__OnAudioJitterBufferGet
 * Desc : Callback called when JitterBuffer ready
 * Args : zjpPkg                  package ready in JitterBuffer
 * Outs : NONE
 ******************************************************************************/
void VPhone::__OnAudioJitterBufferGet (JPackage & zjpPkg)
{
  ReceiverPackage        *pzrpPkg;
  UINT16                  usSliceIt;
  UINT16                  usSliceNum;
  Slice                 **ppzsSlices;

  // Check arguments
  pzrpPkg = (ReceiverPackage *) zjpPkg.m_pvData;
  ASSERT (pzrpPkg != NULL);
  ASSERT (pzrpPkg->usSliceNum > 0);
  ASSERT (pzrpPkg->ppzsSlices != NULL);

#if HLH_DEBUG_CONFIG_VPHONE > 0
  static UINT32           unLastTimestamp = 0;
  if (unLastTimestamp + 1 != pzrpPkg->unTimestamp) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("jump from %u to %u", unLastTimestamp, pzrpPkg->unTimestamp) );
    unLastTimestamp = pzrpPkg->unTimestamp;
  } else {
    unLastTimestamp ++;
  }
#endif


  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Write put this data into audio device
  usSliceNum = pzrpPkg->usSliceNum;
  ppzsSlices = pzrpPkg->ppzsSlices;
  for (usSliceIt = 0; usSliceIt < usSliceNum; usSliceIt ++) {
    ASSERT ( ppzsSlices [usSliceIt] );
    m_zhaAudioDev.Write ( ppzsSlices[usSliceIt]->aucData,
        ppzsSlices[usSliceIt]->usDataLen );
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

}


/******************************************************************************
 * Func : VPhone::OnAudioJitterBufferGet
 * Desc : Wrapper of __OnAudioJitterBufferGet
 * Args : pvThis                        pointer to this instance
 *        zjpPkg                        package ready in JitterBuffer
 * Outs : NONE
 ******************************************************************************/
void VPhone::OnAudioJitterBufferGet (void * pvThis, JPackage & zjpPkg)
{
  ASSERT (pvThis != NULL);

  ( (VPhone*) pvThis )->__OnAudioJitterBufferGet (zjpPkg);
}


/******************************************************************************
 * Func : FreeReceivePackage
 * Desc : Free ReceiverPackage and its contents
 * Args : pzrpPkg                 package to be free
 * Outs : NONE
 ******************************************************************************/
static void FreeReceivePackage (void *pvRecvPkg)
{
  UINT16            usSliceIt;
  UINT16            usSliceNum;
  Slice           **ppzsSlices;
  ReceiverPackage  *pzrpPkg;

  pzrpPkg = (ReceiverPackage *) pvRecvPkg;

  if (pzrpPkg == NULL) {
    return;
  }

  // Free the slices
  ppzsSlices = pzrpPkg->ppzsSlices;
  if (ppzsSlices != NULL) {
    for (usSliceIt = 0, usSliceNum = pzrpPkg->usSliceNum;
        usSliceIt < usSliceNum; usSliceIt ++ ) {
      free ( ppzsSlices [usSliceIt] );
    }
    free (ppzsSlices);
  }

  // Frre the package itself
  free (pzrpPkg);

}

/******************************************************************************
 * Func : VPhone::__OnAudioLudpRecv
 * Desc : Callback called when Ludp receive package
 * Args : zrpPkg                  Package received by ludp receiver
 * Outs : NONE
 ******************************************************************************/
void VPhone::__OnAudioLudpRecv (ReceiverPackage &zrpPkg)
{
  ReceiverPackage     *pzrpPkgNew;

  // Lock this instance
  m_zhmMutex.Lock ();
  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Copy this ReceiverPackage
  pzrpPkgNew = (ReceiverPackage *) malloc ( sizeof(ReceiverPackage) );
  if (pzrpPkgNew == NULL) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("allocate receive package failed") );
    goto failed;
  }
  memcpy ( pzrpPkgNew, &zrpPkg, sizeof (ReceiverPackage) );

  // Put it into JitterBuffer
  m_zjbAudioBuf.PutPkg ( (HLH_RoundU32)pzrpPkgNew->unTimestamp, pzrpPkgNew, FreeReceivePackage );

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return;

failed:
  if (pzrpPkgNew != NULL) {
    free (pzrpPkgNew);
    pzrpPkgNew = NULL;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

}


/******************************************************************************
 * Func : VPhone::OnAudioLudpRecv
 * Desc : Wrapper of __OnAudioLudpRecv ()
 * Args : pvThis                  Pointer to this instance
 *        zrpPkg                  Package received by ludp receiver
 * Outs : NONE
 ******************************************************************************/
void VPhone::OnAudioLudpRecv (void *pvThis, ReceiverPackage &zrpPkg)
{
  ( (VPhone*) pvThis )->__OnAudioLudpRecv (zrpPkg);
}



/******************************************************************************
 * Func : VPhone::OnVideoLudpRecv
 * Desc : Handler of packages received by video ludp receiver
 * Args : pvThis                  Pointer to this instance
 *        zrpPkg                  Package received by ludp receiver
 * Outs : NONE
 ******************************************************************************/
void VPhone::__OnVideoLudpRecv (ReceiverPackage &zrpPkg)
{
  ReceiverPackage     *pzrpPkgNew;

  // Lock this instance
  m_zhmMutex.Lock ();
  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Copy this ReceiverPackage
  pzrpPkgNew = (ReceiverPackage *) malloc ( sizeof(ReceiverPackage) );
  if (pzrpPkgNew == NULL) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("allocate receive package failed") );
    goto failed;
  }
  memcpy ( pzrpPkgNew, &zrpPkg, sizeof (ReceiverPackage) );

  // Put it into JitterBuffer
  m_zjbVideoBuf.PutPkg ( (HLH_RoundU32)pzrpPkgNew->unTimestamp, pzrpPkgNew, FreeReceivePackage );

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return;

failed:
  if (pzrpPkgNew != NULL) {
    free (pzrpPkgNew);
    pzrpPkgNew = NULL;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

}


/******************************************************************************
 * Func : VPhone::OnVideoLudpRecv
 * Desc : Wrapper of __OnVideoLudpRecv ()
 * Args : pvThis                  Pointer to this instance
 *        zrpPkg                  Package received by ludp receiver
 * Outs : NONE
 ******************************************************************************/
void VPhone::OnVideoLudpRecv (void *pvThis, ReceiverPackage &zrpPkg)
{
  ( (VPhone*) pvThis )->__OnVideoLudpRecv (zrpPkg);
}



/******************************************************************************
 * Func : VPhone::__OnVideoJitterBufferGet
 * Desc : Handler for jitter buffer package
 * Args : pvThis                        pointer to this instance
 *        zjpPkg                        package ready in JitterBuffer
 * Outs : NONE
 ******************************************************************************/
void VPhone::__OnVideoJitterBufferGet (JPackage & zjpPkg)
{

  ReceiverPackage  *pzrpPkg;
  UINT16            usSliceIt;
  UINT16            usSliceNum;
  Slice           **ppzsSlices;
	UINT32						unLen;

  UINT8            *pucBuf;
	void 						 *pvDecOutBuf;

  // Check arguments
  pzrpPkg = (ReceiverPackage *) zjpPkg.m_pvData;
  ASSERT (pzrpPkg != NULL);
  ASSERT (pzrpPkg->usSliceNum > 0);
  ASSERT (pzrpPkg->ppzsSlices != NULL);

#if HLH_DEBUG_CONFIG_VPHONE > 0
  static UINT32           unLastTimestamp = 0;
  if (unLastTimestamp + 1 != pzrpPkg->unTimestamp) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("jump from %u to %u", unLastTimestamp, pzrpPkg->unTimestamp) );
    unLastTimestamp = pzrpPkg->unTimestamp;
  } else {
    unLastTimestamp ++;
  }
#endif


  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Put this data into H264 decoder input buffer
  pucBuf = m_aucDecodeBuf;
  usSliceNum = pzrpPkg->usSliceNum;
  ppzsSlices = pzrpPkg->ppzsSlices;
  for (unLen = 0, usSliceIt = 0; usSliceIt < usSliceNum; usSliceIt ++) {
    ASSERT ( ppzsSlices [usSliceIt] );
		unLen += ppzsSlices[usSliceIt]->usDataLen;
		if ( unLen > sizeof(m_aucDecodeBuf) ) {
			HLH_DEBUG ( HLH_DEBUG_MAIN, ("package too long, ignored") );
			goto failed;
		}
    memcpy (pucBuf, ppzsSlices[usSliceIt]->aucData,
        ppzsSlices[usSliceIt]->usDataLen );
    pucBuf += ppzsSlices[usSliceIt]->usDataLen;
  }

  // Decode this image
	pvDecOutBuf = m_zhdDecoder.Decode ( m_aucDecodeBuf, (UINT32)(pucBuf - m_aucDecodeBuf));
  if ( pvDecOutBuf == NULL ) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("decode failed") );
    goto failed;
  }

  // Show the image
  m_zppPostProcessor.Process ( (void*)pvDecOutBuf, m_zfbFrameBuffer.GetFrame(0) );

failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

}


/******************************************************************************
 * Func : VPhone::OnVideoJitterBufferGet
 * Desc : Wrapper of __OnVideoJitterBufferGet
 * Args : pvThis                        pointer to this instance
 *        zjpPkg                        package ready in JitterBuffer
 * Outs : NONE
 ******************************************************************************/
void VPhone::OnVideoJitterBufferGet (void * pvThis, JPackage & zjpPkg)
{
  ASSERT (pvThis != NULL);

  ( (VPhone*) pvThis )->__OnVideoJitterBufferGet (zjpPkg);
}



/******************************************************************************
 * Func : VPhone::__CaptureThreadFunc
 * Desc : Thread function to capture the video, encode and send it.
 * Args : zhtCaptureThread      capture thread to call this function
 * Outs : NONE
 ******************************************************************************/
void VPhone::__CaptureThreadFunc (HLH_Thread & zhtCaptureThread)
{
  int       nRetVal;
  void     *pvEncOutBuf;
  UINT32    unEncOutSize;
  HLH_Time  zhtTime;

  // Notify that this thread started
  zhtCaptureThread.ThreadStarted ();

  // Lock this instance
  m_zhmMutex.Lock ();
  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("not connected") );
    return;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  // Start capture
  nRetVal = m_zhvCamera.Start ();
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("start camera failed") );
    return;
  }

  // Capture video, encode and send it.

  do {
    // Wait for image ready
    zhtTime = HLH_TIME_FROM_USECS (VPHONE_TIMEOUT_POLL_CAMERA_READ);
    nRetVal = m_zhvCamera.TimedPollRead (zhtTime);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("poll to read camera timeout") );
      break;
    }

    // Read camera image
    nRetVal = m_zhvCamera.Read (m_aucCameraBuf, m_unFrameSize);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("read camera failed") );
      break;
    }

    // Encode this image
		pvEncOutBuf = m_zheEncoder.Encode (m_aucCameraBuf, m_unFrameSize, unEncOutSize);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("encode frame failed") );
      break;
    }

    // Send the stream
    nRetVal = m_zlsVideoSender.SendPackage ( (UINT8*)pvEncOutBuf, unEncOutSize );
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("send package failed, ignored") );
      // ignore this after all, may cause by lacked memory
    }

  } while ( !zhtCaptureThread.IsStopping () );

  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("return from capture thread") );

}



/******************************************************************************
 * Func : VPhone::CaptureThreadFunc
 * Desc : Wrapper of __CaptureThreadFunc
 * Args : zhtCaptureThread      capture thread to call this function
 * Outs : always NULL.
 ******************************************************************************/
void * VPhone::CaptureThreadFunc (
    HLH_Thread    & zhtCaptureThread,
    void          * pvThis )
{
  ASSERT (pvThis != NULL);

  ( (VPhone*) pvThis )->__CaptureThreadFunc (zhtCaptureThread);

  return NULL;
}
