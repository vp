/*******************************************************************************
 *    File Name : FrameBuffer.cpp
 * 
 *       Author : Henry He
 * Created Time : Fri 30 Oct 2009 09:29:17 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  <sys/mman.h>

#include  "fb/FrameBuffer.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/



/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/





/******************************************************************************
 * Func : FrameBuffer::FrameBuffer
 * Desc : Constructor of FrameBuffer
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
FrameBuffer::FrameBuffer ()
{

  m_zhmMutex.Init ();
  m_bCreated = false;
  memset ( &m_ffsScreenInfo, 0, sizeof (m_ffsScreenInfo) );
  memset ( &m_fvsScreenInfo, 0, sizeof (m_fvsScreenInfo) );

}   // -----  end of method FrameBuffer::FrameBuffer  (constructor)  ----- 


/******************************************************************************
 * Func : FrameBuffer::~FrameBuffer
 * Desc :  Deconstructor of FrameBuffer
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
FrameBuffer::~FrameBuffer ()
{

  Destroy ();
  m_zhmMutex.Destroy ();

}   // -----  end of method FrameBuffer::~FrameBuffer  (deconstructor)  ----- 





/******************************************************************************
 * Func : FrameBuffer::Create
 * Desc : Create frame buffer
 * Args : pcFBPath                Path of fb device
 * Outs : If success return 0, otherwise return error code.
 ******************************************************************************/
int FrameBuffer::Create (const char *pcFBPath)
{
  int            nRetVal;
  s3c_win_info_t swiOSDInfo;


  ASSERT (pcFBPath != NULL);


  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("already created") );
    return FRAME_BUFFER_ERR_ALREADY_CREATED;
  }

  // Open framebuffer device
  m_fdFB = open ( pcFBPath, O_RDWR|O_NDELAY );
  if (m_fdFB == -1) {
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("can't open %s", pcFBPath) );
    goto failed;
  }

  // Get fix screen info
  nRetVal = ioctl (m_fdFB, FBIOGET_FSCREENINFO, &m_ffsScreenInfo);
  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("can't get fix screen info") );
    goto failed;
  }

  // Get variable screen info
  nRetVal = ioctl (m_fdFB, FBIOGET_VSCREENINFO, &m_fvsScreenInfo);
  if(nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("can't get var screen info") );
    goto failed;
  }

  Printf ("FrameBuffer: screen: %d X %d X %db in ( %d X %d )\n", m_fvsScreenInfo.xres,
      m_fvsScreenInfo.yres, m_fvsScreenInfo.bits_per_pixel,
      m_fvsScreenInfo.xres_virtual, m_fvsScreenInfo.yres_virtual);

  // Check on bits per pixel
  if        (m_fvsScreenInfo.bits_per_pixel == 16) {
    m_scsColor = RGB16;
    m_unBytesPerPixel = 2;
  } else if (m_fvsScreenInfo.bits_per_pixel == 24) {
    m_scsColor = RGB24;
    m_unBytesPerPixel = 3;
  } else if (m_fvsScreenInfo.bits_per_pixel == 8) {
    m_scsColor = RGB8;
    m_unBytesPerPixel = 1;
  } else {
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("Color not supported") );
    goto failed;
  }

  // Try to get full buffer
  HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN,
      ("mmap size = %d\n",
       m_fvsScreenInfo.xres_virtual * m_fvsScreenInfo.yres_virtual * m_unBytesPerPixel) );

  m_pvFrameBuf = (void *) mmap ( 0,
      m_fvsScreenInfo.xres_virtual * m_fvsScreenInfo.yres_virtual * m_unBytesPerPixel,
      PROT_READ|PROT_WRITE, MAP_SHARED, m_fdFB, 0 );
  if (m_pvFrameBuf == NULL) {
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("FrameBuffer: Can't map frame buffer.") );
    goto failed;
  }


  swiOSDInfo.Bpp       = m_fvsScreenInfo.bits_per_pixel;
  swiOSDInfo.LeftTop_x = 0;
  swiOSDInfo.LeftTop_y = 0;
  swiOSDInfo.Width     = m_fvsScreenInfo.xres;
  swiOSDInfo.Height    = m_fvsScreenInfo.yres;

  // set OSD's information 
  nRetVal = ioctl(m_fdFB, SET_OSD_INFO, &swiOSDInfo);
  if( nRetVal < 0 ) {
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("FrameBuffer: Some problem with the ioctl SET_OSD_INFO") );
    goto  failed;
  }

  nRetVal = ioctl (m_fdFB, SET_OSD_START);
  if( nRetVal < 0 ) {
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("FrameBuffer: Some problem with the ioctl SET_OSD_START") );
    goto  failed;
  }


  // Notify that this instance created
  m_bCreated = true;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:
  // Unmap framebuffer if failed
  if (m_pvFrameBuf != NULL) {
    munmap (m_pvFrameBuf, m_fvsScreenInfo.xres_virtual * m_fvsScreenInfo.yres_virtual * m_unBytesPerPixel);
    m_pvFrameBuf = NULL;
  }

  // Close framebuffer if failed
  if (m_fdFB >= 0) {
    close (m_fdFB);
    m_fdFB = -1;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return FRAME_BUFFER_ERR_CANT_CREATE;

}


/******************************************************************************
 * Func : FrameBuffer::Destroy
 * Desc : Destroy frame buffer
 * Args : 
 * Outs : 
 ******************************************************************************/
void FrameBuffer::Destroy ()
{
  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Unmap frame buffer
  munmap (m_pvFrameBuf,
      m_fvsScreenInfo.xres_virtual * m_fvsScreenInfo.yres_virtual * m_unBytesPerPixel);
  m_pvFrameBuf = NULL;

  // Close frame buffer
  close (m_fdFB);
  m_fdFB = -1;

  // Notify that this instance not created
  m_bCreated = false;

  // Unlock this instance
  m_zhmMutex.Unlock ();
}


/******************************************************************************
 * Func : FrameBuffer::GetFrame
 * Desc : Get frame pointer with specific y
 * Args : unYOffset               offset of Y
 * Outs : If success return pointer to frame buffer, otherwise return NULL.
 ******************************************************************************/
void *FrameBuffer::GetFrame (UINT32 unYOffset)
{
  void        *pvFrame;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("not created") );
    return NULL;
  }

  // Get frame buffer pointer
  pvFrame = ((UINT8*)m_ffsScreenInfo.smem_start)
    + m_fvsScreenInfo.xres_virtual * m_unBytesPerPixel * unYOffset;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return pvFrame;
}


/******************************************************************************
 * Func : FrameBuffer::SetYOffset
 * Desc : Set Y offset of frame buffer
 * Args : unYOffset           offset of Y to set
 * Outs : If success return 0, otherwise return error code.
 ******************************************************************************/
int FrameBuffer::SetYOffset (UINT32 unYOffset)
{

  int nRetVal;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("not created") );
    return FRAME_BUFFER_ERR_NOT_CREATED;
  }

  // Check Y offset
  if ( unYOffset > m_fvsScreenInfo.yres_virtual ) {
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("unYOffset larger than m_fvsScreenInfo.yres_virtual") );
    goto failed;
  }

  // Set Y offset
  m_fvsScreenInfo.yoffset = unYOffset;
  nRetVal = ioctl (m_fdFB, FBIOPUT_VSCREENINFO, &m_fvsScreenInfo);
  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("ioctl FBIOPUT_VSCREENINFO failed") );
    goto failed;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return FRAME_BUFFER_ERR_FAILED;

}


/******************************************************************************
 * Func : FrameBuffer::GetWidth
 * Desc : Get Width  of Screen
 * Args : NONE
 * Outs : If success return width of screen, otherwise return 0
 ******************************************************************************/
UINT32  FrameBuffer::GetWidth ()
{
  UINT32 unWidth;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("not created") );
    return 0;
  }

  unWidth = m_fvsScreenInfo.xres;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return unWidth;
}


/******************************************************************************
 * Func : FrameBuffer::GetHeight
 * Desc : Get Height of Screen
 * Args : NONE
 * Outs : If success return height, otherwise return 0.
 ******************************************************************************/
UINT32 FrameBuffer::GetHeight ()
{
  UINT32 unHeight;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("not created") );
    return 0;
  }

  unHeight = m_fvsScreenInfo.yres;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return unHeight;
}


/******************************************************************************
 * Func : FrameBuffer::GetVirtualWidth
 * Desc : Get virtual width of screen
 * Args : NONE
 * Outs : If success return virtual width, otherwise return 0.
 ******************************************************************************/
UINT32 FrameBuffer::GetVirtualWidth ()
{
  UINT32 unWidth;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("not created") );
    return 0;
  }

  unWidth = m_fvsScreenInfo.xres_virtual;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return unWidth;
}


/******************************************************************************
 * Func : FrameBuffer::GetVirtualHeight
 * Desc : Get virtual height of screen
 * Args : NONE
 * Outs : If success return virtual height, otherwise return 0.
 ******************************************************************************/
UINT32 FrameBuffer::GetVirtualHeight ()
{
  UINT32 unHeight;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("not created") );
    return 0;
  }

  unHeight = m_fvsScreenInfo.yres_virtual;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return unHeight;
}


/******************************************************************************
 * Func : FrameBuffer::GetLineLength
 * Desc : Get length of a line in bytes
 * Args : NONE
 * Outs : If success return line length, otherwise return 0.
 ******************************************************************************/
UINT32 FrameBuffer::GetLineLength ()
{
  UINT32 unLength;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("not created") );
    return 0;
  }

  unLength = m_ffsScreenInfo.line_length;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return unLength;
}



/******************************************************************************
 * Func : FrameBuffer::GetColor
 * Desc : Get color space
 * Args : NONE
 * Outs : If success return color, otherwise return RGB16.
 ******************************************************************************/
s3c_color_space_t FrameBuffer::GetColor ()
{
  s3c_color_space_t scsColor;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_FB|HLH_DEBUG_MAIN, ("not created") );
    return RGB16;
  }

  scsColor = m_scsColor;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return scsColor;
}


