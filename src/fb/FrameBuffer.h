/*******************************************************************************
 *         File : FrameBuffer.h
 * 
 *       Author : Henry He
 *      Created : Fri 30 Oct 2009 09:29:11 PM CST
 *  Description : 
 ******************************************************************************/

#ifndef __FRAMEBUFFER_INC_20091030_212911_HENRY__
#define __FRAMEBUFFER_INC_20091030_212911_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  <linux/fb.h>

#include  "utils/common.h"
#include  "fb/lcd.h"
#include  "pp/s3c_pp.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


//===========================  Error Code  =====================================

#define FRAME_BUFFER_ERR_FAILED          (-1)
#define FRAME_BUFFER_ERR_NOT_CREATED     (-2)
#define FRAME_BUFFER_ERR_ALREADY_CREATED (-3)
#define FRAME_BUFFER_ERR_CANT_CREATE     (-4)

/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/

typedef struct
{
  unsigned int map_dma_f1;
  unsigned int map_dma_f2;
} s3c_fb_dma_info_t;


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/



/******************************************************************************
 * Class : FrameBuffer
 * Desc  : Simple Wrapper of framebuffer
 ******************************************************************************/

class FrameBuffer
{

  public:

    // ====================  LIFECYCLE    ========================================= 

    // Constructor of FrameBuffer
    FrameBuffer ();

    // Decontructor of FrameBuffer
    ~ FrameBuffer ();


  public:

    // ====================  OPERATORS    ========================================= 

    // ====================  OPERATIONS   ========================================= 

    // Create frame buffer
    int Create (const char *pcFBPath);

    // Destroy frame buffer
    void Destroy ();


    // ====================  ACCESS       ========================================= 

    // Get frame pointer with specific y
    void * GetFrame (UINT32 unYOffset);

    // Set Y offset of frame buffer
    int SetYOffset (UINT32 unYOffset);

    // Get Width  of Screen
    UINT32  GetWidth ();
    
    // Get Height of Screen
    UINT32 GetHeight ();

    // Get virtual width of screen
    UINT32 GetVirtualWidth ();

    // Get virtual height of screen
    UINT32 GetVirtualHeight ();

    // Get length of a line in bytes
    UINT32 GetLineLength ();

    // Get color
    s3c_color_space_t GetColor ();


  private:

    // ====================  OPERATIONS   ========================================= 


  private:

    // ====================  MEMBER DATA  ========================================= 

    // Mutex of this instance
    HLH_Mutex                   m_zhmMutex;

    // Whether this instance created
    bool                        m_bCreated;

    // File descriptor of framebuffer
    int                         m_fdFB;

    // Buffer of FrameBuffer
    void                       *m_pvFrameBuf;

    // Frame buffer fix screen info
    struct fb_fix_screeninfo    m_ffsScreenInfo;

    // Frame buffer var screen info
    struct fb_var_screeninfo    m_fvsScreenInfo;

    // Frame buffer color space
    s3c_color_space_t           m_scsColor;
    UINT32                      m_unBytesPerPixel;

};  // -----  end of class  FrameBuffer  ----- 




#endif /* __FRAMEBUFFER_INC_20091030_212911_HENRY__ */
