/*******************************************************************************
 *    File Name : HLH_Audio.cpp
 * 
 *       Author : Henry He
 * Created Time : 2009-10-16 10:03:57
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "fake_audio/HLH_Audio.h"



#if (__USE_FAKE_AUDIO__ > 0)


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/





/*******************************************************************************
 * Desc : Member Functions
 ******************************************************************************/




////////////////////////////////////////////////////////////////////////////////
// Constructor / Deconstructor
////////////////////////////////////////////////////////////////////////////////





/******************************************************************************
 * Func : HLH_Audio::HLH_Audio
 * Desc : Constructor of HLH_Audio
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_Audio::HLH_Audio ()
{
  m_zhmMutex.Init ();
  m_zhmReadMutex.Init ();
  m_zhmWriteMutex.Init ();

  m_bCreated = false;

  m_fdAudio = -1;
  m_fdFakeAudioIn = -1;
  m_fdFakeAudioOut = -1;
}



/******************************************************************************
 * Func : HLH_Audio::~HLH_Audio
 * Desc : Deconstructor of HLH_Audio
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_Audio::~HLH_Audio ()
{
  Destroy ();
}



////////////////////////////////////////////////////////////////////////////////
// Operations
////////////////////////////////////////////////////////////////////////////////




/******************************************************************************
 * Func : HLH_Audio::Create
 * Desc : Create audio for read/write
 * Args : unSampleBits              Numbers of bits per sample
 *        unSampleRate              Sample rate of audio device
 *        unChannels                Number of channels
 *        zoaOnRead                 Callback function, called if data availble
 *                                      if not NULL, thread will be start to poll for read
 *        pvOnReadParam             Parameter to m_zoaOnRead ()
 *        unBufSizeLog2             Buffer size (2^unBufSizeLog2) of audio device
 *        unBufNum                  Buffer number of audio device
 * Outs : if success return 0, otherwise return error code
 ******************************************************************************/
int HLH_Audio::Create (
    OnAudioReadFunc   zoaOnRead,
    void            * pvOnReadParam,
    UINT32            unSampleBits,
    UINT32            unSampleRate,
    UINT32            unChannels,
    UINT16            unBufSizeLog2,
    UINT16            unBufNum
    )
{
  int nRetVal;
  int nSetting;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return HLH_AUDIO_ERR_ALREADYCREATED;
  }

  // Open the audio device
  m_fdAudio = open ("/dev/dsp", O_RDWR /*| O_NONBLOCK*/);
  if (m_fdAudio == -1) {
    goto failed;
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Audio device setting

  // Set Duplex
  nSetting = 1;
  nRetVal = ioctl (m_fdAudio, SNDCTL_DSP_SETDUPLEX, &nSetting);
  if (nRetVal < 0 || nSetting != 1) {
    goto failed;
  }

  // Set sample format
  nSetting = unSampleBits;
  nRetVal = ioctl (m_fdAudio, SNDCTL_DSP_SETFMT, &nSetting);
  if (nRetVal < 0 || (UINT32)nSetting != unSampleBits) {
    goto failed;
  }

  // Set sample rate
  nSetting = unSampleRate;
  nRetVal = ioctl (m_fdAudio, SNDCTL_DSP_SPEED, &nSetting);
  if (nRetVal < 0 || (UINT32)nSetting != unSampleRate) {
    goto failed;
  }

  // Set channels number
  nSetting = (unChannels == 1) ? 0 : 1;
  nRetVal = ioctl (m_fdAudio, SNDCTL_DSP_STEREO, &nSetting); 
  if (  nRetVal < 0 | nSetting != ( (unChannels == 1) ? 0 : 1 )  ) {
    goto failed;
  }

  // Set audio buffer
  nSetting = (unBufNum << 16) | unBufSizeLog2;
  nRetVal = ioctl (m_fdAudio, SNDCTL_DSP_SETFRAGMENT, &nSetting);
  if (  nRetVal < 0 || nSetting != ( (unBufNum << 16) | unBufSizeLog2 )  ) {
    goto failed;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Set read handler
  m_zoaOnRead     = zoaOnRead;
  m_pvOnReadParam = pvOnReadParam;


  ////////////////////////////////////////////////////////////////////////////////
  // prepare file descriptor set for poll
  FD_ZERO (&m_fsReadSet);
  FD_ZERO (&m_fsWriteSet);
  FD_ZERO (&m_fsErrorSet);

#if (__USE_FAKE_AUDIO_IN__ > 0)
  m_fdFakeAudioIn = open (HLH_AUDIO_FAKE_FILE_IN, O_RDONLY);
  if (m_fdFakeAudioIn < 0) {
    HLH_DEBUG ( HLH_DEBUG_AUDIO, ("can't open fake input file") );
    goto failed;
  }
  FD_SET (m_fdFakeAudioIn, &m_fsReadSet);
  FD_SET (m_fdFakeAudioIn, &m_fsErrorSet);
  m_fdRead = m_fdFakeAudioIn;
#else
  FD_SET (m_fdAudio, &m_fsReadSet);
  m_fdRead = m_fdAudio;
#endif

#if (__USE_FAKE_AUDIO_OUT__ > 0)
  m_fdFakeAudioOut = open (HLH_AUDIO_FAKE_FILE_OUT,
      O_CREAT|O_TRUNC|O_WRONLY, 0666);
  if (m_fdFakeAudioOut < 0) {
    HLH_DEBUG ( HLH_DEBUG_AUDIO, ("can't open fake output file") );
    goto failed;
  }
  FD_SET (m_fdFakeAudioOut, &m_fsWriteSet);
  FD_SET (m_fdFakeAudioOut, &m_fsErrorSet);
  m_fdWrite = m_fdFakeAudioOut;
#else
  FD_SET (m_fdAudio, &m_fsWriteSet);
  m_fdWrite = m_fdAudio;
#endif


#if (__USE_FAKE_AUDIO_OUT__ <= 0 && __USE_FAKE_AUDIO_IN__ <= 0)
  FD_SET (m_fdAudio, &m_fsErrorSet);
#endif

  // Create read thread only if callback provided
  if (m_zoaOnRead != NULL) {
    // Allocate read buffer
    m_unReadLen     = 2 ^ unBufSizeLog2;
    m_pcReadBuf     = (char*) malloc (m_unReadLen);
    if (m_pcReadBuf == NULL) {
      goto failed;
    }

    // Start read thread
    nRetVal = m_zhtReadThread.Start (ReadThreadFunc, this);
    if (nRetVal < 0) {
      // Free the memory allocated
      free (m_pcReadBuf);
      m_pcReadBuf = NULL;
      goto failed;
    }
  }

  // Notify that this instance created
  m_bCreated = true;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:

  // Close the audio device if failed
  if (m_fdAudio >= 0) {
    close (m_fdAudio);
    m_fdAudio = -1;
  }

#if (__USE_FAKE_AUDIO_IN__ > 0)
  if (m_fdFakeAudioIn >= 0) {
    close (m_fdFakeAudioIn);
    m_fdFakeAudioIn = -1;
  }
#endif

#if (__USE_FAKE_AUDIO_OUT__ > 0)
  if (m_fdFakeAudioOut >= 0) {
    close (m_fdFakeAudioOut);
    m_fdFakeAudioOut = -1;
  }
#endif

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return HLH_AUDIO_ERR_FAILED;
}



/******************************************************************************
 * Func : HLH_Audio::Destroy
 * Desc : Destory this instance created
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void HLH_Audio::Destroy ()
{
  // Stop read thread
  m_zhtReadThread.Stop ();


  // Lock send mutex
  m_zhmReadMutex.Lock ();

  // Lock this instance
  m_zhmMutex.Lock ();

  // Check if created
  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

    // Unlock send mutex
    m_zhmReadMutex.Unlock ();
    return;
  }

  // Close the device opened
  close (m_fdAudio);
  m_fdAudio = -1;

#if (__USE_FAKE_AUDIO_IN__ > 0)
  close (m_fdFakeAudioIn);
  m_fdFakeAudioIn = -1;
#endif

#if (__USE_FAKE_AUDIO_OUT__ > 0)
  close (m_fdFakeAudioOut);
  m_fdFakeAudioOut = -1;
#endif


  // Free memory allocated
  free (m_pcReadBuf);
  m_pcReadBuf = NULL;


  // Notify that this instance not created
  m_bCreated = false;


  // Unlock this instance
  m_zhmMutex.Unlock ();

  // Unlock send mutex
  m_zhmReadMutex.Unlock ();
}



/******************************************************************************
 * Func : HLH_Audio::IsCreated
 * Desc : Whether this instance is created
 * Args : NONE
 * Outs : if created, return true, otherwise return false
 ******************************************************************************/
bool HLH_Audio::IsCreated ()
{

  bool bCreated;

  // Lock this instance
  m_zhmMutex.Lock ();

  bCreated = m_bCreated;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return bCreated;

}






/******************************************************************************
 * Func : HLH_Audio::Read (nonblock)
 * Desc : Read for audio device
 * Args : pvBuf                 pointer to buffer (at least \c unLen length)
 *        unLen                 bytes to read
 * Outs : if success return the number of bytes read,
 *            otherwise return error code
 ******************************************************************************/
int HLH_Audio::Read (void *pvBuf, UINT32 unLen)
{

  int nRetVal;

  // Lock read mutex
  m_zhmReadMutex.Lock ();

  // Lock this instance
  m_zhmMutex.Lock ();

  // Check if created
  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

    // Unlock read mutex
    m_zhmReadMutex.Unlock ();

    return HLH_AUDIO_ERR_FAILED;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  nRetVal = read (m_fdRead, pvBuf, unLen);
  if (nRetVal < 0) {
    goto failed;
  }

  // Unlock read mutex
  m_zhmReadMutex.Unlock ();

  return nRetVal;

failed:
  // Unlock this instance
  m_zhmReadMutex.Unlock ();

  return HLH_AUDIO_ERR_FAILED;

}


/******************************************************************************
 * Func : HLH_Audio::Write (nonblock)
 * Desc : Write to audio device
 * Args : pvBuf               Pointer to buffer
 *        unLen               Bytes to write
 * Outs : If success return the number of bytes writen,
 *        otherwise return error code
 ******************************************************************************/
int HLH_Audio::Write (void *pvBuf, UINT32 unLen)
{

  int nRetVal;

  // Lock write mutex
  m_zhmWriteMutex.Lock ();

  // Lock this instance
  m_zhmMutex.Lock ();

  // Check if created
  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

    // Unlock write mutex
    m_zhmWriteMutex.Unlock ();

    return HLH_AUDIO_ERR_FAILED;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  nRetVal = write (m_fdWrite, pvBuf, unLen);
  if (nRetVal < 0) {
    goto failed;
  }

  // Unlock write mutex
  m_zhmWriteMutex.Unlock ();

  return nRetVal;

failed:
  // Unlock this instance
  m_zhmWriteMutex.Unlock ();

  return HLH_AUDIO_ERR_FAILED;

}


////////////////////////////////////////////////////////////////////////////////
// Poll Operations
////////////////////////////////////////////////////////////////////////////////




/******************************************************************************
 * Func : HLH_Audio::Poll
 * Desc : Poll for specific event
 * Args : unPollType            type to poll
 * Outs : if success return number of events, otherwise return error code
 ******************************************************************************/
int HLH_Audio::Poll (UINT32 &unPollType)
{

  fd_set    fsReadSet;
  fd_set    fsWriteSet;
  fd_set    fsErrorSet;

  int       nRetVal;
  UINT32    unPollTypeNew;

  // Lock before process
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock after process
    m_zhmMutex.Unlock ();

    return HLH_AUDIO_ERR_NOT_CREATED;
  }

  memcpy ( &fsReadSet,  &m_fsReadSet,  sizeof (fd_set) );
  memcpy ( &fsWriteSet, &m_fsWriteSet, sizeof (fd_set) );
  memcpy ( &fsErrorSet, &m_fsErrorSet, sizeof (fd_set) );

  // Unlock after process,
  // Unlock before poll as we may want to close before poll successed
  m_zhmMutex.Unlock ();

  // Poll for specific event
  nRetVal = select ( m_fdAudio + 1,
      unPollType & HLH_AUDIO_POLL_READ  ? &fsReadSet  : NULL,
      unPollType & HLH_AUDIO_POLL_WRITE ? &fsWriteSet : NULL,
      unPollType & HLH_AUDIO_POLL_ERROR ? &fsErrorSet : NULL,
      NULL
      );

  if (nRetVal < 0) {
    goto failed;
  }


  // Set return value

  unPollTypeNew = 0;

  if (  ( unPollType & HLH_AUDIO_POLL_READ )
      && FD_ISSET (m_fdRead, &fsReadSet)  ) {
    unPollTypeNew |= HLH_AUDIO_POLL_READ;
  }

  if (  ( unPollType & HLH_AUDIO_POLL_WRITE )
      && FD_ISSET (m_fdWrite, &fsWriteSet)  ) {
    unPollTypeNew |= HLH_AUDIO_POLL_WRITE;
  }

  if (  ( unPollType & HLH_AUDIO_POLL_ERROR )
      && FD_ISSET (m_fdAudio, &fsErrorSet)  ) {
    unPollTypeNew |= HLH_AUDIO_POLL_ERROR;
  }

  unPollType = unPollTypeNew;

  return nRetVal;

failed:

  return HLH_AUDIO_ERR_FAILED;

}


/******************************************************************************
 * Func : HLH_Audio::PollWait
 * Desc : Poll for specific event util timeout
 * Args : unPollType              type to poll
 *        zhtTime                 time to wait until poll return
 * Outs : if success return number of events, otherwise return error code
 ******************************************************************************/
int HLH_Audio::PollWait (UINT32 &unPollType, HLH_Time &zhtTime)
{

  fd_set            fsReadSet;
  fd_set            fsWriteSet;
  fd_set            fsErrorSet;

  int               nRetVal;

  UINT32            unPollTypeNew;

  struct timeval    tvTime;

  // Lock before process
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock after process
    m_zhmMutex.Unlock ();

    return HLH_AUDIO_ERR_NOT_CREATED;
  }

  memcpy ( &fsReadSet,  &m_fsReadSet,  sizeof (fd_set) );
  memcpy ( &fsWriteSet, &m_fsWriteSet, sizeof (fd_set) );
  memcpy ( &fsErrorSet, &m_fsErrorSet, sizeof (fd_set) );

  // Poll for specific event
  tvTime = zhtTime.GetTimeVal ();

  // Unlock after process
  m_zhmMutex.Unlock ();

  nRetVal = select ( m_fdAudio + 1,
      unPollType & HLH_AUDIO_POLL_READ  ? &fsReadSet  : NULL,
      unPollType & HLH_AUDIO_POLL_WRITE ? &fsWriteSet : NULL,
      unPollType & HLH_AUDIO_POLL_ERROR ? &fsErrorSet : NULL,
      &tvTime
      );

  if (nRetVal < 0) {
    goto fail;
  }

  // Return the time left
  zhtTime.SetTime (tvTime);

  // Set return value

  unPollTypeNew = 0;

  if (  ( unPollType & HLH_AUDIO_POLL_READ )
      && FD_ISSET (m_fdRead, &fsReadSet)  ) {
    unPollTypeNew |= HLH_AUDIO_POLL_READ;
  }

  if (  ( unPollType & HLH_AUDIO_POLL_WRITE )
      && FD_ISSET (m_fdWrite, &fsWriteSet)  ) {
    unPollTypeNew |= HLH_AUDIO_POLL_WRITE;
  }

  if (  ( unPollType & HLH_AUDIO_POLL_ERROR )
      && FD_ISSET (m_fdAudio, &fsErrorSet)  ) {
    unPollTypeNew |= HLH_AUDIO_POLL_ERROR;
  }

  unPollType = unPollTypeNew;

  return nRetVal;

fail:
  return HLH_AUDIO_ERR_FAILED;

}


/******************************************************************************
 * Func : HLH_Audio::ReadThreadFunc
 * Desc : Read thread to poll for read event and call callback function of read
 * Args : zhtThread                 Thread called this function
 * Outs : NONE
 ******************************************************************************/
void HLH_Audio::__ReadThreadFunc (HLH_Thread &zhtThread)
{
  int         nRetVal;

  UINT32      unPollType;
  HLH_Time    zhtTime;


  // Notify that this thread started
  zhtThread.ThreadStarted ();


  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();


  do {

    // Poll for read events
    unPollType = HLH_AUDIO_POLL_READ;
    zhtTime.SetTime (HLH_AUDIO_TIMEOUT_POLLWAIT_SECS, 0);

    nRetVal = PollWait (unPollType, zhtTime);

    // If there is any data received
    if ( nRetVal > 0 ) {

      // Read audio data
      nRetVal = Read ( m_pcReadBuf, m_unReadLen );
      if (nRetVal < 0) {
        // XXX: do nothing if receive error
        goto check_stop;
      }

      // Call callback function
      if (m_zoaOnRead != NULL) {
        m_zoaOnRead (m_pvOnReadParam, m_pcReadBuf, nRetVal);
      }

    }

check_stop:
    // Check if stop request
    if ( zhtThread.IsStopping () ) {
      // Respond to stop request
      return;
    }

  } while (1);

}


/******************************************************************************
 * Func : HLH_Audio::ReadThreadFunc
 * Desc : Read thread to poll for read event and call callback function of read
 * Args : pvThis              Pointer of this instance
 * Outs : Always return (void *) 0
 ******************************************************************************/
void * HLH_Audio::ReadThreadFunc (HLH_Thread &zhtThread, void *pvThis)
{
  ASSERT (pvThis != NULL);

  // Make it easy to write this code
  ( (HLH_Audio *) pvThis )->__ReadThreadFunc (zhtThread);

  return (void *) 0;
}


#endif /* (__USE_FAKE_AUDIO__ > 0) */
