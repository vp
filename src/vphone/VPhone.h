/*******************************************************************************
 *         File : VPhone.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-18 18:07:19
 *  Description : 
 ******************************************************************************/

#ifndef __VIDEOPHONE_INC_20091018_180719_HENRY__
#define __VIDEOPHONE_INC_20091018_180719_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "src/common.h"

/*******************************************************************************
 * Desc : Configure
 ******************************************************************************/

//===========================  H264 Encoder Configure  =========================
#define VPHONE_H264_ENC_WIDTH            320
#define VPHONE_H264_ENC_HEIGHT           240
#define VPHONE_H264_ENC_FRAME_RATE       15
#define VPHONE_H264_ENC_BIT_RATE         300 /* kbps */
#define VPHONE_H264_ENC_GOP_NUM          15



//===========================  Timeout  ========================================
#define VPHONE_TIMEOUT_POLL_CAMERA_READ_US   (500*1000)

////////////////////////////////////////////////////////////////////////////////
// Socket port of VPhone

#define VPHONE_PORT_AVDATA               5102


////////////////////////////////////////////////////////////////////////////////
// Audio delay in package
#define VPHONE_AUDIO_EXPECT_PKG_DELAY    (8000/160 * 1/3)

////////////////////////////////////////////////////////////////////////////////
// Audio LudpReceiver of VPhone

#define VPHONE_ALR_PKG_NUM               (VPHONE_AUDIO_EXPECT_PKG_DELAY * 11/10)
#define VPHONE_ALR_TIMETICK_US           (10 * 1000)
#define VPHONE_ALR_TIMOUT_IN_TIMETICK    5
#define VPHONE_ALR_MAX_RETRY             3

////////////////////////////////////////////////////////////////////////////////
// Audio JitterBuffer of VPhone
#define VPHONE_AJB_PER_TIMESTAMP         1
#define VPHONE_AJB_JITTER_TIMESTAMP      \
	(VPHONE_AUDIO_EXPECT_PKG_DELAY * VPHONE_AJB_PER_TIMESTAMP * 12/10)

////////////////////////////////////////////////////////////////////////////////
// Audio LudpSender of VPhone

#define VPHONE_ALS_PKG_NUM               (VPHONE_AUDIO_EXPECT_PKG_DELAY * 13/10)



////////////////////////////////////////////////////////////////////////////////
// Video delay in package
#define VPHONE_VIDEO_PKG_DELAY_EXPECT    (25 * 1/3)


////////////////////////////////////////////////////////////////////////////////
// Video LudpReceiver of VPhone

#define VPHONE_VLR_PKG_NUM               (VPHONE_VIDEO_PKG_DELAY_EXPECT * 11/10)
#define VPHONE_VLR_TIMETICK_US           (20 * 1000)
#define VPHONE_VLR_TIMOUT_IN_TIMETICK    5
#define VPHONE_VLR_MAX_RETRY             3

////////////////////////////////////////////////////////////////////////////////
// Video JitterBuffer of VPhone
#define VPHONE_VJB_PER_TIMESTAMP         1
#define VPHONE_VJB_JITTER_TIMESTAMP      (VPHONE_VIDEO_PKG_DELAY_EXPECT * 12/10)


////////////////////////////////////////////////////////////////////////////////
// Video LudpSender of VPhone

#define VPHONE_VLS_PKG_NUM               (VPHONE_VIDEO_PKG_DELAY_EXPECT * 13/10)





/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


////////////////////////////////////////////////////////////////////////////////
// Error Code of VideoPhoneCommand

#define VPHONE_COMMAND_ERR_FAILED    (-1)

////////////////////////////////////////////////////////////////////////////////
// Error Code of VPhone

#define VPHONE_ERR_FAILED            (-1)
#define VPHONE_ERR_NOT_CREATED       (-2)
#define VPHONE_ERR_ALREADY_CREATED   (-3)
#define VPHONE_ERR_WRONG_STATE       (-4)
#define VPHONE_ERR_CANNT_CONNECT     (-5)


////////////////////////////////////////////////////////////////////////////////
// HLH_Time from us

#define HLH_TIME_FROM_USECS(unUSecs) ( HLH_Time (unUSecs/1000000, unUSecs%1000000) )


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


////////////////////////////////////////////////////////////////////////////////
// State of VPhone
typedef enum tagVideoPhoneState
{
  VPHONE_STATE_IDLE = 0,
  VPHONE_STATE_CONNECTED,
  VPHONE_STATE_STOPPING
} VideoPhoneState ;


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Class to manager Video Phone communication

class VPhone
{

  public: 
    /******************************************************************************
     * Desc : Constructor / Deconstructor
     ******************************************************************************/
    VPhone ();
    ~ VPhone ();


  public:
    /******************************************************************************
     * Desc : Operations
     ******************************************************************************/

    // Start a communication to peer of address \c zhsPeerAddr
    int Start (
        HLH_SockAddr zhsPeerAddr, UINT32 unTimestampInit,
        bool bUseAudioReceiver, bool bUseAudioSender,
        bool bUseVideoReceiver, bool bUseVideoSender );

    // Stop the communication with peer
    void Stop ();


  private:
    /******************************************************************************
     * Desc : Callbacks
     ******************************************************************************/

    // Callback called when audio data ready
    void __OnAudioRead (void *pvBuf, UINT32 unLen);

    // Wrapper of \c __OnAudioRead ()
    static void OnAudioRead (void *pvThis, void *pvBuf, UINT32 unLen);

    // Callback called when JitterBuffer ready
    void __OnAudioJitterBufferGet (JPackage & zjpPkg);

    // Wrapper of __OnJitterBufferGet
    static void OnAudioJitterBufferGet (void * pvThis, JPackage & zjpPkg);

    // Callback called when Ludp receive package
    void __OnAudioLudpRecv (ReceiverPackage &zrpPkg);

    // Wrapper of __OnLudpRecv ()
    static void OnAudioLudpRecv (void *pvThis, ReceiverPackage &zrpPkg);



    // Handler of packages received by video ludp receiver
    void __OnVideoLudpRecv (ReceiverPackage &zrpPkg);

    // Wrapper of __OnVideoLudpRecv ()
    static void OnVideoLudpRecv (void *pvThis, ReceiverPackage &zrpPkg);

    // Handler for jitter buffer package
    void __OnVideoJitterBufferGet (JPackage & zjpPkg);

    // Wrapper of __OnVideoJitterBufferGet
    static void OnVideoJitterBufferGet (void * pvThis, JPackage & zjpPkg);

    // Thread function to capture the video, encode and send it.
    void __CaptureThreadFunc (HLH_Thread & zhtCaptureThread);

    // Wrapper of __CaptureThreadFunc
    static void * CaptureThreadFunc (HLH_Thread &zhtCaptureThread, void *pvThis);



  private:
    /******************************************************************************
     * Desc : Private Data
     ******************************************************************************/


    //===========================  Instance  =======================================

    // Mutex of this instance
    HLH_Mutex               m_zhmMutex;

    //===========================  Connection  =====================================


    // State of connection
    VideoPhoneState         m_zvpState;

    // Socket for audio and video
    HLH_UDPSock             m_zhuAVSock;

    // Whether use audio receiver
    bool                    m_bUseAudioReceiver;

    // Whether use audio sender
    bool                    m_bUseAudioSender;

    // Whether use video receiver
    bool                    m_bUseVideoReceiver;

    // Whether use video sender
    bool                    m_bUseVideoSender;


    //===========================  Audio  ==========================================


    // JitterBuffer for LudpReceiver
    JitterBuffer            m_zjbAudioBuf;

    // LudpReceiver for audio
    LudpReceiver            m_zlrAudioReceiver;

    // LudpSender for audio
    LudpSender              m_zlsAudioSender;

    // Audio device
    HLH_Audio               m_zhaAudioDev;



    //===========================  Video  ==========================================


    // Frame size
    UINT32                  m_unFrameSize;

    // Frame buffer
    UINT8                   m_aucCameraBuf [VPHONE_H264_ENC_WIDTH * VPHONE_H264_ENC_HEIGHT * 3 / 2];

    // Decode buffer
    UINT8                   m_aucDecodeBuf [VPHONE_H264_ENC_WIDTH * VPHONE_H264_ENC_HEIGHT * 3 / 2];

    // Camera for video
    HLH_Video               m_zhvCamera;

    // Capture thread for camera
    HLH_Thread              m_zhtCapture;

    // H264 Encoder
    H264Encoder             m_zheEncoder;
    UINT8                 * m_pucEncInBuf;

    // LudpSender for video
    LudpSender              m_zlsVideoSender;



    // LudpReceiver for video
    LudpReceiver            m_zlrVideoReceiver;

    // JitterBuffer for video
    JitterBuffer            m_zjbVideoBuf;

    // H264 Decoder
    H264Decoder             m_zhdDecoder;

    // Post Processor
    PostProcessor           m_zppPostProcessor;

    // Framebuffer
    FrameBuffer             m_zfbFrameBuffer;


};


#endif /* __VIDEOPHONE_INC_20091018_180719_HENRY__ */
