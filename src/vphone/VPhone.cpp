/*******************************************************************************
 *    File Name : VPhone.cpp
 * 
 *       Author : Henry He
 * Created Time : 2009-10-18 18:07:05
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "vphone/VPhone.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/



/******************************************************************************
 * Desc : Operations
 ******************************************************************************/







/******************************************************************************
 * Desc : Constructor / Deconstructor
 ******************************************************************************/






/******************************************************************************
 * Func : VPhone::VPhone
 * Desc : Constructor of Constructor
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
VPhone::VPhone ()
{
  m_zhmMutex.Init ();
}


/******************************************************************************
 * Func : VPhone::~ VPhone
 * Desc : Deconstructor of VPhone
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
VPhone::~ VPhone ()
{
  Stop ();
}





/******************************************************************************
 * Desc : Operations
 ******************************************************************************/



/******************************************************************************
 * Func : VPhone::Start
 * Desc : Start a communication
 * Args : zhsPeerAddr               address of peer, its port will be modified
 *        unTimestampInit           Initialize timestamp used by ludp sender
 * Outs : if success return 0, otherwise return error code
 ******************************************************************************/
int VPhone::Start (
    HLH_SockAddr zhsPeerAddr, UINT32 unTimestampInit,
    bool bUseAudioReceiver, bool bUseAudioSender,
    bool bUseVideoReceiver, bool bUseVideoSender )
{
  int               nRetVal;

  HLH_SockAddr      zhsAVAddr;

  void            * pvEncOutBuf;
  UINT32            unEncOutSize;
  void            * pvDecOutBuf;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_zvpState != VPHONE_STATE_IDLE) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("not in idle") );
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return VPHONE_ERR_WRONG_STATE;
  }

  // Create socket connection

  zhsAVAddr.SetAddr (INADDR_ANY, VPHONE_PORT_AVDATA);
  nRetVal = m_zhuAVSock.Create (zhsAVAddr);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create socket for media failed") );
    goto failed;
  }

  zhsPeerAddr.SetAddrPort (VPHONE_PORT_AVDATA);
  nRetVal = m_zhuAVSock.Connect (zhsPeerAddr);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("connect to peer media port failed") );
    goto failed;
  }

  // Save which component used
  m_bUseAudioReceiver = bUseAudioReceiver;
  m_bUseAudioSender   = bUseAudioSender;
  m_bUseVideoReceiver = bUseVideoReceiver;
  m_bUseVideoSender   = bUseVideoSender;



  ////////////////////////////////////////////////////////////////////////////////
  //
  //  Create audio communication
  // 
  ////////////////////////////////////////////////////////////////////////////////

  if (m_bUseAudioReceiver > 0) {
    // JitterBuffer for audio
    nRetVal = m_zjbAudioBuf.Create (
        VPHONE_AJB_PER_TIMESTAMP, VPHONE_AJB_JITTER_TIMESTAMP,
        OnAudioJitterBufferGet, this );

    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create jitters buffer for audio failed") );
      goto failed;
    }

    // LudpReceiver for audio
    nRetVal = m_zlrAudioReceiver.Create (
        m_zhuAVSock, LUDP_SLICE_BASE_TYPE_AUDIO,
        OnAudioLudpRecv, this,
        VPHONE_ALR_PKG_NUM,
        VPHONE_ALR_TIMETICK_US,
        VPHONE_ALR_TIMOUT_IN_TIMETICK,
        VPHONE_ALR_MAX_RETRY );

    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create ludp receiver for audio failed") );
      goto failed;
    }
  } // if (m_bUseAudioReceiver > 0)

  if (m_bUseAudioSender) {
    // LudpSender for audio
    nRetVal = m_zlsAudioSender.Create ( m_zhuAVSock,
        LUDP_SLICE_BASE_TYPE_AUDIO,
        unTimestampInit,
        VPHONE_ALS_PKG_NUM);

    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create ludp sender for audio failed") );
      goto failed;
    }
  } // if (m_bUseAudioSender)

  if (m_bUseAudioSender) {
    // Audio device (8bits, 8000 sample/s, read callback, 8 x (2^8) bytes buffer )
    nRetVal = m_zhaAudioDev.Create ( OnAudioRead, this, AFMT_U8, 8000, 1, 8, 8 );
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create audio device failed") );
      goto failed;
    }
  } else if (m_bUseAudioReceiver) {
    // Audio device (8bits, 8000 sample/s, no read callback, 8 x (2^8) bytes buffer )
    nRetVal = m_zhaAudioDev.Create ( NULL, NULL, AFMT_U8, 8000, 1, 8, 8 );
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create audio device failed") );
      goto failed;
    }
  } // if (m_bUseAudioReceiver)



  ////////////////////////////////////////////////////////////////////////////////
  //
  //  Create video communication
  //
  ////////////////////////////////////////////////////////////////////////////////



  if ( m_bUseVideoSender || m_bUseVideoReceiver ) {
    // Calculate frame size
    m_unFrameSize = VPHONE_H264_ENC_WIDTH * VPHONE_H264_ENC_HEIGHT * 3 / 2;


    //===========================  Video Encoder  ========================================

    // Create H264 encoder
    nRetVal = m_zheEncoder.Create (
        VPHONE_H264_ENC_WIDTH, VPHONE_H264_ENC_HEIGHT,
        VPHONE_H264_ENC_FRAME_RATE, VPHONE_H264_ENC_BIT_RATE,
        VPHONE_H264_ENC_GOP_NUM);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create H264 encoder failed") );
      goto failed;
    }


    memset (m_aucCameraBuf, 0, sizeof(m_aucCameraBuf));

    pvEncOutBuf = m_zheEncoder.Encode (m_aucCameraBuf, m_unFrameSize, unEncOutSize);
    if (pvEncOutBuf == NULL) {
      HLH_DEBUG ( HLH_DEBUG_MAIN, ("encode first frame failed") );
      goto failed;
    }


    //===========================  Video Decoder  ========================================


    // Create H264 decoder
    nRetVal = m_zhdDecoder.Create (pvEncOutBuf, unEncOutSize);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_MAIN, ("create H264 decoder failed") );
      goto failed;
    }

    pvDecOutBuf = m_zhdDecoder.Decode (pvEncOutBuf, unEncOutSize);
    if (pvDecOutBuf == NULL) {
      HLH_DEBUG ( HLH_DEBUG_MAIN, ("decode first frame failed") );
      goto failed;
    }

  } // if ( m_bUseVideoSender || m_bUseVideoReceiver )


  //===========================  Close encoder/decoder not needed  ===============

  if (!m_bUseVideoReceiver) {
    m_zhdDecoder.Destroy ();
  } // if (!m_bUseVideoReceiver)

  if (!m_bUseVideoSender) {
    m_zheEncoder.Destroy ();
  } // if (!m_bUseVideoSender)


  //===========================  Others for Video Receiver  ============================


  if (m_bUseVideoReceiver) {

    // JitterBuffer for video
    nRetVal = m_zjbVideoBuf.Create (
        VPHONE_VJB_PER_TIMESTAMP, VPHONE_VJB_JITTER_TIMESTAMP,
        OnVideoJitterBufferGet, this );

    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create jitters buffer for video failed") );
      goto failed;
    }

    // LudpReceiver for video
    nRetVal = m_zlrVideoReceiver.Create (
        m_zhuAVSock, LUDP_SLICE_BASE_TYPE_VIDEO,
        OnVideoLudpRecv, this,
        VPHONE_VLR_PKG_NUM,
        VPHONE_VLR_TIMETICK_US,
        VPHONE_VLR_TIMOUT_IN_TIMETICK,
        VPHONE_VLR_MAX_RETRY );

    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create ludp receiver for video failed") );
      goto failed;
    }

    // Frame buffer
    nRetVal = m_zfbFrameBuffer.Create ("/dev/fb0");
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create frame buffer failed") );
      goto failed;
    }

    // Post processor
    nRetVal = m_zppPostProcessor.Create ("/dev/misc/s3c-pp");
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create post processor failed") );
      goto failed;
    }

    nRetVal = m_zppPostProcessor.SetParam (
        VPHONE_H264_ENC_WIDTH, VPHONE_H264_ENC_HEIGHT,
        0, 0, VPHONE_H264_ENC_WIDTH, VPHONE_H264_ENC_HEIGHT, YC420,
        m_zfbFrameBuffer.GetVirtualWidth (), m_zfbFrameBuffer.GetVirtualHeight (),
        //0, 0, VPHONE_H264_ENC_WIDTH, VPHONE_H264_ENC_HEIGHT,
        0, 0, 480, 320,
        m_zfbFrameBuffer.GetColor () );
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("set parameter of post process failed") );
      goto failed;
    }

  } // if (m_bUseVideoReceiver)



  //===========================  Others for Video Sender  ==============================



  if (m_bUseVideoSender) {

    // LudpSender for video
    nRetVal = m_zlsVideoSender.Create ( m_zhuAVSock,
        LUDP_SLICE_BASE_TYPE_VIDEO,
        unTimestampInit,
        VPHONE_VLS_PKG_NUM);

    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create ludp sender for video failed") );
      goto failed;
    }

    // Camera for video
    nRetVal = m_zhvCamera.Create (HLH_VIDEO_V4L2_DEV_CODEC, V4L2_PIX_FMT_YUV420,
        VPHONE_H264_ENC_WIDTH, VPHONE_H264_ENC_HEIGHT);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create video device failed") );
      goto failed;
    }

    // Capture thread for camera
    nRetVal = m_zhtCapture.Start (CaptureThreadFunc, this);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("start capture thread failed") );
      goto failed;
    }

  } // if (m_bUseVideoSender)

  // Notify that this instance connected
  m_zvpState = VPHONE_STATE_CONNECTED;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return  0;

failed:
  // Socket connection
  m_zhuAVSock.Destroy ();

  if (m_bUseAudioReceiver) {
    // JitterBuffer for audio
    m_zjbAudioBuf.Destroy ();

    // LudpReceiver for audio
    m_zlrAudioReceiver.Destroy ();
  } // if (m_bUseAudioSender)

  if (m_bUseAudioSender) {
    // LudpSender for audio
    m_zlsAudioSender.Destroy ();
  } // if (m_bUseAudioSender)

  if (m_bUseAudioSender || m_bUseAudioReceiver) {
    // Audio device
    m_zhaAudioDev.Destroy ();
  } // if (m_bUseAudioSender || m_bUseAudioReceiver)


  if (m_bUseVideoReceiver) {
    m_zjbVideoBuf.Destroy ();
    m_zlrVideoReceiver.Destroy ();
  } // if (m_bUseVideoReceiver)

  if (m_bUseVideoSender) {
    m_zlsVideoSender.Destroy ();
  } // if (m_bUseVideoSender)

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return  VPHONE_ERR_FAILED;

}



/******************************************************************************
 * Func : VPhone::Stop
 * Desc : Stop a communication
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void VPhone::Stop ()
{
  HLH_ENTER_FUNC (HLH_DEBUG_VPHONE);

  // Lock this instance
  m_zhmMutex.Lock ();
  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after lock instance") );

  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("already stopping") );
    return;
  }

  // Prevent others to stop again
  m_zvpState = VPHONE_STATE_STOPPING;
  m_zhmMutex.Unlock ();


  // Destroy the audio/video socket
  m_zhuAVSock.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy A/V socket") );

  // Destroy audio communication

  if (m_bUseAudioReceiver) {
    // JitterBuffer for audio
    m_zjbAudioBuf.Destroy ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy audio jitter buffer") );

    // LudpReceiver for audio
    m_zlrAudioReceiver.Destroy ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy audio receiver") );

    // Audio device (8bits, 8000 sample/s, read callback, 8 x (2^8) bytes buffer )
    m_zhaAudioDev.Destroy ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy audio device") );
  } // if (m_bUseAudioReceiver)

  if (m_bUseAudioSender) {
    // LudpSender for audio
    m_zlsAudioSender.Destroy ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy audio sender") );

    // Audio device (8bits, 8000 sample/s, read callback, 8 x (2^8) bytes buffer )
    m_zhaAudioDev.Destroy ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy audio device") );
  } // if (m_bUseAudioSender)


  if (m_bUseVideoReceiver) {
    m_zjbVideoBuf.Destroy ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy video jitter buffer") );
    m_zlrVideoReceiver.Destroy ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy video ludp receiver") );

    m_zhdDecoder.Destroy ();
    m_zppPostProcessor.Destroy ();
    m_zfbFrameBuffer.Destroy ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy video receiver") );
  } // if (m_bUseVideoReceiver)

  if (m_bUseVideoSender) {
    m_zlsVideoSender.Destroy ();
    m_zhvCamera.Destroy ();

    m_zheEncoder.Destroy ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("after destroy video sender") );
  } // if (m_bUseVideoSender)

  // Notify this instance stopped
  m_zhmMutex.Lock ();
  m_zvpState = VPHONE_STATE_IDLE;
  m_zhmMutex.Unlock ();

  HLH_EXIT_FUNC (HLH_DEBUG_VPHONE);
}





/******************************************************************************
 * Desc : Callbacks
 ******************************************************************************/





/******************************************************************************
 * Func : VPhone::__OnAudioRead
 * Desc : Callback called when audio data ready
 * Args : pvBuf                     pointer to audio data read
 *        unLen                     length of audio data read
 * Outs : NONE
 ******************************************************************************/
void VPhone::__OnAudioRead (void *pvBuf, UINT32 unLen)
{

  // Lock this instance
  m_zhmMutex.Lock ();
  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Send this audio data if connected
  m_zlsAudioSender.SendPackage ( (UINT8*)pvBuf, unLen );

  // Unlock this instance
  m_zhmMutex.Unlock ();

}


/******************************************************************************
 * Func : OnAudioRead
 * Desc : Wrapper of \c __OnAudioRead ()
 * Args : pvThis                    pointer to this instance
 *        pvBuf                     pointer to audio data read
 *        unLen                     length of audio data read
 * Outs : NONE
 ******************************************************************************/
void VPhone::OnAudioRead (void *pvThis, void *pvBuf, UINT32 unLen)
{
  ASSERT (pvThis != NULL);

  ( (VPhone*) pvThis )->__OnAudioRead (pvBuf, unLen);
}


/******************************************************************************
 * Func : VPhone::__OnAudioJitterBufferGet
 * Desc : Callback called when JitterBuffer ready
 * Args : zjpPkg                  package ready in JitterBuffer
 * Outs : NONE
 ******************************************************************************/
void VPhone::__OnAudioJitterBufferGet (JPackage & zjpPkg)
{
  ReceiverPackage        *pzrpPkg;
  UINT16                  usSliceIt;
  UINT16                  usSliceNum;
  Slice                 **ppzsSlices;

  // Check arguments
  pzrpPkg = (ReceiverPackage *) zjpPkg.m_pvData;
  ASSERT (pzrpPkg != NULL);
  ASSERT (pzrpPkg->usSliceNum > 0);
  ASSERT (pzrpPkg->ppzsSlices != NULL);

#if HLH_DEBUG_CONFIG_VPHONE > 0
  static UINT32           unLastTimestamp = 0;
  if (unLastTimestamp + 1 != pzrpPkg->unTimestamp) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("jump from %u to %u", unLastTimestamp, pzrpPkg->unTimestamp) );
    unLastTimestamp = pzrpPkg->unTimestamp;
  } else {
    unLastTimestamp ++;
  }
#endif


  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Write put this data into audio device
  usSliceNum = pzrpPkg->usSliceNum;
  ppzsSlices = pzrpPkg->ppzsSlices;
  for (usSliceIt = 0; usSliceIt < usSliceNum; usSliceIt ++) {
    ASSERT ( ppzsSlices [usSliceIt] );
    m_zhaAudioDev.Write ( ppzsSlices[usSliceIt]->aucData,
        ppzsSlices[usSliceIt]->usDataLen );
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

}


/******************************************************************************
 * Func : VPhone::OnAudioJitterBufferGet
 * Desc : Wrapper of __OnAudioJitterBufferGet
 * Args : pvThis                        pointer to this instance
 *        zjpPkg                        package ready in JitterBuffer
 * Outs : NONE
 ******************************************************************************/
void VPhone::OnAudioJitterBufferGet (void * pvThis, JPackage & zjpPkg)
{
  ASSERT (pvThis != NULL);

  ( (VPhone*) pvThis )->__OnAudioJitterBufferGet (zjpPkg);
}


/******************************************************************************
 * Func : FreeReceivePackage
 * Desc : Free ReceiverPackage and its contents
 * Args : pzrpPkg                 package to be free
 * Outs : NONE
 ******************************************************************************/
static void FreeReceivePackage (void *pvRecvPkg)
{
  UINT16            usSliceIt;
  UINT16            usSliceNum;
  Slice           **ppzsSlices;
  ReceiverPackage  *pzrpPkg;

  pzrpPkg = (ReceiverPackage *) pvRecvPkg;

  if (pzrpPkg == NULL) {
    return;
  }

  // Free the slices
  ppzsSlices = pzrpPkg->ppzsSlices;
  if (ppzsSlices != NULL) {
    for (usSliceIt = 0, usSliceNum = pzrpPkg->usSliceNum;
        usSliceIt < usSliceNum; usSliceIt ++ ) {
      free ( ppzsSlices [usSliceIt] );
    }
    free (ppzsSlices);
  }

  // Frre the package itself
  free (pzrpPkg);

}

/******************************************************************************
 * Func : VPhone::__OnAudioLudpRecv
 * Desc : Callback called when Ludp receive package
 * Args : zrpPkg                  Package received by ludp receiver
 * Outs : NONE
 ******************************************************************************/
void VPhone::__OnAudioLudpRecv (ReceiverPackage &zrpPkg)
{
  ReceiverPackage     *pzrpPkgNew;

  // Lock this instance
  m_zhmMutex.Lock ();
  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Copy this ReceiverPackage
  pzrpPkgNew = (ReceiverPackage *) malloc ( sizeof(ReceiverPackage) );
  if (pzrpPkgNew == NULL) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("allocate receive package failed") );
    goto failed;
  }
  memcpy ( pzrpPkgNew, &zrpPkg, sizeof (ReceiverPackage) );

  // Put it into JitterBuffer
  m_zjbAudioBuf.PutPkg ( (HLH_RoundU32)pzrpPkgNew->unTimestamp, pzrpPkgNew, FreeReceivePackage );

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return;

failed:
  if (pzrpPkgNew != NULL) {
    free (pzrpPkgNew);
    pzrpPkgNew = NULL;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

}


/******************************************************************************
 * Func : VPhone::OnAudioLudpRecv
 * Desc : Wrapper of __OnAudioLudpRecv ()
 * Args : pvThis                  Pointer to this instance
 *        zrpPkg                  Package received by ludp receiver
 * Outs : NONE
 ******************************************************************************/
void VPhone::OnAudioLudpRecv (void *pvThis, ReceiverPackage &zrpPkg)
{
  ( (VPhone*) pvThis )->__OnAudioLudpRecv (zrpPkg);
}



/******************************************************************************
 * Func : VPhone::OnVideoLudpRecv
 * Desc : Handler of packages received by video ludp receiver
 * Args : pvThis                  Pointer to this instance
 *        zrpPkg                  Package received by ludp receiver
 * Outs : NONE
 ******************************************************************************/
void VPhone::__OnVideoLudpRecv (ReceiverPackage &zrpPkg)
{
  ReceiverPackage     *pzrpPkgNew;

  // Lock this instance
  m_zhmMutex.Lock ();
  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Copy this ReceiverPackage
  pzrpPkgNew = (ReceiverPackage *) malloc ( sizeof(ReceiverPackage) );
  if (pzrpPkgNew == NULL) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("allocate receive package failed") );
    goto failed;
  }
  memcpy ( pzrpPkgNew, &zrpPkg, sizeof (ReceiverPackage) );

  // Put it into JitterBuffer
  m_zjbVideoBuf.PutPkg ( (HLH_RoundU32)pzrpPkgNew->unTimestamp, pzrpPkgNew, FreeReceivePackage );

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return;

failed:
  if (pzrpPkgNew != NULL) {
    free (pzrpPkgNew);
    pzrpPkgNew = NULL;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

}


/******************************************************************************
 * Func : VPhone::OnVideoLudpRecv
 * Desc : Wrapper of __OnVideoLudpRecv ()
 * Args : pvThis                  Pointer to this instance
 *        zrpPkg                  Package received by ludp receiver
 * Outs : NONE
 ******************************************************************************/
void VPhone::OnVideoLudpRecv (void *pvThis, ReceiverPackage &zrpPkg)
{
  ( (VPhone*) pvThis )->__OnVideoLudpRecv (zrpPkg);
}



/******************************************************************************
 * Func : VPhone::__OnVideoJitterBufferGet
 * Desc : Handler for jitter buffer package
 * Args : pvThis                        pointer to this instance
 *        zjpPkg                        package ready in JitterBuffer
 * Outs : NONE
 ******************************************************************************/
void VPhone::__OnVideoJitterBufferGet (JPackage & zjpPkg)
{

  ReceiverPackage  *pzrpPkg;
  UINT16            usSliceIt;
  UINT16            usSliceNum;
  Slice           **ppzsSlices;
  UINT32						unLen;

  UINT8            *pucBuf;
  void 						 *pvDecOutBuf;

  // Check arguments
  pzrpPkg = (ReceiverPackage *) zjpPkg.m_pvData;
  ASSERT (pzrpPkg != NULL);
  ASSERT (pzrpPkg->usSliceNum > 0);
  ASSERT (pzrpPkg->ppzsSlices != NULL);

#if HLH_DEBUG_CONFIG_VPHONE > 0
  static UINT32           unLastTimestamp = 0;
  if (unLastTimestamp + 1 != pzrpPkg->unTimestamp) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("jump from %u to %u", unLastTimestamp, pzrpPkg->unTimestamp) );
    unLastTimestamp = pzrpPkg->unTimestamp;
  } else {
    unLastTimestamp ++;
  }
#endif


  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Put this data into H264 decoder input buffer
  pucBuf = m_aucDecodeBuf;
  usSliceNum = pzrpPkg->usSliceNum;
  ppzsSlices = pzrpPkg->ppzsSlices;
  for (unLen = 0, usSliceIt = 0; usSliceIt < usSliceNum; usSliceIt ++) {
    ASSERT ( ppzsSlices [usSliceIt] );
    unLen += ppzsSlices[usSliceIt]->usDataLen;
    if ( unLen > sizeof(m_aucDecodeBuf) ) {
      HLH_DEBUG ( HLH_DEBUG_MAIN, ("package too long, ignored") );
      goto failed;
    }
    memcpy (pucBuf, ppzsSlices[usSliceIt]->aucData,
        ppzsSlices[usSliceIt]->usDataLen );
    pucBuf += ppzsSlices[usSliceIt]->usDataLen;
  }

  // Decode this image
  pvDecOutBuf = m_zhdDecoder.Decode ( m_aucDecodeBuf, (UINT32)(pucBuf - m_aucDecodeBuf));
  if ( pvDecOutBuf == NULL ) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("decode failed") );
    goto failed;
  }

  // Show the image
  m_zppPostProcessor.Process ( (void*)pvDecOutBuf, m_zfbFrameBuffer.GetFrame(0) );

failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

}


/******************************************************************************
 * Func : VPhone::OnVideoJitterBufferGet
 * Desc : Wrapper of __OnVideoJitterBufferGet
 * Args : pvThis                        pointer to this instance
 *        zjpPkg                        package ready in JitterBuffer
 * Outs : NONE
 ******************************************************************************/
void VPhone::OnVideoJitterBufferGet (void * pvThis, JPackage & zjpPkg)
{
  ASSERT (pvThis != NULL);

  ( (VPhone*) pvThis )->__OnVideoJitterBufferGet (zjpPkg);
}



/******************************************************************************
 * Func : VPhone::__CaptureThreadFunc
 * Desc : Thread function to capture the video, encode and send it.
 * Args : zhtCaptureThread      capture thread to call this function
 * Outs : NONE
 ******************************************************************************/
void VPhone::__CaptureThreadFunc (HLH_Thread & zhtCaptureThread)
{
  int       nRetVal;
  void     *pvEncOutBuf;
  UINT32    unEncOutSize;
  HLH_Time  zhtTime;

  // Notify that this thread started
  zhtCaptureThread.ThreadStarted ();

  // Lock this instance
  m_zhmMutex.Lock ();
  if (m_zvpState != VPHONE_STATE_CONNECTED) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("not connected") );
    return;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  // Start capture
  nRetVal = m_zhvCamera.Start ();
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("start camera failed") );
    return;
  }

  // Capture video, encode and send it.

  do {
    // Wait for image ready
    zhtTime = HLH_TIME_FROM_USECS (VPHONE_TIMEOUT_POLL_CAMERA_READ_US);
    nRetVal = m_zhvCamera.TimedPollRead (zhtTime);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("poll to read camera timeout") );
      break;
    }

    // Read camera image
    nRetVal = m_zhvCamera.Read (m_aucCameraBuf, m_unFrameSize);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("read camera failed") );
      break;
    }

    // Encode this image
    pvEncOutBuf = m_zheEncoder.Encode (m_aucCameraBuf, m_unFrameSize, unEncOutSize);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("encode frame failed") );
      break;
    }

    // Send the stream
    nRetVal = m_zlsVideoSender.SendPackage ( (UINT8*)pvEncOutBuf, unEncOutSize );
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("send package failed, ignored") );
      // ignore this after all, may cause by lacked memory
    }

  } while ( !zhtCaptureThread.IsStopping () );

  HLH_DEBUG ( HLH_DEBUG_VPHONE, ("return from capture thread") );

}



/******************************************************************************
 * Func : VPhone::CaptureThreadFunc
 * Desc : Wrapper of __CaptureThreadFunc
 * Args : zhtCaptureThread      capture thread to call this function
 * Outs : always NULL.
 ******************************************************************************/
void * VPhone::CaptureThreadFunc (
    HLH_Thread    & zhtCaptureThread,
    void          * pvThis )
{
  ASSERT (pvThis != NULL);

  ( (VPhone*) pvThis )->__CaptureThreadFunc (zhtCaptureThread);

  return NULL;
}
