/*******************************************************************************
 *    File Name : JitterBuffer.cpp
 * 
 *       Author : Henry He
 * Created Time : 2009-10-14 10:13:41
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/

#include "JitterBuffer.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

//#define M_ZHRHIGHTIMESTAMP      m_zhrHighTimestamp//( HLH_RoundU32(*(volatile UINT32*)(m_zhrHighTimestamp.m_unVal)) )

#define M_ZHRHIGHTIMESTAMP      ( HLH_RoundU32(m_zhrHighTimestamp.GetUINT32()) )
#define M_ZHRRECVTIMESTAMP      ( HLH_RoundU32(m_zhrRecvTimestamp.GetUINT32()) )


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/





/******************************************************************************
 * Desc : Constructor / Deconstructor
 ******************************************************************************/





/******************************************************************************
 * Func : JitterBuffer::JitterBuffer
 * Desc : Constructor
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
JitterBuffer::JitterBuffer ()
{

  // Init mutexs and conditions
  m_zhmMutex.Init ();
  m_zhcPkgListCond.Init ();

  // Notify that this instance not created
  m_bCreated = false;

}


/******************************************************************************
 * Func : JitterBuffer::~JitterBuffer
 * Desc : Deconstructor of JitterBuffer
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
JitterBuffer::~JitterBuffer ()
{
  Destroy ();
}





/******************************************************************************
 * Desc : Operations
 ******************************************************************************/






/******************************************************************************
 * Func : JitterBuffer::Create
 * Desc : Create this instance
 * Args : unTimestampFirst          First timestamp will be received
 *        zhtPerTimestamp           Time interval between timestamp
 *        zhtJitterTime             Jitter time before ignore old packages
 * Outs : If success, return 0. otherwise return error code
 ******************************************************************************/
int JitterBuffer::Create (
    UINT32                  unPerTimestamp,
    UINT32                  unJitterTimestamp,
    OnJitterBufferGetFunc   zojOnJitterBufferGet,
    void                  * pvOnJitterBufferGetParam
    )
{
  ASSERT (unPerTimestamp < unJitterTimestamp);

  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

    HLH_DEBUG ( HLH_DEBUG_JITTER, ("already created") );
    return JITTER_BUFFER_ERR_ALREADY_CREATED;
  }


  // Init package list
  m_slpPkgList.clear ();

  // Init timestamp
  m_unJitterTimestamp = unJitterTimestamp;
  m_unPerTimestamp    = unPerTimestamp;

#if 0
  m_zhrRecvTimestamp  = unTimestampFirst;
  m_zhrHighTimestamp  = unTimestampFirst + unJitterTimestamp/2;
#else
  m_bPut = false;
#endif


  // Create Thread
  m_zojOnJitterBufferGet      = zojOnJitterBufferGet;
  m_pvOnJitterBufferGetParam  = pvOnJitterBufferGetParam;

  if (zojOnJitterBufferGet != NULL) {
    HLH_DEBUG ( HLH_DEBUG_JITTER, ("start get thread") );
    m_zhtGetThread.Start (GetThreadFunc, this);
  }


  // Notify that this instance is created
  m_bCreated = true;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

}




/******************************************************************************
 * Func : JitterBuffer::Destroy
 * Desc : Destroy this Jitter Buffer, and stop thread started
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void JitterBuffer::Destroy ()
{
  std::list<JPackage>::iterator   slpIt;
  JReleaseFunc                    zjrRelease;


  // Stop get thread if started
  m_zhtGetThread.Stop ();

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

    HLH_DEBUG ( HLH_DEBUG_JITTER, ("not created") );
    return;
  }


  // Clear Pkg List
  for ( slpIt = m_slpPkgList.begin(); slpIt != m_slpPkgList.end(); slpIt ++ ) {
    zjrRelease = slpIt->m_zjrRelease;

    if (zjrRelease != NULL) {
      zjrRelease (slpIt->m_pvData);
    }
  }

  m_slpPkgList.clear ();

  // Notify that this instance not created
  m_bCreated = false;

  // Unlock this instance
  m_zhmMutex.Unlock ();
}



/******************************************************************************
 * Func : JitterBuffer::IsCreated
 * Desc : Whether this instance created
 * Args : NONE
 * Outs : If created return 0, otherwise return error code.
 ******************************************************************************/
bool JitterBuffer::IsCreated ()
{
  bool bCreated;

  m_zhmMutex.Lock ();
  bCreated = m_bCreated;
  m_zhmMutex.Unlock ();

  return bCreated;
}





/******************************************************************************
 * Func : JitterBuffer::PutPkg
 * Desc : Put one package into this JitterBuffer
 * Args : zhrTimestamp              timestamp of package
 *        pvData                    pointer of package
 *        zjrRelease                release function for pvData
 *                                  (can be NULL if no release need)
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int JitterBuffer::PutPkg ( HLH_RoundU32 zhrTimestamp,
    void *pvData, JReleaseFunc zjrRelease )
{

  std::list<JPackage>::iterator   slpIt;
  JPackage                        zjpPkgNew;
  HLH_RoundU32                    zhrTimestamp2Recv;
  bool                            bSignal = false;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_JITTER, ("not created") );
    return JITTER_BUFFER_ERR_NOT_CREATED;
  }

  // Initialize timestamp range to receive
  if (!m_bPut) {
    m_bPut = true;
    m_zhrRecvTimestamp  = (UINT32)zhrTimestamp - m_unJitterTimestamp/2;
    m_zhrHighTimestamp  = zhrTimestamp;
    HLH_DEBUG ( HLH_DEBUG_JITTER,
        ( "Init m_zhrRecvTimestamp = %u, m_zhrHighTimestamp = %u",
          (UINT32)m_zhrRecvTimestamp, (UINT32)m_zhrHighTimestamp ) );
  }

  HLH_DEBUG ( HLH_DEBUG_JITTER, ("put pkg %u", (UINT32)zhrTimestamp) );
  // Check if zhrTimestamp is old
  if (zhrTimestamp < m_zhrRecvTimestamp) {
    HLH_DEBUG ( HLH_DEBUG_JITTER, ("XXX: ignore old package") );
    // Just ignore this old package
    goto failed;
  }

  // Search for the position to insert
  for ( slpIt = m_slpPkgList.begin (); slpIt != m_slpPkgList.end (); slpIt ++ ) {
    if (zhrTimestamp <= slpIt->m_zhrTimestamp) {
      break;
    }
  }

  // Get the position now 
  // Insert the package into list

  if (slpIt != m_slpPkgList.end () && zhrTimestamp == slpIt->m_zhrTimestamp) {
    // XXX: should not happed after all, just ignore this here
    HLH_DEBUG ( HLH_DEBUG_JITTER, ("XXX %u already received", (UINT32)zhrTimestamp) );
    goto failed;
  } else {
    zjpPkgNew.m_zhrTimestamp = zhrTimestamp;
    zjpPkgNew.m_pvData       = pvData;
    zjpPkgNew.m_zjrRelease   = zjrRelease;

    m_slpPkgList.insert (slpIt, zjpPkgNew);
  }


  // Goto the receive pointer
  for ( slpIt = m_slpPkgList.begin ();
      slpIt != m_slpPkgList.end () && slpIt->m_zhrTimestamp < m_zhrRecvTimestamp;
      slpIt ++) {
  }


  // Update \c m_zhrRecvTimestamp and \c m_zhtRecvTime if need
  for ( zhrTimestamp2Recv = m_zhrRecvTimestamp;
      slpIt != m_slpPkgList.end () && zhrTimestamp2Recv == slpIt->m_zhrTimestamp;
      slpIt ++, zhrTimestamp2Recv += m_unPerTimestamp ) {
  }


  if (slpIt == m_slpPkgList.end() || zhrTimestamp2Recv != m_zhrRecvTimestamp) {
    // At least one oldest package received
    m_zhrRecvTimestamp = zhrTimestamp2Recv;

    // Signal GetPkg ()
    HLH_DEBUG ( HLH_DEBUG_JITTER, ("package ready before %d", (UINT32)zhrTimestamp2Recv) );
    bSignal = true;
  }

  if (zhrTimestamp > m_zhrHighTimestamp) {
    m_zhrHighTimestamp = zhrTimestamp;
    bSignal = true;
  }

  if (bSignal) {
    HLH_DEBUG ( HLH_DEBUG_JITTER, ("put signal") );
    m_zhcPkgListCond.Signal ();
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

  return JITTER_BUFFER_ERR_FAILED;

}

/******************************************************************************
 * Func : JitterBuffer::GetPkg
 * Desc : Get one package from this JitterBuffer
 * Args : zhtWait                   Time to wait before success
 * Outs : If success, return one package,
 *        otherwise return package with null content.
 ******************************************************************************/
JPackage JitterBuffer::GetPkg (HLH_Time &zhtWaitTime)
{
  int          nRetVal;
  JPackage     zjpPkg;
  HLH_RoundU32 zhrHighTimestamp;

  std::list<JPackage>::iterator  slpIt;


  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    HLH_DEBUG ( HLH_DEBUG_JITTER, ("not created") );
    goto failed;
  }


  // If package not empty
  if ( !m_slpPkgList.empty () ) {

    zjpPkg = m_slpPkgList.front ();

    zhrHighTimestamp = M_ZHRHIGHTIMESTAMP;

    // Check if receive timestamp out of date
    if (M_ZHRRECVTIMESTAMP < zhrHighTimestamp - (HLH_RoundU32)m_unJitterTimestamp) {
      // Reset receive timestamp
      HLH_DEBUG ( HLH_DEBUG_TEST|HLH_DEBUG_JITTER,
          ("reset jitter from %u to %u", (UINT32)M_ZHRRECVTIMESTAMP, (UINT32)zhrHighTimestamp - m_unJitterTimestamp/2) );
      m_zhrRecvTimestamp = (UINT32)zhrHighTimestamp - m_unJitterTimestamp/2;
    }

    // Check if already anti jitter
    if (zjpPkg.m_zhrTimestamp < M_ZHRRECVTIMESTAMP) {
      m_slpPkgList.pop_front ();
      HLH_DEBUG ( HLH_DEBUG_TEST|HLH_DEBUG_JITTER, ("get pkg %u", (UINT32)zjpPkg.m_zhrTimestamp) );
      goto finished;
    }

    // Wait for receive
    goto wait_recv;

  } else {

    zhrHighTimestamp = M_ZHRHIGHTIMESTAMP;

    // Check if receive timestamp out of date
    if (M_ZHRRECVTIMESTAMP < zhrHighTimestamp - (HLH_RoundU32)m_unJitterTimestamp) {
      // Reset receive timestamp
      HLH_DEBUG ( HLH_DEBUG_TEST|HLH_DEBUG_JITTER,
          ("reset jitter from %u to %u", (UINT32)M_ZHRRECVTIMESTAMP, (UINT32)zhrHighTimestamp - m_unJitterTimestamp/2) );
      m_zhrRecvTimestamp = (UINT32)zhrHighTimestamp - m_unJitterTimestamp/2;
    }

wait_recv:

    // Wait for packages
    nRetVal = m_zhcPkgListCond.TimeWait (m_zhmMutex, zhtWaitTime);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_JITTER|HLH_DEBUG_MAIN, ("timeout") );
      goto failed;
    }

    // Check if package availble
    if ( !m_slpPkgList.empty () ) {
      zjpPkg = m_slpPkgList.front ();

      zhrHighTimestamp = M_ZHRHIGHTIMESTAMP;

      // Check if receive timestamp out of date
      if (M_ZHRRECVTIMESTAMP < zhrHighTimestamp - (HLH_RoundU32)m_unJitterTimestamp) {
        // Reset receive timestamp
        HLH_DEBUG ( HLH_DEBUG_TEST|HLH_DEBUG_JITTER,
            ("reset jitter from %u to %u", (UINT32)M_ZHRRECVTIMESTAMP, (UINT32)zhrHighTimestamp - m_unJitterTimestamp/2) );
        m_zhrRecvTimestamp = (UINT32)zhrHighTimestamp - m_unJitterTimestamp/2;
      }

      // Check if already anti jitter
      if (zjpPkg.m_zhrTimestamp < M_ZHRRECVTIMESTAMP) {
        m_slpPkgList.pop_front ();
        HLH_DEBUG ( HLH_DEBUG_TEST|HLH_DEBUG_JITTER, ("get pkg %u", (UINT32)zjpPkg.m_zhrTimestamp) );
        goto finished;
      }
    }

    goto failed;
  }

finished:
  // Unlock this instance
  m_zhmMutex.Unlock ();

  return zjpPkg;

failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

  memset ( &zjpPkg, 0, sizeof(JPackage) );
  return zjpPkg;
}




/******************************************************************************
 * Func : JitterBuffer::__GetThreadFunc
 * Desc : Thread function to call OnJitterBufferGet () 
 *            when JitterBuffer ready.
 * Args : zhtGetThread                Thread to call this function
 * Outs : NONE
 ******************************************************************************/
void JitterBuffer::__GetThreadFunc (HLH_Thread & zhtGetThread)
{
  JPackage      zjpPkg;
  HLH_Time      zhtTime;

  zhtGetThread.ThreadStarted ();

  // Check if this instance created

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();


  do {

    // Wait for package
    zhtTime.SetTime (JITTER_BUFFER_TIMEOUT_GET_PKG_SECONDS, 0);
    zjpPkg = GetPkg (zhtTime);

    // Check if JitterBuffer ready
    if (zjpPkg.m_pvData != NULL) {
      HLH_DEBUG ( HLH_DEBUG_JITTER,
          ("get pkg %u", (UINT32)zjpPkg.m_zhrTimestamp) );
      m_zojOnJitterBufferGet (m_pvOnJitterBufferGetParam, zjpPkg);

      // Check if need to release zjpPkg.m_pvData
      if (zjpPkg.m_zjrRelease != NULL) {
        zjpPkg.m_zjrRelease (zjpPkg.m_pvData);
      }
    }

    // Exit this thread if stop request
    if ( zhtGetThread.IsStopping () ) {
      HLH_DEBUG ( HLH_DEBUG_JITTER|HLH_DEBUG_TEST, ("stop on request") );
      break;
    }

  } while (1);

}

/******************************************************************************
 * Func : JitterBuffer::GetThreadFunc
 * Desc : Wrapper of JitterBuffer::__GetThreadFunc to called by thread
 * Args : zhtGetThread                Thread to call this function
 *        pvThis                      pointer to this instance
 * Outs : Alway return (void*) 0.
 ******************************************************************************/
void * JitterBuffer::GetThreadFunc (HLH_Thread & zhtGetThread, void *pvThis)
{
  ASSERT (pvThis != NULL);
  ( (JitterBuffer *) pvThis )->__GetThreadFunc (zhtGetThread);

  return  ( (void*) 0 );
}

