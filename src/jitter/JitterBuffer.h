/*******************************************************************************
 *         File : JitterBuffer.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-14 10:13:33
 *  Description : 
 ******************************************************************************/

#ifndef __JITTERBUFFER_INC_20091014_101333_HENRY__
#define __JITTERBUFFER_INC_20091014_101333_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "utils/common.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Error code

#define JITTER_BUFFER_ERR_FAILED                          (-1)
#define JITTER_BUFFER_ERR_CANT_CREATE                     (-2)
#define JITTER_BUFFER_ERR_NOT_CREATED                     (-3)
#define JITTER_BUFFER_ERR_ALREADY_CREATED                 (-4)


////////////////////////////////////////////////////////////////////////////////
// Defaults

#define JITTER_BUFFER_TIMEOUT_GET_PKG_SECONDS             1

/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


////////////////////////////////////////////////////////////////////////////////
// Release function for JPackage::m_pvData
typedef void (*JReleaseFunc) (void *pvData);


// Structure to store package of JitterBuffer
typedef struct tagJPackage
{
  HLH_RoundU32  m_zhrTimestamp;
  void         *m_pvData;
  JReleaseFunc  m_zjrRelease;
} JPackage;



////////////////////////////////////////////////////////////////////////////////
// Callback function called when JitterBuffer ready
typedef void (* OnJitterBufferGetFunc) (void * pvParam, JPackage & zjpPkg);



/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/






/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Jitter buffer for real time transfer

class JitterBuffer 
{

  public:
    /******************************************************************************
     * Desc : Constructor / Deconstructor
     ******************************************************************************/

    // Constructor
    JitterBuffer ();

    // Deconstructor
    ~JitterBuffer ();

  public:
    /******************************************************************************
     * Desc : Operations
     ******************************************************************************/

    // Create this instance
    int Create (
        UINT32                  unPerTimestamp,
        UINT32                  unJitterTimestamp,
        OnJitterBufferGetFunc   zojOnJitterBufferGet = NULL,
        void                  * pvOnJitterBufferGetParam = NULL
        );

    // Destroy this Jitter Buffer, and stop thread started
    void Destroy ();

    // Whether this instance created
    bool IsCreated ();

    // Put one package into this JitterBuffer
    int PutPkg ( HLH_RoundU32 zhrTimestamp,
        void *pvData, JReleaseFunc zrfRelease );

    // Peek one package from this JitterBuffer
    JPackage GetPkg (HLH_Time &zhtWaitTime);


  private:
    /******************************************************************************
     * Desc : Private Operations
     ******************************************************************************/
    // Thread function to call OnJitterBufferGet () when JitterBuffer ready.
    void __GetThreadFunc (HLH_Thread & zhtGetThread);

    // Thread function to call OnJitterBufferGet () when JitterBuffer ready. (wrapper)
    static void * GetThreadFunc (HLH_Thread & zhtGetThread, void *pvThis);


  private:
    /******************************************************************************
     * Desc : Private data
     ******************************************************************************/


    ////////////////////////////////////////////////////////////////////////////////
    // Mutex of this instance
    HLH_Mutex               m_zhmMutex;

    // Whether this instance created
    bool                    m_bCreated;

    // Whether first package put
    bool                    m_bPut;

    ////////////////////////////////////////////////////////////////////////////////
    // Jitter buffer
    std::list<JPackage>     m_slpPkgList;

    // Jitter buffer condition, bind with mutex \c m_zhmMutex
    HLH_Cond                m_zhcPkgListCond;



    ////////////////////////////////////////////////////////////////////////////////
    // Jitter Timestamp before ignore old packages
    UINT32                  m_unJitterTimestamp;

    // Timestamp interval between timestamps
    UINT32                  m_unPerTimestamp;

    // Highest timestamp received
    HLH_RoundU32            m_zhrHighTimestamp;

    // Timestamp to received
    HLH_RoundU32            m_zhrRecvTimestamp;


    ////////////////////////////////////////////////////////////////////////////////
    // Thread to call m_zojOnJitterBufferGet
    HLH_Thread              m_zhtGetThread;

    // Callback function to called when JitterBuffer ready
    OnJitterBufferGetFunc   m_zojOnJitterBufferGet;

    // Parameter for m_zojOnJitterBufferGet
    void                  * m_pvOnJitterBufferGetParam;


};


#endif /* __JITTERBUFFER_INC_20091014_101333_HENRY__ */
