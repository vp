/*******************************************************************************
 *         File : HLH_FrameBuffer.h
 * 
 *       Author : Henry He
 *      Created : Sat 24 Oct 2009 03:08:41 PM CST
 *  Description : 
 ******************************************************************************/

#ifndef __HLH_FRAMEBUFFER_INC_20091024_150841_HENRY__
#define __HLH_FRAMEBUFFER_INC_20091024_150841_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  <linux/fb.h>
#include  <sys/mman.h>

#include  "utils/common.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


//===========================  YUV to RGB convert matrix index  ================

#define HLH_FRAMEBUFFER_MATRIX_COEF_INDEX           6


//===========================  Max Values  =====================================

#define HLH_FRAMEBUFFER_MAX_SAMPLE    255


//===========================  Error Code  =====================================

#define HLH_FRAMEBUFFER_ERR_FAILED                  (-1)
#define HLH_FRAMEBUFFER_ERR_CANT_CREATE             (-2)
#define HLH_FRAMEBUFFER_ERR_ALREADY_CREATED         (-3)
#define HLH_FRAMEBUFFER_ERR_NOT_CREATE              (-4)




/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/





/******************************************************************************
 * Class : HLH_FrameBuffer
 * Desc  : Wrapper of framebuffer
 ******************************************************************************/

class HLH_FrameBuffer
{

	//friend int main ();

	public:

	// ====================  LIFECYCLE    ========================================= 

	// Constructor of HLH_FrameBuffer
	HLH_FrameBuffer ();

	// Decontructor of HLH_FrameBuffer
	~ HLH_FrameBuffer ();


	public:

	// ====================  OPERATORS    ========================================= 

	// ====================  OPERATIONS   ========================================= 

	// Whether this frame buffer created
	bool IsCreated ();

	// Create frame buffer
	int Create (const char *pcFB);

	// Destroy this frame buffer
	void Destroy ();

	// Show image of format YUV422P on this framebuffer
	void ShowYUV422PImage (
			UINT8    *pucBufY,
			UINT8    *pucBufU,
			UINT8    *pucBufV,
			UINT32    unSrcX,
			UINT32    unSrcY,
			UINT32    unSrcWidth,
			UINT32    unSrcHeight,
			UINT32    unSrcWidthPad,
			UINT32    unDstX,
			UINT32    unDstY
			);

	// Show image of format YUV420 on this framebuffer
	void ShowYUV420Image (
			UINT8    *pucBufY,
			UINT8    *pucBufU,
			UINT8    *pucBufV,
			UINT32    unSrcX,
			UINT32    unSrcY,
			UINT32    unSrcWidth,
			UINT32    unSrcHeight,
			UINT32    unSrcWidthPad,
			UINT32    unDstX,
			UINT32    unDstY
			);


	// ====================  ACCESS       ========================================= 


	private:

	// ====================  OPERATIONS   ========================================= 

	// Build YUV to RGB lookup tables
	void BuildYcc2RGB565Table ();

	// Convert image of format YUV422P to RGB565
	void ConvertYUV422PToRGB565 (
			UINT8    *pucBufY,
			UINT8    *pucBufU,
			UINT8    *pucBufV,
			UINT32    unSrcWidth,
			UINT32    unSrcHeight,
			UINT32    unSrcWidthPad,

			UINT16   *pusBufRGB,
			UINT32    unDstX,
			UINT32    unDstY,
			UINT32    unDstWidth,
			UINT32    unDstHeight
			);

	// Convert image of format YUV420 to RGB565
	void ConvertYUV420ToRGB565 (
			UINT8    *pucBufY,
			UINT8    *pucBufU,
			UINT8    *pucBufV,
			UINT32    unSrcWidth,
			UINT32    unSrcHeight,
			UINT32    unSrcWidthPad,

			UINT16   *pusBufRGB,
			UINT32    unDstX,
			UINT32    unDstY,
			UINT32    unDstWidth,
			UINT32    unDstHeight
			);

	private:

	// ====================  Member Data  ========================================= 

	// Mutex for this instance
	HLH_Mutex      m_zhmMutex;

	// Whether this instance created
	bool           m_bCreated;

	// File descriptor of framebuffer
	int            m_fdFB;

	// mapped buffer of frame buffer
	UINT16        *m_pusFrame;
	UINT16        *m_pusFrameBuf;

	// Width and height of FrameBuffer
	UINT32         m_unFBWidth;
	UINT32         m_unFBHeight;

	// Variable srceen infomation
	struct fb_var_screeninfo m_fvsVarScrInfo;

	//=========================== Static Tables ==============================

	static void   *m_apvTableV2R[HLH_FRAMEBUFFER_MAX_SAMPLE+1];
	static void   *m_apvTableU2G[HLH_FRAMEBUFFER_MAX_SAMPLE+1];
	static int     m_anTableV2G [HLH_FRAMEBUFFER_MAX_SAMPLE+1];
	static void   *m_apvTableU2B[HLH_FRAMEBUFFER_MAX_SAMPLE+1];
	static UINT16  m_ausTable16 [197 + 2 *682 + 256 + 132];


};  // -----  end of class  HLH_FrameBuffer  ----- 




#endif /* __HLH_FRAMEBUFFER_INC_20091024_150841_HENRY__ */
