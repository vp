/*******************************************************************************
 *    File Name : HLH_FrameBuffer.cpp
 * 
 *       Author : Henry He
 * Created Time : Sat 24 Oct 2009 03:08:45 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "video/HLH_FrameBuffer.h"



/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

//===========================  Convert Macros  =================================

#define RGB(i)                                                       \
  do {                                                               \
    nU  = pucU[i];                                                   \
    nV  = pucV[i];                                                   \
    pusR = (UINT16 *) ppvTableV2R[nV];                               \
    pusG = (UINT16 *) (((UINT8 *)ppvTableU2G[nU]) + pnTableV2G[nV]); \
    pusB = (UINT16 *) ppvTableU2B[nU];                               \
  } while (0)

#define DST(pucY, pusD, i)                                           \
  do {                                                               \
    nY = pucY[i];                                                    \
    pusD[i] = pusR[nY] + pusG[nY] + pusB[nY];                        \
  } while (0)



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/
void   *HLH_FrameBuffer::m_apvTableV2R [HLH_FRAMEBUFFER_MAX_SAMPLE+1];
void   *HLH_FrameBuffer::m_apvTableU2G [HLH_FRAMEBUFFER_MAX_SAMPLE+1];
int     HLH_FrameBuffer::m_anTableV2G  [HLH_FRAMEBUFFER_MAX_SAMPLE+1];
void   *HLH_FrameBuffer::m_apvTableU2B [HLH_FRAMEBUFFER_MAX_SAMPLE+1];
UINT16  HLH_FrameBuffer::m_ausTable16  [197 + 2*682 + 256 + 132];



/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/

//===========================  Const Tables  ===================================

static const int anInverse_Table_6_9[8][4] = {
  {117504, 138453, 13954, 34903}, /* no sequence_display_extension */
  {117504, 138453, 13954, 34903}, /* ITU-R Rec. 709 (1990)         */
  {104597, 132201, 25675, 53279}, /* unspecified                   */
  {104597, 132201, 25675, 53279}, /* reserved                      */
  {104448, 132798, 24759, 53109}, /* FCC                           */
  {104597, 132201, 25675, 53279}, /* ITU-R Rec. 624-4 System B, G  */
  {104597, 132201, 25675, 53279}, /* SMPTE 170M                    */
  {117579, 136230, 16907, 35559}  /* SMPTE 240M (1987)             */
};














/******************************************************************************
 * Func : div_round
 * Desc : Round up divide
 * Args : dividend      Dividend of divide
 *        divisor       Divisor of divide
 * Outs : Result of divide rounded up
 ******************************************************************************/
static inline int div_round (int dividend, int divisor)
{
  if (dividend > 0)
    return (dividend + (divisor>>1)) / divisor;
  else
    return -((-dividend + (divisor>>1)) / divisor);
}




/******************************************************************************
 * Desc : Member Functions
 ******************************************************************************/





/******************************************************************************
 * Func : HLH_FrameBuffer::HLH_FrameBuffer
 * Desc : Constructor of HLH_FrameBuffer
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_FrameBuffer::HLH_FrameBuffer ()
{
  HLH_ENTER_FUNC (HLH_DEBUG_VIDEO);

  m_zhmMutex.Init ();
  m_bCreated = false;
  BuildYcc2RGB565Table ();

  HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);
}   // -----  end of method HLH_FrameBuffer::HLH_FrameBuffer  (constructor)  ----- 


/******************************************************************************
 * Func : HLH_FrameBuffer::~HLH_FrameBuffer
 * Desc :  Deconstructor of HLH_FrameBuffer
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
HLH_FrameBuffer::~HLH_FrameBuffer ()
{
  HLH_ENTER_FUNC (HLH_DEBUG_VIDEO);

  Destroy ();

  HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);
}   // -----  end of method HLH_FrameBuffer::~HLH_FrameBuffer  (deconstructor)  ----- 



/******************************************************************************
 * Func : HLH_FrameBuffer::IsCreated
 * Desc : Whether this instance created
 * Args : NONE
 * Outs : If created nRetValurn true, otherwise nRetValurn false
 ******************************************************************************/
bool HLH_FrameBuffer::IsCreated ()
{
  bool bCreated;

  HLH_ENTER_FUNC (HLH_DEBUG_VIDEO);

  m_zhmMutex.Lock ();
  bCreated = m_bCreated;
  m_zhmMutex.Unlock ();

  HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

  return bCreated;
}


/******************************************************************************
 * Func : HLH_FrameBuffer::Create
 * Desc : Create this instance
 * Args : pcFB                  Path of framebuffer device
 * Outs : If success return 0, otherwise return error code.
 ******************************************************************************/
int HLH_FrameBuffer::Create (const char *pcFB)
{
  int nRetVal;

  HLH_ENTER_FUNC (HLH_DEBUG_VIDEO);


  // Fix srceen infomation
  struct fb_fix_screeninfo ffsFixScrInfo;


  ASSERT (pcFB != NULL);

  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

    HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

    return HLH_FRAMEBUFFER_ERR_ALREADY_CREATED;
  }

  // Open framebuffer device
  m_fdFB = open ( pcFB, O_RDWR );
  if (m_fdFB == -1) {
    goto failed;
  }

  // Get fix screen info
  nRetVal = ioctl (m_fdFB, FBIOGET_FSCREENINFO, &ffsFixScrInfo);
  if (nRetVal < 0) {
    goto failed;
  }

  // Get variable screen info
  nRetVal = ioctl (m_fdFB, FBIOGET_VSCREENINFO, &m_fvsVarScrInfo);
  if(nRetVal < 0) {
    goto failed;
  }

  Printf ("screen: %d X %d X %db\n", m_fvsVarScrInfo.xres,
      m_fvsVarScrInfo.yres, m_fvsVarScrInfo.bits_per_pixel);

  // Check on bits per pixel
  // : XXX we only support 16bpp now
  if (m_fvsVarScrInfo.bits_per_pixel != 16) {
    Printf ("HLH_FrameBuffer: Only support frame buffer with RGB565\n");
    goto failed;
  }

  // Set frame width and height
  m_unFBWidth  = m_fvsVarScrInfo.xres;
  m_unFBHeight = m_fvsVarScrInfo.yres;

  // Try to get double buffer
  m_pusFrame = (UINT16 *) mmap ( 0,
      m_unFBWidth * m_unFBHeight * sizeof(UINT16),
      PROT_READ|PROT_WRITE, MAP_SHARED, m_fdFB, 0 );
  if (m_pusFrame == NULL) {
    Printf ("HLH_FrameBuffer: Can't map frame buffer.\n");
    goto failed;
  }

  // Buffer of frame buffer
  m_pusFrameBuf = (UINT16 *) malloc ( m_unFBWidth * m_unFBHeight * sizeof(UINT16) );
  if (m_pusFrameBuf == NULL) {
    goto failed;
  }

  // Notify that this instance created
  m_bCreated = true;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

  return 0;

failed:
  // Close framebuffer if failed
  if (m_fdFB >= 0) {
    close (m_fdFB);
    m_fdFB = -1;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

  return HLH_FRAMEBUFFER_ERR_CANT_CREATE;

}



/******************************************************************************
 * Func : HLH_FrameBuffer::Destroy
 * Desc : Destroy this instance
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void HLH_FrameBuffer::Destroy ()
{

  HLH_ENTER_FUNC (HLH_DEBUG_VIDEO);

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Lock ();

    HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

    return;
  }

  close (m_fdFB);
  m_fdFB = -1;


  HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

  // Unlock this instance
  m_zhmMutex.Lock ();
}


/******************************************************************************
 * Func : HLH_FrameBuffer::BuildYcc2RGB565Table
 * Desc : Build YUV to RGB lookup tables
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void HLH_FrameBuffer::BuildYcc2RGB565Table ()
{
  int      i;
  int      j;

  UINT8    pucTableY[1024];
  int      nEntrySize;

  void    *pvTableR;
  void    *pvTableG;
  void    *pvTableB;

  int crv =  anInverse_Table_6_9 [HLH_FRAMEBUFFER_MATRIX_COEF_INDEX][0];
  int cbu =  anInverse_Table_6_9 [HLH_FRAMEBUFFER_MATRIX_COEF_INDEX][1];
  int cgu = -anInverse_Table_6_9 [HLH_FRAMEBUFFER_MATRIX_COEF_INDEX][2];
  int cgv = -anInverse_Table_6_9 [HLH_FRAMEBUFFER_MATRIX_COEF_INDEX][3];


  HLH_ENTER_FUNC (HLH_DEBUG_VIDEO);

  for (i = 0; i < 1024; i++) {
    j = (76309 * (i - 384 - 16) + 32768) >> 16;
    pucTableY[i] = (j < 0) ? 0 : ((j > 255) ? 255 : j);
  }

  pvTableR   = m_ausTable16 + 197;
  pvTableG   = m_ausTable16 + 197 + 685;
  pvTableB   = m_ausTable16 + 197 + 2*682;
  nEntrySize = sizeof (UINT16);

  for (i = -197; i < 256+197; i++) {
    ((UINT16 *)pvTableR)[i] = (pucTableY[i+384] >> 3) << 11;
  }

  for (i = -132; i < 256+132; i++) {
    ((UINT16 *)pvTableG)[i] = (pucTableY[i+384] >> 2) << 5;
  }

  for (i = -232; i < 256+232; i++) {
    ((UINT16 *)pvTableB)[i] = pucTableY[i+384] >> 3;
  }

  for (i = 0; i < 256; i++) {
    m_apvTableV2R[i] = (((UINT8 *)pvTableR) +
        nEntrySize * div_round (crv * (i-128), 76309));
    m_apvTableU2G[i] = (((UINT8 *)pvTableG) +
        nEntrySize * div_round (cgu * (i-128), 76309));
    m_anTableV2G[i]  = nEntrySize * div_round (cgv * (i-128), 76309);
    m_apvTableU2B[i] = (((UINT8 *)pvTableB) +
        nEntrySize * div_round (cbu * (i-128), 76309));
  }

  HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

}



/******************************************************************************
 * Func : HLH_FrameBuffer::ConvertYUV422PToRGB565
 * Desc : Convert image of format YUV422P to RGB565
 * Args : pucBufY               Buffer of Y component
 *        pucBufU               Buffer of U component
 *        pucBufV               Buffer of V component
 *        unSrcWidth            Width of source image
 *        unSrcHeight           Height of source image
 *        unSrcWidthPad         Width padding of source image
 *        pusBufRGB             Buffer of RGB565
 *        unDstX                Left offset of destination image
 *        unDstY                Top offset of destination image
 *        unDstWidth            Width of destination image
 *        unDstHeight           Height of destination image
 * Outs : NONE
 ******************************************************************************/
void HLH_FrameBuffer::ConvertYUV422PToRGB565 (
    UINT8    *pucBufY,
    UINT8    *pucBufU,
    UINT8    *pucBufV,
    UINT32    unSrcWidth,
    UINT32    unSrcHeight,
    UINT32    unSrcWidthPad,

    UINT16   *pusBufRGB,
    UINT32    unDstX,
    UINT32    unDstY,
    UINT32    unDstWidth,
    UINT32    unDstHeight
    )
{

  register void        **ppvTableV2R = m_apvTableV2R;
  register void        **ppvTableU2G = m_apvTableU2G;
  register int          *pnTableV2G  = m_anTableV2G;
  register void        **ppvTableU2B = m_apvTableU2B;

  register const UINT8  *pucY        = pucBufY;
  register const UINT8  *pucU        = pucBufU;
  register const UINT8  *pucV        = pucBufV;
  register UINT16       *pusD;

  register UINT32        unDstWidthPad;

  register int           i;
  register int           j;

  UINT32                 unTemp;


  HLH_ENTER_FUNC (HLH_DEBUG_VIDEO);

  HLH_DEBUG (HLH_DEBUG_VIDEO,
      ("unSrcWidth = %d, unSrcHeight = %d, unSrcWidthPad = %d",
       unSrcWidth, unSrcHeight, unSrcWidthPad) );
  HLH_DEBUG (HLH_DEBUG_VIDEO,
      ("unDstX = %d, unDstY = %d, unDstWidth = %d, unDstHeight = %d",
       unDstX, unDstY, unDstWidth, unDstHeight) );

  // Assertions

  ASSERT (pucBufY   != NULL);
  ASSERT (pucBufU   != NULL);
  ASSERT (pucBufV   != NULL);
  ASSERT (pusBufRGB != NULL);

  ASSERT (unSrcWidth  != 0);
  ASSERT (unSrcHeight != 0);
  ASSERT (unDstWidth  != 0);
  ASSERT (unDstHeight != 0);

  ASSERT (unDstX < unDstWidth);
  ASSERT (unDstY < unDstHeight);

  ASSERT ( (unSrcWidth % 2) == 0 );
  ASSERT ( (unSrcWidthPad % 2) == 0 );


  // Adjust image width and height

  if (unDstX + unSrcWidth >= unDstWidth) {
    unTemp         = unSrcWidth;
    unSrcWidth     = (unDstWidth - unDstX) / 2 * 2;
    unSrcWidthPad += unTemp - unSrcWidth;
  }

  if (unDstY + unSrcHeight > unDstHeight) {
    unSrcHeight = unDstHeight - unDstY;
  }

  HLH_DEBUG ( HLH_DEBUG_VIDEO,
      ("unSrcWidth = %d, unSrcWidthPad = %d, unSrcHeight = %d",
       unSrcWidth, unSrcWidthPad, unSrcHeight) );

  // Modify the destination pointer

  pusD = pusBufRGB + unDstY * unDstHeight + unDstX;
  unDstWidthPad = unDstWidth - unSrcWidth;

  // Convert UYVY to RGB565 according to its width

  if ( (unSrcWidth & 0x0F) == 0 ) {

    i = unSrcHeight;

    HLH_DEBUG ( HLH_DEBUG_VIDEO, ("use 16 fast mode") );

    do {

      j = unSrcWidth / 16;

      do {

        register UINT16 *pusR;
        register UINT16 *pusG;
        register UINT16 *pusB;

        register int     nY;
        register int     nU;
        register int     nV;

        RGB (0);
        DST (pucY, pusD, 0);
        DST (pucY, pusD, 1);

        RGB (1);
        DST (pucY, pusD, 2);
        DST (pucY, pusD, 3);

        RGB (2);
        DST (pucY, pusD, 4);
        DST (pucY, pusD, 5);

        RGB (3);
        DST (pucY, pusD, 6);
        DST (pucY, pusD, 7);

        RGB (4);
        DST (pucY, pusD, 8);
        DST (pucY, pusD, 9);

        RGB (5);
        DST (pucY, pusD, 10);
        DST (pucY, pusD, 11);

        RGB (6);
        DST (pucY, pusD, 12);
        DST (pucY, pusD, 13);

        RGB (7);
        DST (pucY, pusD, 14);
        DST (pucY, pusD, 15);

        pucY += 16;
        pucU += 16 / 2;
        pucV += 16 / 2;

        pusD += 16;

      } while (--j);

      pucY += unSrcWidthPad;
      pucU += unSrcWidthPad / 2;
      pucV += unSrcWidthPad / 2;

      pusD += unDstWidthPad;

    } while (--i);

  }


  HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

}


/******************************************************************************
 * Func : HLH_FrameBuffer::ShowYUV422PImage
 * Desc : Show image of format YUV422P on this framebuffer
 * Args : pucBufY               Buffer of Y component
 *        pucBufU               Buffer of U component
 *        pucBufV               Buffer of V component
 *        unSrcX                Left offset of source image
 *        unSrcY                Top offset of source image
 *        unSrcWidth            Width of source image
 *        unSrcHeight           Height of source image
 *        unSrcWidthPad         Width padding of source image
 *        unDstX                Left offset of framebuffer
 *        unDstY                Top offset of framebuffer
 * Outs : NONE
 ******************************************************************************/
void HLH_FrameBuffer::ShowYUV422PImage (
    UINT8    *pucBufY,
    UINT8    *pucBufU,
    UINT8    *pucBufV,
    UINT32    unSrcX,
    UINT32    unSrcY,
    UINT32    unSrcWidth,
    UINT32    unSrcHeight,
    UINT32    unSrcWidthPad,
    UINT32    unDstX,
    UINT32    unDstY
    )
{

  UINT32      unSrcOffset;

  HLH_ENTER_FUNC (HLH_DEBUG_VIDEO);

  HLH_DEBUG ( HLH_DEBUG_VIDEO,
      ("unSrcX = %d, unSrcY = %d, unSrcWidth = %d, unSrcHeight = %d",
       unSrcX, unSrcY, unSrcWidth, unSrcHeight) );
  HLH_DEBUG ( HLH_DEBUG_VIDEO,
      ("unSrcWidthPad = %d, unDstX = %d, unDstY = %d",
       unSrcWidthPad, unDstX, unDstY) );


  // Assertions

  ASSERT (pucBufY != NULL);
  ASSERT (pucBufU != NULL);
  ASSERT (pucBufV != NULL);

  ASSERT (unSrcX < unSrcWidth);
  ASSERT (unSrcY < unSrcHeight);

  ASSERT (unDstX < m_unFBWidth);
  ASSERT (unDstY < m_unFBHeight);


  // Convert and display
  unSrcOffset = unSrcX + unSrcY * (unSrcWidth + unSrcWidthPad);

  HLH_DEBUG ( HLH_DEBUG_VIDEO,
      ("unSrcOffset = %d", unSrcOffset) );

  // Write image to next frame buffer
  ConvertYUV422PToRGB565 (pucBufY + unSrcOffset,
      pucBufU + unSrcOffset/2, pucBufV + unSrcOffset/2,
      unSrcWidth, unSrcHeight, unSrcWidthPad,
      m_pusFrameBuf, unDstX, unDstY,
      m_unFBWidth, m_unFBHeight);

  memcpy ( m_pusFrame, m_pusFrameBuf,
      m_unFBWidth * m_unFBHeight * sizeof(UINT16) );

  HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

}



/******************************************************************************
 * Func : HLH_FrameBuffer::ConvertYUV420ToRGB565
 * Desc : Convert image of format YUV420 to RGB565
 * Args : pucBufY               Buffer of Y component
 *        pucBufU               Buffer of U component
 *        pucBufV               Buffer of V component
 *        unSrcWidth            Width of source image
 *        unSrcHeight           Height of source image
 *        unSrcWidthPad         Width padding of source image
 *        pusBufRGB             Buffer of RGB565
 *        unDstX                Left offset of destination image
 *        unDstY                Top offset of destination image
 *        unDstWidth            Width of destination image
 *        unDstHeight           Height of destination image
 * Outs : NONE
 ******************************************************************************/
void HLH_FrameBuffer::ConvertYUV420ToRGB565 (
    UINT8    *pucBufY,
    UINT8    *pucBufU,
    UINT8    *pucBufV,
    UINT32    unSrcWidth,
    UINT32    unSrcHeight,
    UINT32    unSrcWidthPad,

    UINT16   *pusBufRGB,
    UINT32    unDstX,
    UINT32    unDstY,
    UINT32    unDstWidth,
    UINT32    unDstHeight
    )
{

  register void        **ppvTableV2R = m_apvTableV2R;
  register void        **ppvTableU2G = m_apvTableU2G;
  register int          *pnTableV2G  = m_anTableV2G;
  register void        **ppvTableU2B = m_apvTableU2B;

  register const UINT8  *pucY        = pucBufY;
  register const UINT8  *pucU        = pucBufU;
  register const UINT8  *pucV        = pucBufV;
  register UINT16       *pusD;

  register UINT32        unDstWidthPad;

  register int           i;
  register int           j;

  UINT32                 unTemp;


  HLH_ENTER_FUNC (HLH_DEBUG_VIDEO);

  HLH_DEBUG (HLH_DEBUG_VIDEO,
      ("unSrcWidth = %d, unSrcHeight = %d, unSrcWidthPad = %d",
       unSrcWidth, unSrcHeight, unSrcWidthPad) );
  HLH_DEBUG (HLH_DEBUG_VIDEO,
      ("unDstX = %d, unDstY = %d, unDstWidth = %d, unDstHeight = %d",
       unDstX, unDstY, unDstWidth, unDstHeight) );

  // Assertions

  ASSERT (pucBufY   != NULL);
  ASSERT (pucBufU   != NULL);
  ASSERT (pucBufV   != NULL);
  ASSERT (pusBufRGB != NULL);

  ASSERT (unSrcWidth  != 0);
  ASSERT (unSrcHeight != 0);
  ASSERT (unDstWidth  != 0);
  ASSERT (unDstHeight != 0);

  ASSERT (unDstX < unDstWidth);
  ASSERT (unDstY < unDstHeight);

  ASSERT ( (unSrcWidth % 2) == 0 );
  ASSERT ( (unSrcWidthPad % 2) == 0 );


  // Adjust image width and height

  if (unDstX + unSrcWidth >= unDstWidth) {
    unTemp         = unSrcWidth;
    unSrcWidth     = (unDstWidth - unDstX - 1) / 2 * 2;
    unSrcWidthPad += unTemp - unSrcWidth;
  }

  if (unDstY + unSrcHeight >= unDstHeight) {
    unSrcHeight = unDstHeight - unDstY - 1;
  }

  HLH_DEBUG ( HLH_DEBUG_VIDEO,
      ("unSrcWidth = %d, unSrcWidthPad = %d, unSrcHeight = %d",
       unSrcWidth, unSrcWidthPad, unSrcHeight) );

  // Modify the destination pointer

  pusD = pusBufRGB + unDstY * unDstHeight + unDstX;
  unDstWidthPad = unDstWidth - unSrcWidth;

  // Convert UYVY to RGB565 according to its width

  if ( (unSrcWidth & 0x0F) == 0 ) {

    i = unSrcHeight;

    HLH_DEBUG ( HLH_DEBUG_VIDEO, ("use 16 fast mode") );

    do {

      j = unSrcWidth / 16;

      do {

        register UINT16 *pusR;
        register UINT16 *pusG;
        register UINT16 *pusB;

        register int     nY;
        register int     nU;
        register int     nV;

        RGB (0);
        DST (pucY, pusD, 0);
        DST (pucY, pusD, 1);
        DST (pucY, pusD, 0);
        DST (pucY, pusD, 1);

        RGB (1);
        DST (pucY, pusD, 2);
        DST (pucY, pusD, 3);

        RGB (2);
        DST (pucY, pusD, 4);
        DST (pucY, pusD, 5);

        RGB (3);
        DST (pucY, pusD, 6);
        DST (pucY, pusD, 7);

        RGB (4);
        DST (pucY, pusD, 8);
        DST (pucY, pusD, 9);

        RGB (5);
        DST (pucY, pusD, 10);
        DST (pucY, pusD, 11);

        RGB (6);
        DST (pucY, pusD, 12);
        DST (pucY, pusD, 13);

        RGB (7);
        DST (pucY, pusD, 14);
        DST (pucY, pusD, 15);

        pucY += 16;
        pucU += 16 / 2;
        pucV += 16 / 2;

        pusD += 16;

      } while (--j);

      pucY += unSrcWidthPad;
      pucU += unSrcWidthPad / 2;
      pucV += unSrcWidthPad / 2;

      pusD += unDstWidthPad;

    } while (--i);

  }


  HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

}


/******************************************************************************
 * Func : HLH_FrameBuffer::ShowYUV420Image
 * Desc : Show image of format YUV420 on this framebuffer
 * Args : pucBufY               Buffer of Y component
 *        pucBufU               Buffer of U component
 *        pucBufV               Buffer of V component
 *        unSrcX                Left offset of source image
 *        unSrcY                Top offset of source image
 *        unSrcWidth            Width of source image
 *        unSrcHeight           Height of source image
 *        unSrcWidthPad         Width padding of source image
 *        unDstX                Left offset of framebuffer
 *        unDstY                Top offset of framebuffer
 * Outs : NONE
 ******************************************************************************/
void HLH_FrameBuffer::ShowYUV420Image (
    UINT8    *pucBufY,
    UINT8    *pucBufU,
    UINT8    *pucBufV,
    UINT32    unSrcX,
    UINT32    unSrcY,
    UINT32    unSrcWidth,
    UINT32    unSrcHeight,
    UINT32    unSrcWidthPad,
    UINT32    unDstX,
    UINT32    unDstY
    )
{

  UINT32      unSrcOffset;

  HLH_ENTER_FUNC (HLH_DEBUG_VIDEO);

  HLH_DEBUG ( HLH_DEBUG_VIDEO,
      ("unSrcX = %d, unSrcY = %d, unSrcWidth = %d, unSrcHeight = %d",
       unSrcX, unSrcY, unSrcWidth, unSrcHeight) );
  HLH_DEBUG ( HLH_DEBUG_VIDEO,
      ("unSrcWidthPad = %d, unDstX = %d, unDstY = %d",
       unSrcWidthPad, unDstX, unDstY) );


  // Assertions

  ASSERT (pucBufY != NULL);
  ASSERT (pucBufU != NULL);
  ASSERT (pucBufV != NULL);

  ASSERT (unSrcX < unSrcWidth);
  ASSERT (unSrcY < unSrcHeight);

  ASSERT (unDstX < m_unFBWidth);
  ASSERT (unDstY < m_unFBHeight);


  // Convert and display
  unSrcOffset = unSrcX + unSrcY * (unSrcWidth + unSrcWidthPad);

  HLH_DEBUG ( HLH_DEBUG_VIDEO,
      ("unSrcOffset = %d", unSrcOffset) );

  // Write image to next frame buffer
  ConvertYUV420ToRGB565 (pucBufY + unSrcOffset,
      pucBufU + unSrcOffset/4, pucBufV + unSrcOffset/4,
      unSrcWidth, unSrcHeight, unSrcWidthPad,
      m_pusFrameBuf, unDstX, unDstY,
      m_unFBWidth, m_unFBHeight);

  memcpy ( m_pusFrame, m_pusFrameBuf,
      m_unFBWidth * m_unFBHeight * sizeof(UINT16) );


  HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

}
