/*******************************************************************************
 *    File Name : HLH_Video.cpp
 * 
 *       Author : Henry He
 * Created Time : Sat 24 Oct 2009 10:55:23 AM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "HLH_Video.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/

//===========================  Tables  =========================================

// Table of built in image format
static const struct v4l2_pix_format s_vpfPixFormatTable [HLH_VIDEO_PIX_FORMAT_NUMBER] = {
  // [HLH_VIDEO_PIX_FORMAT_160X120]
  { 160, 120, 16, V4L2_PIX_FMT_YUV422P, 0, 0, 0, 0 },
  // [HLH_VIDEO_PIX_FORMAT_320X240]
  { 320, 240, 16, V4L2_PIX_FMT_YUV422P, 0, 0, 0, 0 },
  // [HLH_VIDEO_PIX_FORMAT_640X480]
  { 640, 480, 16, V4L2_PIX_FMT_YUV422P, 0, 0, 0, 0 },
  // [HLH_VIDEO_PIX_FORMAT_88X72]
  { 88, 72, 16, V4L2_PIX_FMT_YUV422P, 0, 0, 0, 0 },
  // [HLH_VIDEO_PIX_FORMAT_176X144]
  { 176, 144, 16, V4L2_PIX_FMT_YUV422P, 0, 0, 0, 0 },
  // [HLH_VIDEO_PIX_FORMAT_352X288]
  { 352, 288, 16, V4L2_PIX_FMT_YUV422P, 0, 0, 0, 0 },
};






/******************************************************************************
 * Desc : Member Functions
 ******************************************************************************/





/******************************************************************************
 * Func : HLH_Video::HLH_Video
 * Desc : Constructor of HLH_Video
 * Args : 
 * Outs : 
 ******************************************************************************/
HLH_Video::HLH_Video ()
{

  m_zhmMutex.Init ();
  m_bCreated = false;

}   // -----  end of method HLH_Video::HLH_Video  (constructor)  ----- 


/******************************************************************************
 * Func : HLH_Video::~HLH_Video
 * Desc :  Deconstructor of HLH_Video
 * Args : 
 * Outs : 
 ******************************************************************************/
HLH_Video::~HLH_Video ()
{

  Destroy ();

}   // -----  end of method HLH_Video::~HLH_Video  (deconstructor)  ----- 




/******************************************************************************
 * Func : HLH_Video::Create
 * Desc : Create instance of HLH_Video
 * Args : pcVideo           Path of video device
 *        unPixFmt          Pixel format used
 *        unWidth           Width of image
 *        unHeight          Height of image
 * Outs : If success return 0, otherwise return error code.
 ******************************************************************************/
int HLH_Video::Create (const char *pcVideo, UINT32 unPixFmt, UINT32 unWidth, UINT32 unHeight)
{
  int nRetVal;


  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();

    HLH_DEBUG ( HLH_DEBUG_MAIN, ("already created") );
		return HLH_VIDEO_ERR_ALREAD_CREATED;
	}

	// Open video device for read/write
	m_fdVideo = open (pcVideo, O_RDWR);
	if ( m_fdVideo < 0 ) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("can't open %s", pcVideo) );
		goto failed;
	}

	// Get camera info
	nRetVal = ioctl ( m_fdVideo, VIDIOC_QUERYCAP, &m_vcCameraInfo );
	if ( nRetVal < 0 ) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("ioctl VIDIOC_QUERYCAP failed") );
		goto failed;
	}

	// Set image format
	nRetVal = __SetImageFormat (unPixFmt, unWidth, unHeight);
	if ( nRetVal < 0 ) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("set image format failed") );
		goto failed;
	}

	// Prepare fd_set for poll
	FD_ZERO (&m_fsReadSet);
	FD_SET  (m_fdVideo, &m_fsReadSet);

	// Notify that this instance created
	m_bCreated = true;

	// Unlock this instance
	m_zhmMutex.Unlock ();

	return 0;

failed:
	// Close device if failed
	if ( m_fdVideo >= 0 ) {
		close (m_fdVideo);
		m_fdVideo = -1;
	}

	// Unlock this instance
	m_zhmMutex.Unlock ();


	return HLH_VIDEO_ERR_CANT_CREATE;
}


/******************************************************************************
 * Func : HLH_Video::Destroy
 * Desc : Destroy this instance
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void HLH_Video::Destroy ()
{

	// Lock this instance
	m_zhmMutex.Lock ();

	// Check if created
	if (!m_bCreated) {
		// Unlock this instance
		m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("not created") );
		return;
	}

	// Close video device
	close (m_fdVideo);
	m_fdVideo = -1;

	// Notify that this instance not created
	m_bCreated = false;

	// Unlock this instance
	m_zhmMutex.Unlock ();

}



/******************************************************************************
 * Func : HLH_Video::IsCreated
 * Desc : Whether this instance created
 * Args : NONE
 * Outs : If created return true, otherwise return false
 ******************************************************************************/
bool HLH_Video::IsCreated ()
{
	bool bCreated;

	m_zhmMutex.Lock ();
	bCreated = m_bCreated;
	m_zhmMutex.Unlock ();

	return bCreated;
}


/******************************************************************************
 * Func : HLH_Video::SetImageFormat
 * Desc : Locked set image format
 * Args : unPixFmt              Pixel format used
 *        unWidth               Width  of image
 *        unHeight              Height of image
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int HLH_Video::SetImageFormat (UINT32 unPixFmt, UINT32 unWidth, UINT32 unHeight)
{

	int nRetVal;

	// Lock this instance
	m_zhmMutex.Lock ();

	// Check if created
	if (!m_bCreated) {
		// Unlock this instance
		m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("not created") );
		return HLH_VIDEO_ERR_NOT_CREATED;
	}

	nRetVal = __SetImageFormat (unPixFmt, unWidth, unHeight);

	// Unlock this instance
	m_zhmMutex.Unlock ();

	return nRetVal;
}

/******************************************************************************
 * Func : HLH_Video::__SetImageFormat
 * Desc : Set image format
 * Args : unPixFmt              index of built-in format
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int HLH_Video::__SetImageFormat (UINT32 unPixFmt, UINT32 unWidth, UINT32 unHeight)
{
	int nRetVal;

	struct v4l2_format  vfFormat;


	// Set image format

	vfFormat.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	vfFormat.fmt.pix.width = unWidth;
	vfFormat.fmt.pix.height = unHeight;
	vfFormat.fmt.pix.depth = 16;
	vfFormat.fmt.pix.pixelformat = unPixFmt;  // V4L2_PIX_FMT_YUV422P

	nRetVal = ioctl ( m_fdVideo, VIDIOC_S_FMT, &vfFormat );
	if ( nRetVal < 0 ) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("ioctl VIDIOC_S_FMT failed") );
		goto failed;
	}

	m_unWidth  = unWidth;
	m_unHeight = unHeight;


	return 0;

failed:

	return HLH_VIDEO_ERR_FAILED;

}



/******************************************************************************
 * Func : HLH_Video::GetImageFormat
 * Desc : Get image format current used
 * Args : NONE
 * Outs : Return 0 if success, format info will pass wia unWidth and unHeight,
 *        otherwise return error code
 ******************************************************************************/
int HLH_Video::GetImageFormat (UINT32 &unWidth, UINT32 &unHeight)
{


	// Lock this instance
	m_zhmMutex.Lock ();

	if (!m_bCreated) {
		// Unlock this instance
		m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("not created") );
		return HLH_VIDEO_ERR_FAILED;
	}

	unWidth  = m_unWidth;
	unHeight = m_unHeight;

	// Unlock this instance
	m_zhmMutex.Unlock ();


	return 0;

}

/******************************************************************************
 * Func : HLH_Video::Start
 * Desc : Send start command to video device
 * Args : NONE
 * Outs : If success return 0, otherwise return -1.
 ******************************************************************************/
int HLH_Video::Start ()
{
	int  nRetVal;
	char cStart = 'O';

	// Lock this instance
	m_zhmMutex.Lock ();

	if (!m_bCreated) {
		// Unlock this instance
		m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("not created") );
		return HLH_VIDEO_ERR_NOT_CREATED;
	}

	nRetVal = write ( m_fdVideo, &cStart, sizeof(cStart) );
	if (nRetVal < 0) {
		goto failed;
	}

	// Unlock this instance
	m_zhmMutex.Unlock ();

	return 0;

failed:
	// Unlock this instance
	m_zhmMutex.Unlock ();

	return HLH_VIDEO_ERR_FAILED;
}

/******************************************************************************
 * Func : 
 * Desc : Read image data from video device
 * Args : 
 * Outs : 
 ******************************************************************************/
int HLH_Video::Read (void *pvBuf, UINT32 unLen)
{
	int nRetVal;


	// Lock this instance
	m_zhmMutex.Lock ();

	if (!m_bCreated) {
		// Unlock this instance
		m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("not created") );
		return HLH_VIDEO_ERR_NOT_CREATED;
	}

	nRetVal = read (m_fdVideo, pvBuf, unLen);
	if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("read %u from camera failed", unLen) );
		goto failed;
	}

	// Unlock this instance
	m_zhmMutex.Unlock ();

	return 0;

failed:
	// Unlock this instance
	m_zhmMutex.Unlock ();

	return HLH_VIDEO_ERR_FAILED;
}

/******************************************************************************
 * Func : 
 * Desc : Poll for video device data
 * Args : 
 * Outs : 
 ******************************************************************************/
int HLH_Video::TimedPollRead (HLH_Time &zhtTime)
{
	int            nRetVal;
	struct timeval tvTime;
	fd_set         fsReadSet;


	// Lock this instance
	m_zhmMutex.Lock ();

	if (!m_bCreated) {
		// Unlock this instance
		m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("not created") );
		return HLH_VIDEO_ERR_NOT_CREATED;
	}

	memcpy ( &fsReadSet, &m_fsReadSet, sizeof(fd_set) );
	tvTime = zhtTime.GetTimeVal ();

	// Poll and wait until timeout
	nRetVal = select (m_fdVideo + 1, &fsReadSet, NULL, NULL, &tvTime);
	if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("select read failed") );
		goto failed;
	}

	// Set time remained
	zhtTime.SetTime (tvTime);

	// Unlock this instance
	m_zhmMutex.Unlock ();

	return nRetVal;

failed:
	// Unlock this instance
	m_zhmMutex.Unlock ();

	return HLH_VIDEO_ERR_FAILED;
}


