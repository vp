/*******************************************************************************
 *         File : HLH_Video.h
 * 
 *       Author : Henry He
 *      Created : Sat 24 Oct 2009 10:56:30 AM CST
 *  Description : 
 ******************************************************************************/

#ifndef __HLH_VIDEO_INC_20091024_105630_HENRY__
#define __HLH_VIDEO_INC_20091024_105630_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "utils/common.h"
#include  "video/videodev.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

//===========================  Device Node  ====================================

#define HLH_VIDEO_V4L2_DEV_PREVIEW "/dev/video/preview"
#define HLH_VIDEO_V4L2_DEV_CODEC   "/dev/video/codec"



//===========================  Error Code  =====================================

#define HLH_VIDEO_ERR_FAILED            (-1)
#define HLH_VIDEO_ERR_CANT_CREATE       (-2)
#define HLH_VIDEO_ERR_NOT_CREATED       (-3)
#define HLH_VIDEO_ERR_ALREAD_CREATED    (-4)


//===========================  Default Values  =================================

#define	HLH_VIDEO_DEFAULT_PIX_FORMAT        HLH_VIDEO_PIX_FORMAT_320X240


//===========================  Formats  ========================================

#define	HLH_VIDEO_PIX_FORMAT_160X120        0
#define	HLH_VIDEO_PIX_FORMAT_QQVGA          0
#define	HLH_VIDEO_PIX_FORMAT_320X240        1
#define	HLH_VIDEO_PIX_FORMAT_QVGA           1
#define	HLH_VIDEO_PIX_FORMAT_640X480        2
#define	HLH_VIDEO_PIX_FORMAT_VGA            2

#define	HLH_VIDEO_PIX_FORMAT_88X72          3
#define	HLH_VIDEO_PIX_FORMAT_QQCIF          3
#define	HLH_VIDEO_PIX_FORMAT_176X144        4
#define	HLH_VIDEO_PIX_FORMAT_QCIF           4
#define	HLH_VIDEO_PIX_FORMAT_352X288        5
#define	HLH_VIDEO_PIX_FORMAT_CIF            5

//===========================  Number of Formats  ==============================

#define HLH_VIDEO_PIX_FORMAT_NUMBER         6



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/






/******************************************************************************
 * Class : HLH_Video
 * Desc  : Wrapper of /dev/video/codec
 ******************************************************************************/

class HLH_Video
{

  public:

    // ====================  LIFECYCLE    ========================================= 

    // Constructor of HLH_Video
    HLH_Video ();

    // Decontructor of HLH_Video
    ~ HLH_Video ();


  public:

    // ====================  OPERATORS    ========================================= 

    // ====================  OPERATIONS   ========================================= 

    // Create instance of HLH_Video
    int Create (const char *pcDevice, UINT32 unPixFmt, UINT32 unWidth, UINT32 unHeight);

    // Destroy instance of HLH_Video
    void Destroy ();

    // Whether this instance created
    bool IsCreated ();

    // ====================  ACCESS       ========================================= 

    // Set image format by width and height
    int SetImageFormat (UINT32 unPixFmt, UINT32 unWidth, UINT32 unHeight);

    // Get image format current used
    int GetImageFormat (UINT32 &unWidth, UINT32 &unHeight);
		// Send start command to video device
		int Start ();

    // Read image data from video device
    int Read (void *pvBuf, UINT32 unLen);

    // Poll for video device data
    int TimedPollRead (HLH_Time &zhtTime);


  private:

    // ====================  OPERATIONS   ========================================= 

		int __SetImageFormat (UINT32 unPixFmt, UINT32 unWidth, UINT32 unHeight);

  private:

    // ====================  MEMBER DATA  ========================================= 

    // Mutex for this instance
    HLH_Mutex              m_zhmMutex;

    // whether this instance created
    bool                   m_bCreated;

    // File descriptor of Video device
    int                    m_fdVideo;

    // Video width and height
    UINT32                 m_unWidth;
    UINT32                 m_unHeight;

    // Saved camera Info (save when create)
    struct v4l2_capability m_vcCameraInfo;

    // File descriptor set for poll
    fd_set                 m_fsReadSet;

};  // -----  end of class  HLH_Video  ----- 







#endif /* __HLH_VIDEO_INC_20091024_105630_HENRY__ */
