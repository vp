/*******************************************************************************
 *         File : LudpReceiver.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-13 9:02:32
 *  Description : 
 ******************************************************************************/

#ifndef __LUDPRECEIVER_INC_20091013_090232_HENRY__
#define __LUDPRECEIVER_INC_20091013_090232_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "utils/common.h"
#include "ludp/common.h"



/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


////////////////////////////////////////////////////////////////////////////////
// Error Number

#define LUDP_RECEIVER_ERR_FAILED                         (-1)
#define LUDP_RECEIVER_ERR_NOT_CREATED                    (-2)
#define LUDP_RECEIVER_ERR_ALREADY_CREATED                (-3)
#define LUDP_RECEIVER_ERR_CANT_CREATE                    (-4)

////////////////////////////////////////////////////////////////////////////////
// Max Values

#define LUDP_RECEIVER_MAX_RESEND_SLICE_SN_PER_SLICE       200

/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Struct of Receiver Package

typedef struct tagReceiverPackage
{
  // Whether this package finished
  bool          bFinished;

  // Timestamp of this package
  UINT32        unTimestamp;

  // Slice pointer number
  UINT16        usSliceNum;

  // Number of slices not received
  UINT16        usSliceLeft;

  // Array of slice pointers
  Slice       **ppzsSlices;

  // If Timeout reached (unTimeout == 0), unRetry--,
  //      don't retry to ask sender to resend if unRetry == 0;
  UINT32        unRetry;
  // If unTimeout == 0 and unRetry != 0,
  //      ask sender to resend part of this package
  UINT32        unTimeout;

} ReceiverPackage;


////////////////////////////////////////////////////////////////////////////////
// Callback function
typedef void (*OnLudpRecvFunc) (void *pvParam, ReceiverPackage &zrpPkg);


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/





/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/





/******************************************************************************
 * Desc : Ludp Receiver
 ******************************************************************************/
class LudpReceiver
{

  public:
    /******************************************************************************
     * Desc : Constructor / Deconstructor
     ******************************************************************************/
    LudpReceiver ();
    virtual ~LudpReceiver ();


  public:
    /******************************************************************************
     * Desc : Operations
     ******************************************************************************/
    int Create (
        HLH_UDPSock      &zhuSock,
        UINT8             ucBaseType,
        OnLudpRecvFunc    OnLudpRecv,
        void             *pvOnLudpRecvParam,
        UINT32            unPkgNum,
        UINT32            unTimeTickUs,
        UINT32            unTimeout,
        UINT32            unMaxRetry );


    // Destroy this LudpReceiver
    void Destroy ();

    // Whether this instance created
    bool IsCreated ();


  private:
    /******************************************************************************
     * Desc : Operations
     ******************************************************************************/

    ////////////////////////////////////////////////////////////////////////////////
    // Whether zhrTimestamp in current receive range
    bool IsInRecvRange (HLH_RoundU32 zhrTimestamp);


    // Update timestamp range and package list if need according to zhrTimestamp
    bool UpdateRecvRange (HLH_RoundU32 zhrTimestamp);

    // Clear package list in range (unPosLow ~ unPosHigh)
    void ClearPkgListRange (UINT32 unPosLow, UINT32 unPosHigh);

    ////////////////////////////////////////////////////////////////////////////////
    // Process Slice received
    int ProcessSlice (Slice * pzsSlice);

    ////////////////////////////////////////////////////////////////////////////////
    // Receive data from socket
    static void OnReceived (
        void *pvThis,
        void *pvBuf, UINT32 unLen,
        HLH_SockAddr &zhsPeerAddr);


    ////////////////////////////////////////////////////////////////////////////////
    // Send Request to sender to resend some slice of package (unTimestamp)
    int SendRequest (UINT32 unTimestamp);

    // Check for timeout and Send request to sender to resend some slice
    void CheckTimeout (HLH_Thread &zhtTimeoutThread);

    ////////////////////////////////////////////////////////////////////////////////
    // TimeoutThreadFunc:
    //    Check for timeout and Send request to sender to resend some slice
    static void * TimeoutThreadFunc (HLH_Thread &zhtTimeoutThread, void *pvThis);



  private:
    /******************************************************************************
     * Desc : Private Data
     ******************************************************************************/
    ////////////////////////////////////////////////////////////////////////////////
    // Mutex of this instance
    HLH_Mutex           m_zhmMutex;

    // Whether this instance created
    bool                m_bCreated;

    // Whether first package received
    bool                m_bReceived;

    // Socket of ludp (shared with others)
    HLH_UDPSock       * m_pzhuSock;

    // Base type of slices to process by this receiver
    UINT8               m_ucBaseType;


    ////////////////////////////////////////////////////////////////////////////////
    // Timestamp Range, initialed by the first slice received
    HLH_RoundU32        m_zhrTimestampHigh;

    // Time tick value
    HLH_Time            m_zhtTimeTick;

    // Default timeout value
    UINT32              m_unMaxTimeout;

    // Max retry times
    UINT32              m_unMaxRetry;


    ////////////////////////////////////////////////////////////////////////////////
    // Ring Buffer of Package List Pointer
    ReceiverPackage   * m_pzrPkgList;

    // Number of Ring Buffer Pointer
    UINT32              m_unPkgNum;

    // Current Positions of Ring Buffer
    UINT32              m_unHighPos;


    ////////////////////////////////////////////////////////////////////////////////
    // Timeout Thread
    HLH_Thread          m_zhtTimeoutThread;

    // Callback function called when finish receviing a package
    OnLudpRecvFunc      m_zolOnLudpRecv;

    // Parameter to m_zolOnLudpRecv ()
    void               *m_pvOnLudpRecvParam;

};



#endif /* __LUDPRECEIVER_INC_20091013_090232_HENRY__ */
