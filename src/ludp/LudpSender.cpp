/*******************************************************************************
 *    File Name : LudpSender.cpp
 * 
 *       Author : Henry He
 * Created Time : 2009-10-13 8:58:38
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "LudpSender.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/






/******************************************************************************
 * Func : RoundAdd
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
static inline UINT32 RoundAdd (UINT32 unRound, UINT32 unVal1, UINT32 unVal2)
{
  UINT32 unResult;

  unResult = (unVal1 + unVal2) % unRound;

  return unResult;
}


/******************************************************************************
 * Func : RoundSub
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
static inline UINT32 RoundSub (UINT32 unRound, UINT32 unVal1, UINT32 unVal2)
{
  UINT32  unResult;

  if (unVal1 >= unVal2) {
    unResult = unVal1 - unVal2;
  } else {
    unResult = unRound + unVal1 - unVal2;
  }

  return unResult;
}





/******************************************************************************
 * Desc : Constructor / Deconstructor of LudpSender
 ******************************************************************************/







/******************************************************************************
 * Func : LudpSender
 * Desc : Constructor of LudpSender
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
LudpSender::LudpSender ()
{

  // Init mutex and condition
  m_zhmMutex.Init ();
  m_zhcSendRequestCond.Init ();

  // Notify that this instance not created
  m_bCreated = false;

}



/******************************************************************************
 * Func : LudpSender::~LudpSender
 * Desc : Deconstructor of LudpSender
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
LudpSender::~LudpSender ()
{
  Destroy ();
}





/******************************************************************************
 * Desc : Operations
 ******************************************************************************/





/******************************************************************************
 * Func : LudpSender::Create
 * Desc : Create instance of LudpSender
 * Args : zhuSock             Socket used by this LudpSender (shared)
 *        ucBaseType          Base type of data process by this sender
 *                            used by OnReceived to filter packages
 *        unTimestamp         Initial timestamp
 *        unPkgNum            Number of package to be store for resend
 *        usMaxSliceDataLen   Max length of Slice Data
 * Outs : If successe, return 0, otherwise return error code
 ******************************************************************************/
int LudpSender::Create (
        HLH_UDPSock  &zhuSock,
        UINT8         ucBaseType,
        UINT32        unTimestamp,
        UINT32        unPkgNum,
        UINT16        usMaxSliceDataLen
        )
{

  int nRetVal;

  // Check arguments
  ASSERT (unPkgNum > 0);
  ASSERT (usMaxSliceDataLen > 0);

  // Lock this instance
  m_zhmMutex.Lock ();

  // Check if already created
  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_LUDP, ("already created") );
    return LUDP_SENDER_ERR_ALREADY_CREATED;
  }

  // Set socket pointer (share with other)
  m_pzhuSock    = &zhuSock;
  zhuSock.AddRecvHandler (OnReceived, this);

  m_ucBaseType  = ucBaseType;

  // Initial Max Slice Data Len
  m_usMaxSliceDataLen = usMaxSliceDataLen;

  // Initial timestamp of package to be send
  m_zhrTimestampCur = unTimestamp;

  // Initial Send Queue
  m_slSendQueue.clear ();

  // Initial Package List
  m_unPkgNum = unPkgNum;
  m_unCurPos = 0;

  m_pzsPkgList = (SenderPackage *) malloc ( sizeof (SenderPackage) * unPkgNum );
  if (m_pzsPkgList == NULL) {
		HLH_DEBUG ( HLH_DEBUG_LUDP, ("memory allocate send pkg list failed") );
    goto failed;
  }
  memset ( m_pzsPkgList, 0, sizeof(SenderPackage) * unPkgNum );

  // Start Send thread
  nRetVal = m_zhtSendThread.Start (SendThreadFunc, this);
  if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_LUDP, ("start send thread failed") );
    goto failed;
  }

  // Notify that this instance created
  m_bCreated = true;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:

  if (m_pzsPkgList != NULL) {
    free (m_pzsPkgList);
    m_pzsPkgList = NULL;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return LUDP_SENDER_ERR_CANT_CREATE;

}


/******************************************************************************
 * Func : LudpSender::IsCreated
 * Desc : Whether this instance created
 * Args : NONE
 * Outs : If created return true, otherwise return false.
 ******************************************************************************/
bool LudpSender::IsCreated ()
{
  bool bCreated;

  m_zhmMutex.Lock ();
  bCreated = m_bCreated;
  m_zhmMutex.Unlock ();

  return bCreated;
}

/******************************************************************************
 * Func : LudpSender::Stop
 * Desc : Stop Ludp Sender Connection
 * Args : NONE
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int LudpSender::Destroy ()
{
  UINT32          unPkgIt;
  UINT16          usSliceIt;

  SenderPackage  *pzspPkg;
  Slice         **ppzsSlices;
  Slice          *pzsSlice;

  ////////////////////////////////////////////////////////////////////////////////
  // Stop send thread if started
  m_zhtSendThread.Stop ();


  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return LUDP_SENDER_ERR_NOT_CREATED;
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Release package list
  if (m_pzsPkgList != NULL) {

    // Release packages
    for (unPkgIt = 0; unPkgIt < m_unPkgNum; unPkgIt ++) {
      pzspPkg = & m_pzsPkgList [unPkgIt];
      ppzsSlices = pzspPkg->ppzsSlices;
      if (ppzsSlices != NULL) {
        // Release slices
        for (usSliceIt = 0; usSliceIt < pzspPkg->usSliceNum; usSliceIt ++) {
          free (ppzsSlices [usSliceIt]);
        }
        // Release the slice pointer list
        free (ppzsSlices);
      }
    }

    free (m_pzsPkgList);
    m_pzsPkgList = NULL;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Release send queue

  while ( !m_slSendQueue.empty() ) {
    pzsSlice = m_slSendQueue.front ();
    free (pzsSlice);
    m_slSendQueue.pop_front ();
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Notify that this instance not created
  m_bCreated = false;


  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;
}


/******************************************************************************
 * Func : SendPackage
 * Desc : Divide package into slice and put them into transmit queue
 * Args : pucBuffer         Buffer of package to be send
 *        unLen             Length of package
 * Outs : If operation successed, return 0, otherwise return error code
 ******************************************************************************/
int LudpSender::SendPackage (UINT8 *pucBuffer, UINT32 unLen)
{
  UINT32      unSliceNum;
  UINT32      unSliceIt;

  UINT32      unSliceLen;

  Slice     **ppzsSlices;
  Slice      *pzsSlice;

  UINT8      *pucData;

  bool        bSend = false;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_LUDP, ("not created") );
    return LUDP_SENDER_ERR_NOT_CREATED;
  }

  ////////////////////////////////////////////////////////////////////////////////

  // If current position isn't empty, free resource used by that package
  unSliceNum = m_pzsPkgList [m_unCurPos].usSliceNum;
  ppzsSlices = m_pzsPkgList [m_unCurPos].ppzsSlices;
  if ( unSliceNum != 0 && ppzsSlices != NULL ) {

    for (unSliceIt = 0; unSliceIt < unSliceNum; unSliceIt ++) {
      free ( ppzsSlices [unSliceIt] );
    }

    free (ppzsSlices);

    m_pzsPkgList [m_unCurPos].usSliceNum = 0;
    m_pzsPkgList [m_unCurPos].ppzsSlices = NULL;

  }

  ////////////////////////////////////////////////////////////////////////////////

  // Calculate the number of slices
  unSliceNum = (unLen + m_usMaxSliceDataLen - 1) / m_usMaxSliceDataLen;


  // Allocate space to store the slice pointers
  ppzsSlices = (Slice **) malloc ( sizeof (Slice *) * unSliceNum );
  if (ppzsSlices == NULL) {
    HLH_DEBUG ( HLH_DEBUG_LUDP, ("allocate slices failed") );
    goto failed;
  }

  memset ( ppzsSlices, 0, sizeof (Slice *) * unSliceNum );

  m_pzsPkgList [m_unCurPos].zhrTimestamp = m_zhrTimestampCur;
  m_pzsPkgList [m_unCurPos].usSliceNum   = unSliceNum;
  m_pzsPkgList [m_unCurPos].ppzsSlices   = ppzsSlices;

  // Process all the slices
  pucData = pucBuffer;

  for (unSliceIt = 0; unSliceIt < unSliceNum; unSliceIt++) {

    // Caculate length of slice data
    unSliceLen = unLen < m_usMaxSliceDataLen ? unLen : m_usMaxSliceDataLen;

    ////////////////////////////////////////////////////////////////////////////////
    // Put slice into resend list

    // Allocate one Slice
    pzsSlice = (Slice *) malloc (sizeof (Slice) - 4 + unSliceLen);
    if (pzsSlice == NULL) {
      goto failed;
    }

    // Set info of slice
    pzsSlice->usType       = LUDP_SLICE_TYPE (m_ucBaseType, LUDP_SLICE_SUB_TYPE_DATA);
    pzsSlice->usDataLen    = unSliceLen;
    pzsSlice->usSliceNum   = unSliceNum;
    pzsSlice->usSliceSN    = unSliceIt;
    pzsSlice->unTimestamp  = m_zhrTimestampCur;

    // Copy data to buffer
    memcpy (pzsSlice->aucData, pucData, unSliceLen);

    // Put it into list
    ppzsSlices [unSliceIt] = pzsSlice;


    ////////////////////////////////////////////////////////////////////////////////
    // Put slice into send queue

    // Allocate one Slice
    pzsSlice = CloneSlice (pzsSlice);
    if (pzsSlice == NULL) {
      HLH_DEBUG ( HLH_DEBUG_LUDP, ("clone slice failed") );
      goto failed;
    }

    // Put slice into queue
    m_slSendQueue.push_back (pzsSlice);

    bSend = true;

    ////////////////////////////////////////////////////////////////////////////////
    // Update pointers / counters

    pucData += unSliceLen;
    unLen   -= unSliceLen;

  }

  // Increase timestamp (auto round up)
  m_zhrTimestampCur ++;

  // Move ring buffer pointer
  m_unCurPos = RoundAdd (m_unPkgNum, m_unCurPos, 1);


  // Notify send thread if need
  if (bSend) {
    //HLH_DEBUG ( HLH_DEBUG_LUDP, ("signal send thread to send") );
    m_zhcSendRequestCond.Signal ();
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  // Successed
  return 0;

failed:

  ////////////////////////////////////////////////////////////////////////////////
  // If we can't allocate memory successfully, just abort this package sending
  ////////////////////////////////////////////////////////////////////////////////

  unSliceNum = m_pzsPkgList [m_unCurPos].usSliceNum;
  ppzsSlices = m_pzsPkgList [m_unCurPos].ppzsSlices;

  if (ppzsSlices != NULL) {
    // Remove slice of current timestamp from list
    for (unSliceIt = 0; unSliceIt < unSliceNum; unSliceIt++) {
      free ( ppzsSlices [unSliceIt] );
    }

    free (ppzsSlices);
  }

  m_pzsPkgList [m_unCurPos].usSliceNum = 0;
  m_pzsPkgList [m_unCurPos].ppzsSlices = NULL;

  // Remove slice of current timestamp from queue
  while (  ( pzsSlice = m_slSendQueue.back () ) != NULL
      && pzsSlice->unTimestamp == (UINT32) m_zhrTimestampCur  ) {
    free (pzsSlice);
    m_slSendQueue.pop_back ();
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return LUDP_SENDER_ERR_FAILED;
}

/******************************************************************************
 * Func : LudpSender::__SendThreadFunc
 * Desc : Send Thread Function:
 *            Send the slices in pszSendQueue
 * Args : zhtThread         send thread
 * Outs : If success started, return (void*)0, otherwise return (void*)-1
 ******************************************************************************/
void LudpSender::__SendThreadFunc (HLH_Thread &zhtThread)
{

  int           nRetVal;

  Slice        *pzsSlice;
  UINT32        unPollType;
  HLH_Time      zhtTime;

  // Notify zhtThread that SendThreadFunc () started
  zhtThread.ThreadStarted ();

  if ( !IsCreated() ) {
    HLH_DEBUG ( HLH_DEBUG_LUDP, ("not created") );
    return;
  }

  do {

    ////////////////////////////////////////////////////////////////////////////////
    // Lock this instance
    m_zhmMutex.Lock ();

    // Check if send request

    // If no send request, wait for a while
    if ( m_slSendQueue.empty () ) {
      m_zhcSendRequestCond.TimeWait ( m_zhmMutex,
          HLH_Time (LUDP_SENDER_TIMEOUT_SEND_COND_WAIT_SECONDS, 0) );

      // If there is still no send, 
      if ( m_slSendQueue.empty () ) {
        HLH_DEBUG ( HLH_DEBUG_LUDP, ("timeout and send queue still empty") );
        // Unlock this instance
        m_zhmMutex.Unlock ();
        // Check for stop condition
        goto check_stop;
      }
    }

    // Unlock this instance
    m_zhmMutex.Unlock ();


    ////////////////////////////////////////////////////////////////////////////////
    // There are some slices to send

    // Whether socket is ready for send until timeout
    unPollType = HLH_SOCK_POLL_WRITE;
    zhtTime = HLH_Time (LUDP_SENDER_TIMEOUT_SOCK_WRITE_WAIT_SECONDS, 0);
    nRetVal = m_pzhuSock->PollWait ( unPollType, zhtTime);
    if ( nRetVal < 0 ) {
      HLH_DEBUG ( HLH_DEBUG_LUDP, ("poll failed") );
      // Socket hasn't ready for send yet
      goto check_stop;
    }


    ////////////////////////////////////////////////////////////////////////////////
    // Send the slice now

    // Lock this instance
    m_zhmMutex.Lock ();

    // Maybe send queue changed
    if ( m_slSendQueue.empty () ) {
      // Unlock this instance
      m_zhmMutex.Unlock ();

      goto check_stop;
    }

    // Send the slice
    pzsSlice = m_slSendQueue.front ();
    ASSERT (pzsSlice != NULL);

#if 1
    m_pzhuSock->Send ( pzsSlice, HLH_SLICE_LENGTH (pzsSlice) );
#else /* simulate for slice lost */
    {
      int nRand;
      int nLimit = 1023 * 3/4;

      nRand = rand () % 1023;
      if (nRand < nLimit) {
        m_pzhuSock->Send ( pzsSlice, HLH_SLICE_LENGTH (pzsSlice) );
      }
    }
#endif

    // Release the slices sent
    free (pzsSlice);
    m_slSendQueue.pop_front ();

    // Unlock this instance
    m_zhmMutex.Unlock ();

check_stop:
    // Check for stop condition
    if ( zhtThread.IsStopping () ) {
      HLH_DEBUG ( HLH_DEBUG_LUDP, ("stop on request") );
      // Exit the thread
      return;
    }

  } while (1);

}


/******************************************************************************
 * Func : LudpSender::SendThreadFunc
 * Desc : Send Thread Function:
 *            Send the slices in pszSendQueue
 * Args : zhtThread         send thread
 *        pvThis            pointer to this instance
 * Outs : Always return 0
 ******************************************************************************/
void * LudpSender::SendThreadFunc (HLH_Thread &zhtThread, void *pvThis)
{
  ( (LudpSender*) pvThis )->__SendThreadFunc (zhtThread);

  return (void*) 0;
}


/******************************************************************************
 * Func : ResendSlices
 * Desc : Resend Package: Respond the slice which request to resend slices
 * Args : pzsSlice            the slice send by receiver to request some old slices
 *            Data Format:
 *              .0........1.||.2.......3.|.4.......5.| ...
 *               usSliceNum || usSliceSN | usSliceSN | ...
 * Outs : NONE
 ******************************************************************************/
void LudpSender::ResendSlices (Slice *pzsSlice)
{

  HLH_RoundU32    zhrTimestamp;
  HLH_RoundU32    zhrTimestampLow;

  UINT16          usSliceNum;
  UINT16          usSliceIt;
  UINT16          usSliceSN;

  UINT8          *pucData;
  UINT16         *pusSliceSN;

  UINT32          unPos;

  SenderPackage  *pzsPkg;

  Slice         **ppzsSlices;
  Slice          *pzsSliceSrc;
  Slice          *pzsSliceDst;

  bool            bSend = false;

  // Get Some Info from package
  pucData = pzsSlice->aucData;

  // Prepare resend info
  zhrTimestamp = pzsSlice->unTimestamp;
  usSliceNum   = * (UINT16*) &pucData[0];
  pusSliceSN   = (UINT16*) &pucData[2];



  // Lock this instance
  m_zhmMutex.Lock ();

  // Check if \c zhrTimestamp is valid
  zhrTimestampLow = (UINT32) m_zhrTimestampCur - m_unPkgNum;
  if ( zhrTimestamp < zhrTimestampLow || zhrTimestamp >= m_zhrTimestampCur) {
    HLH_DEBUG ( HLH_DEBUG_LUDP,
        ( "resend %u out of range %u ~ %u",
          (UINT32)zhrTimestamp, (UINT32)zhrTimestampLow,
          (UINT32)m_zhrTimestampCur-1 ) );
    goto failed;
  }

  // Get Package info from resend list
  unPos = RoundSub ( m_unPkgNum, m_unCurPos, m_zhrTimestampCur - zhrTimestamp );
  pzsPkg = & m_pzsPkgList [unPos];

  // Package may be absend as a result of memory allocate error
  if ( IS_NULL_SENDER_PACKAGE (pzsPkg) ) {
    HLH_DEBUG ( HLH_DEBUG_LUDP, ("resend of null package") );
    goto failed;
  }

  // Check for valid
  ASSERT (zhrTimestamp == pzsPkg->zhrTimestamp);



  // Resend the request slices


  ppzsSlices = pzsPkg->ppzsSlices;

  for (usSliceIt = 0; usSliceIt < usSliceNum; usSliceIt++) {

    // Get SliceSN from slice
    usSliceSN = *pusSliceSN++;

    HLH_DEBUG ( HLH_DEBUG_LUDP,
        ("resend slice %u of pkg %u", usSliceSN, (UINT32)zhrTimestamp) );
    // Check for valid
    ASSERT ( usSliceSN < pzsPkg->usSliceNum );

    // Make a copy of that slice
    pzsSliceSrc = ppzsSlices [usSliceSN];
    pzsSliceDst = CloneSlice (pzsSliceSrc);
    if (pzsSliceDst == NULL) {
      HLH_DEBUG ( HLH_DEBUG_LUDP, ("clone slice failed") );
      goto failed;
    }

    // Queue that slice
    m_slSendQueue.push_back (pzsSliceDst);
    bSend = true;

  }

  // Notify send thread if need
  if (bSend) {
    //HLH_DEBUG ( HLH_DEBUG_LUDP, ("signal send thread to send") );
    m_zhcSendRequestCond.Signal ();
  }

  // Finished
  // Falling through

failed:
  m_zhmMutex.Unlock ();
  return;


}





/******************************************************************************
 * Func : LudpSender::OnReceived
 * Desc : Callback function: receive data from socket
 *        Receive the slices sent by receiver and respond to them
 * Args : pvThis         pointer to this instance
 *        pvBuf           buffer for the data received
 *        unLen           length of data received
 *        zhsPeerAddr     peer address of data received
 * Outs : 
 ******************************************************************************/
void LudpSender::OnReceived (
    void *pvThis,
    void *pvBuf, UINT32 unLen,
    HLH_SockAddr &zhsPeerAddr)
{
  LudpSender  *pzlsSender;
  Slice       *pzsSlice;

  ASSERT ( pvThis != NULL );
  ASSERT ( pvBuf  != NULL );
  ASSERT ( unLen  != 0    );

  (void)zhsPeerAddr;

  // Pointer to this instance
  pzlsSender = (LudpSender *) pvThis;

  // Make it convenience to access slice member
  pzsSlice   = (Slice *) pvBuf;


  // If resend requested
  if ( LUDP_SLICE_BASE_TYPE(pzsSlice->usType) == pzlsSender->m_ucBaseType
      && LUDP_SLICE_SUB_TYPE (pzsSlice->usType) == LUDP_SLICE_SUB_TYPE_RESEND) {
    pzlsSender->ResendSlices (pzsSlice);
  } else {
    HLH_DEBUG ( HLH_DEBUG_LUDP, ("unknown slice type") );
  }
}


