/*******************************************************************************
 *         File : LudpSender.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-13 8:58:26
 *  Description : 
 ******************************************************************************/

#ifndef __LUDPSENDER_INC_20091013_085826_HENRY__
#define __LUDPSENDER_INC_20091013_085826_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "utils/common.h"

#include "ludp/common.h"



/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


////////////////////////////////////////////////////////////////////////////////
// Default Values

#define LUDP_SENDER_DEFAULT_MAX_SLICE_DATA_LEN      1000


////////////////////////////////////////////////////////////////////////////////
// Error Number

#define LUDP_SENDER_ERR_FAILED                      (-1)
#define LUDP_SENDER_ERR_NOT_CREATED                 (-2)
#define LUDP_SENDER_ERR_ALREADY_CREATED             (-3)
#define LUDP_SENDER_ERR_CANT_CREATE                 (-4)


////////////////////////////////////////////////////////////////////////////////
// Timeout

#define LUDP_SENDER_TIMEOUT_SEND_COND_WAIT_SECONDS  2
#define LUDP_SENDER_TIMEOUT_SOCK_WRITE_WAIT_SECONDS 2



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


////////////////////////////////////////////////////////////////////////////////
// struct of Sender Package

typedef struct tagSenderPackage
{
  HLH_RoundU32  zhrTimestamp;
  UINT16        usSliceNum;
  Slice       **ppzsSlices;
} SenderPackage;


#define IS_NULL_SENDER_PACKAGE(pzspPkg)     \
  ( (pzspPkg)->usSliceNum == 0 || (pzspPkg)->ppzsSlices == NULL )




/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/






/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/





/******************************************************************************
 * Desc : Ludp Sender
 ******************************************************************************/
class LudpSender
{

  public:
    /******************************************************************************
     * Desc : Constructor / Deconstructor
     ******************************************************************************/
    LudpSender ();

    virtual ~LudpSender ();


  public:
    /******************************************************************************
     * Desc : Operations
     ******************************************************************************/

    // Create Ludp Sender
    int Create (
        HLH_UDPSock  &zhuSock,
        UINT8         ucBaseType,
        UINT32        unTimestamp,
        UINT32        unPkgNum,
        UINT16        usMaxSliceDataLen = LUDP_SENDER_DEFAULT_MAX_SLICE_DATA_LEN
        );

    // Whether this instance created
    bool IsCreated ();

    // Destroy Ludp Sender
    int Destroy ();

    // Divide package into slice and put them into transmit queue
    int SendPackage (UINT8 *pucBuffer, UINT32 unLen);



  private:
    /******************************************************************************
     * Desc : Operations
     ******************************************************************************/

    // Send Thread Function:
    //      Send the slices in pzsSendQueue
    void __SendThreadFunc (HLH_Thread &zhtThread);

    // Wrapper of \c __SendThreadFunc
    static void * SendThreadFunc (HLH_Thread &zhtThread, void *pvThis);

    // Callback function: receive data from socket
    //      Receive the slices sent by receiver and respond to them
    static void OnReceived (void *pvThis,
        void *pvBuf, UINT32 unLen,
        HLH_SockAddr &zhsSockAddr);

    // Resend Package: Respond the slice which request to resend slices
    void ResendSlices (Slice *pzsSlice);



  private:
    /******************************************************************************
     * Desc : Private Data
     ******************************************************************************/

    ////////////////////////////////////////////////////////////////////////////////
    // Mutex of this instance
    HLH_Mutex           m_zhmMutex;

    // Whether this instance be created
    bool                m_bCreated;

    // Socket for this LUDP instance
    HLH_UDPSock       * m_pzhuSock;

    // Max length of data in slice
    UINT16              m_usMaxSliceDataLen;

    // Timestamp of Newest Package
    HLH_RoundU32        m_zhrTimestampCur;

    // Base type of slices to process by this sender
    UINT8               m_ucBaseType;


    ////////////////////////////////////////////////////////////////////////////////
    // Ring Buffer of Package List Pointer (dynamic allocated)
    SenderPackage     * m_pzsPkgList;

    // Number of Package Pointers in Ring Buffer
    UINT32              m_unPkgNum;

    // Current Position of Ring Buffer
    UINT32              m_unCurPos;


    ////////////////////////////////////////////////////////////////////////////////
    // Queue of Slices to send
    std::list<Slice *>  m_slSendQueue;

    // Condition: whether send is needed
    HLH_Cond            m_zhcSendRequestCond;


    ////////////////////////////////////////////////////////////////////////////////
    // Send Thread: 
    //      send the slices in \c m_pzsPkgList
    HLH_Thread          m_zhtSendThread;

};





#endif /* __LUDPSENDER_INC_20091013_085826_HENRY__ */
