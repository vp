/*******************************************************************************
 *         File : ludp.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-7 8:51:58
 *  Description : 
 ******************************************************************************/

#ifndef __LUDP_INC_20091007_085158_HENRY__
#define __LUDP_INC_20091007_085158_HENRY__


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Slice Type

#define LUDP_SLICE_BASE_TYPE_VIDEO              0x12
#define LUDP_SLICE_BASE_TYPE_AUDIO              0x13

#define LUDP_SLICE_SUB_TYPE_DATA                0x12
#define LUDP_SLICE_SUB_TYPE_RESEND              0x13

#define LUDP_SLICE_TYPE(base_type, sub_type)    \
  (  ( (UINT16)(base_type) << 8 )  |  ( (UINT8)(sub_type) )   )

#define LUDP_SLICE_BASE_TYPE(type)              \
  (  (UINT16)(type) >> 8  )

#define LUDP_SLICE_SUB_TYPE(type)               \
  (  (UINT8)(type)  )


#define LUDP_SLICE_TYPE_VIDEO_DATA              LUDP_SLICE_TYPE (LUDP_SLICE_BASE_TYPE_VIDEO, LUDP_SLICE_SUB_TYPE_DATA)
#define LUDP_SLICE_TYPE_VIDEO_RESEND            LUDP_SLICE_TYPE (LUDP_SLICE_BASE_TYPE_VIDEO, LUDP_SLICE_SUB_TYPE_RESEND)
#define LUDP_SLICE_TYPE_AUDIO_DATA              LUDP_SLICE_TYPE (LUDP_SLICE_BASE_TYPE_AUDIO, LUDP_SLICE_SUB_TYPE_DATA)
#define LUDP_SLICE_TYPE_AUDIO_RESEND            LUDP_SLICE_TYPE (LUDP_SLICE_BASE_TYPE_AUDIO, LUDP_SLICE_SUB_TYPE_RESEND)


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Structure of Slice

typedef struct tagSlice
{

  UINT16              usType;               // Type of this Slice
  UINT16              usDataLen;            // Length of acData
  UINT16              usSliceNum;           // Number of Slices in Package
  UINT16              usSliceSN;            // Serial Number of Slice in Package
  UINT32              unTimestamp;          // Timestamp of this Slice

  UINT8               aucData [4];          // Data of Current Slice

} Slice;


#define HLH_SLICE_LENGTH(pzsSlice)          \
  ( sizeof (Slice) - 4 + (pzsSlice)->usDataLen )



/******************************************************************************
 * Func : CloneSlice
 * Desc : Clone slice and return a new cloned slice
 * Args : pzsSlice            slice to clone
 * Outs : if success, return a new cloned slice, otherwise return NULL
 ******************************************************************************/
static inline Slice * CloneSlice (const Slice *pzsSlice)
{

  Slice    *pzsSliceNew;
  UINT32    unSliceLen;

  // Check arguments
  if (pzsSlice == NULL) {
    goto failed;
  }

  unSliceLen = HLH_SLICE_LENGTH (pzsSlice);

  // Allocate memory
  pzsSliceNew = (Slice *) malloc ( unSliceLen );
  if (pzsSliceNew == NULL) {
    goto failed;
  }

  // Copy data
  memcpy (pzsSliceNew, pzsSlice, unSliceLen);

  return pzsSliceNew;

failed:
  return NULL;

}




/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/

#include "ludp/LudpReceiver.h"
#include "ludp/LudpSender.h"



#endif /* __LUDP_INC_20091007_085158_HENRY__ */
