/*******************************************************************************
 *    File Name : LudpReceiver.cpp
 * 
 *       Author : Henry He
 * Created Time : 2009-10-13 9:02:39
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/

#include "LudpReceiver.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/






/******************************************************************************
 * Func : RoundAdd
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
static inline UINT32 RoundAdd (UINT32 unRound, UINT32 unVal1, UINT32 unVal2)
{
  UINT32 unResult;

  unResult = (unVal1 + unVal2) % unRound;

  return unResult;
}


/******************************************************************************
 * Func : RoundSub
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
static inline UINT32 RoundSub (UINT32 unRound, UINT32 unVal1, UINT32 unVal2)
{
  UINT32  unResult;

  if (unVal1 >= unVal2) {
    unResult = unVal1 - unVal2;
  } else {
    unResult = unRound + unVal1 - unVal2;
  }

  return unResult;
}






/******************************************************************************
 * Desc : Constructor / Deconstructor
 ******************************************************************************/






/******************************************************************************
 * Func : LudpReceiver
 * Desc : Constructor of LudpReceiver
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
LudpReceiver::LudpReceiver ()
{

  // Init Mutex / Cond
  m_zhmMutex.Init ();

  // Notify that this instance hasn't been created
  m_bCreated = false;

}



/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
LudpReceiver::~LudpReceiver ()
{
  Destroy ();
}






/******************************************************************************
 * Desc : Operations
 ******************************************************************************/







/******************************************************************************
 * Func : LudpReceiver::Create
 * Desc : Create instance of LudpReceiver
 * Args : pzhuSock          Socket of this receiver (shared)
 *        ucBaseType        Base type process by this receiver
 *        unPkgNum          Number of package buffer to be used
 *        unTimeoutMs       Timeout of package since last reciption
 *        unInterval        Interval of timestamp to be considered as valid
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int LudpReceiver::Create (
    HLH_UDPSock      &zhuSock,
    UINT8             ucBaseType,
    OnLudpRecvFunc    zolOnLudpRecv,
    void             *pvOnLudpRecvParam,
    UINT32            unPkgNum,
    UINT32            unTimeTickUs,
    UINT32            unTimeout,
    UINT32            unMaxRetry )
{

  // Check arguments
  ASSERT (zolOnLudpRecv != NULL);
  ASSERT (unPkgNum > 0);
  ASSERT (unTimeTickUs > 0);
  ASSERT (unTimeout > 0);
  ASSERT (unMaxRetry);

  // Lock this instance
  m_zhmMutex.Lock ();

  // Check if already created
  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_LUDP, ("already created") );
    return LUDP_RECEIVER_ERR_ALREADY_CREATED;
  }


  // Set socket pointer (share with other)
  m_pzhuSock = &zhuSock;
  zhuSock.AddRecvHandler (OnReceived, this);

  m_ucBaseType = ucBaseType;


  // Init settings
  m_zhtTimeTick.SetTime ( unTimeTickUs / 1000000, unTimeTickUs % 1000000 );
  m_unMaxTimeout  = unTimeout;
  m_unMaxRetry = unMaxRetry;


#if 0
  // Init timestamp receive range high
  m_zhrTimestampHigh = unTimestampInit + unPkgNum - 1;
#else
  m_bReceived = false;
#endif

  // Init package array
  m_unPkgNum  = unPkgNum;
  m_unHighPos = unPkgNum/2;

  m_pzrPkgList = (ReceiverPackage *) malloc ( sizeof(ReceiverPackage) * unPkgNum );
  if (m_pzrPkgList == NULL) {
    HLH_DEBUG ( HLH_DEBUG_LUDP, ("memory allocate package list failed") );
    goto fail;
  }
  memset ( m_pzrPkgList, 0, sizeof(ReceiverPackage) * unPkgNum );

  // Start timeout thread
  m_zolOnLudpRecv = zolOnLudpRecv;
  m_pvOnLudpRecvParam  = pvOnLudpRecvParam;
  m_zhtTimeoutThread.Start (TimeoutThreadFunc, this);

  // Notify that this instance created
  m_bCreated = true;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

fail:
  // Unlock this instance
  m_zhmMutex.Unlock ();

  return LUDP_RECEIVER_ERR_CANT_CREATE;

}

/******************************************************************************
 * Func : LudpReceiver::IsCreated
 * Desc : Whether this instance created
 * Args : NONE
 * Outs : If created return true, otherwise return false
 ******************************************************************************/
bool LudpReceiver::IsCreated ()
{
  bool bCreated;

  m_zhmMutex.Lock ();
  bCreated = m_bCreated;
  m_zhmMutex.Unlock ();

  return bCreated;
}




/******************************************************************************
 * Func : LudpReceiver::Destroy
 * Desc : Destory this LudpReceiver
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void LudpReceiver::Destroy ()
{

  // Stop Timeout Thread if started
  m_zhtTimeoutThread.Stop ();

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  ////////////////////////////////////////////////////////////////////////////////
  // Release package list

  if (m_pzrPkgList != NULL) {
    // Release package list
    ClearPkgListRange (0, m_unPkgNum-1);
    free (m_pzrPkgList);
    m_pzrPkgList = NULL;
  }


  ////////////////////////////////////////////////////////////////////////////////
  // Notify this LudpReceiver not created

  m_bCreated = false;

  // Unlock this instance
  m_zhmMutex.Unlock ();

}





/******************************************************************************
 * Func : LudpReceiver::IsRecvRange
 * Desc : Whether zhrTimestamp in current receive range
 * Args : zhrTimestamp                Timestamp to check
 * Outs : if zhrTimestamp in current receive range, return true
 *            otherwise return false
 ******************************************************************************/
bool LudpReceiver::IsInRecvRange (HLH_RoundU32 zhrTimestamp)
{
  return ( zhrTimestamp >= m_zhrTimestampHigh - HLH_RoundU32(m_unPkgNum)
      && zhrTimestamp <= m_zhrTimestampHigh );
}



/******************************************************************************
 * Func : LudpReceiver::UpdateRecvRange
 * Desc : Update timestamp range and package list if need according to zhrTimestamp
 * Args : zhrTimestamp           Timestamp received
 * Outs : if updated return true, otherwise return false
 ******************************************************************************/
bool LudpReceiver::UpdateRecvRange (HLH_RoundU32 zhrTimestamp)
{

  HLH_RoundU32    zhrTimestampLow;
  HLH_RoundU32    zhrOldTimestampHigh;

  UINT32          unOldHighPos;

  // Update only if new timestamp comming
  if ( zhrTimestamp > m_zhrTimestampHigh ) {
    HLH_DEBUG ( HLH_DEBUG_LUDP,
        ("before update: m_zhrTimestampHigh = %u, m_unHighPos = %u",
         (UINT32)m_zhrTimestampHigh, m_unHighPos) );

    // Update timestamps

    zhrOldTimestampHigh = m_zhrTimestampHigh;

    zhrTimestampLow     = (UINT32)zhrTimestamp - m_unPkgNum + 1;
    m_zhrTimestampHigh  = zhrTimestamp;

    // Update positions

    unOldHighPos = m_unHighPos;
    m_unHighPos = RoundAdd (m_unPkgNum, m_unHighPos,
        m_zhrTimestampHigh - zhrOldTimestampHigh);

    // Clear old range

    if (   zhrOldTimestampHigh >= zhrTimestampLow
        && zhrOldTimestampHigh < m_zhrTimestampHigh ) {
      // As zhrTimestamp > m_zhrTimestampHigh,
      // zhrOldTimestampHigh won't equal m_zhrTimestampHigh

      // If new range interleave with old range,
      //    clear some of package out of range.

      if ( m_unHighPos < unOldHighPos ) {
        if ( unOldHighPos + 1 != m_unPkgNum ) {
          ClearPkgListRange ( unOldHighPos+1, m_unPkgNum-1 );
        }
        ClearPkgListRange ( 0, m_unHighPos );
      } else { // m_unHighPos > unOldHighPos
        ClearPkgListRange ( unOldHighPos+1, m_unHighPos );
      }

    } else {
      // Otherwise clear all package
      ClearPkgListRange ( 0, m_unPkgNum - 1 );
    }

    HLH_DEBUG ( HLH_DEBUG_LUDP,
        ("after update: m_zhrTimestampHigh = %u, m_unHighPos = %u",
         (UINT32)m_zhrTimestampHigh, m_unHighPos) );
    return true;
  }

  return false;

}



/******************************************************************************
 * Func : LudpReceiver::ClearPkgListRange
 * Desc : Clear package list in range (unPosLow ~ unPosHigh)
 * Args : unPosLow          low  limit of range (included)
 *        unPosHigh         high limit of range (included)
 * Outs : NONE
 ******************************************************************************/
void LudpReceiver::ClearPkgListRange (UINT32 unPosLow, UINT32 unPosHigh)
{

  UINT32              unPkgIt;
  UINT16              usSliceIt;
  ReceiverPackage    *pzspPkg;
  Slice             **ppzsSlices;
  Slice              *pzsSlice;

  ASSERT ( unPosLow  <  m_unPkgNum );
  ASSERT ( unPosHigh <  m_unPkgNum );
  ASSERT ( unPosLow  <= unPosHigh );

  HLH_DEBUG ( HLH_DEBUG_LUDP, 
      ("clear range %u - %u", unPosLow, unPosHigh) );
  // Release packages
  for (unPkgIt = unPosLow; unPkgIt <= unPosHigh; unPkgIt ++) {
    pzspPkg = & m_pzrPkgList [unPkgIt];
    ppzsSlices = pzspPkg->ppzsSlices;
    if (ppzsSlices != NULL) {
      // Release slices
      for (usSliceIt = 0; usSliceIt < pzspPkg->usSliceNum; usSliceIt ++) {
        pzsSlice = ppzsSlices [usSliceIt];
        if (pzsSlice != NULL) {
          free (pzsSlice);
        }
      }
      free (ppzsSlices);
    }
  }

  // Clear content of packages
  pzspPkg = & m_pzrPkgList [unPosLow];
  memset ( pzspPkg, 0,
      sizeof (ReceiverPackage) * (unPosHigh - unPosLow + 1) );

}




/******************************************************************************
 * Func : ProcessSlice
 * Desc : Process Slice received
 * Args : pzsSlice            slice to be process
 * Outs : If process operation successed, return 0, otherwise return -1
 ******************************************************************************/
int LudpReceiver::ProcessSlice (Slice * pzsSlice)
{

  UINT32              unPos;
  ReceiverPackage    *pzrpPkg;
  Slice             **ppzsSlices;
  Slice              *pzsSliceNew;

  ReceiverPackage     zrpPkg;


  ASSERT (pzsSlice != NULL);

  HLH_DEBUG ( HLH_DEBUG_LUDP,
      ("new slice %u/%u of package %u arrived",
       pzsSlice->usSliceSN, pzsSlice->usSliceNum, pzsSlice->unTimestamp) );

  // Lock this instance
  m_zhmMutex.Lock ();

  // Check if this instance created
  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_LUDP, ("not created") );
    return LUDP_RECEIVER_ERR_NOT_CREATED;
  }


  // Set timestamp if first slice
  if (!m_bReceived) {
    m_bReceived = true;
    m_zhrTimestampHigh = pzsSlice->unTimestamp + m_unPkgNum/2;
  }

  if ( !IsInRecvRange (pzsSlice->unTimestamp) ) {
    HLH_DEBUG ( HLH_DEBUG_LUDP,
        ("pkg %u not in range %u - %u",
         pzsSlice->unTimestamp,
         (UINT32)m_zhrTimestampHigh-m_unPkgNum+1,
         (UINT32)m_zhrTimestampHigh) );
  }

  // Whether need to process this slice 

  if ( IsInRecvRange (pzsSlice->unTimestamp)
      || UpdateRecvRange (pzsSlice->unTimestamp) ) {

    // Caculate the position of slice
    unPos = RoundSub (m_unPkgNum, m_unHighPos,
        (UINT32)m_zhrTimestampHigh - pzsSlice->unTimestamp);

    pzrpPkg = & m_pzrPkgList [unPos];

    // Check if this package finish receiption
    if (pzrpPkg->bFinished) {
      HLH_DEBUG ( HLH_DEBUG_LUDP,
          ("receive slice of package already finished") );
      goto finished;
    }

    // Allocate package if need

    ppzsSlices = pzrpPkg->ppzsSlices;
    if (ppzsSlices == NULL) {
      // Allocate slice array
      ppzsSlices = (Slice **) malloc ( sizeof (Slice*) * pzsSlice->usSliceNum );
      if (ppzsSlices == NULL) {
        HLH_DEBUG ( HLH_DEBUG_LUDP, ("allocate slice failed") );
        goto failed;
      }
      memset ( ppzsSlices, 0, sizeof (Slice*) * pzsSlice->usSliceNum );

      // Set package info
      pzrpPkg->ppzsSlices  = ppzsSlices;
      pzrpPkg->usSliceNum  = pzsSlice->usSliceNum;
      pzrpPkg->usSliceLeft = pzsSlice->usSliceNum;
      pzrpPkg->unTimestamp = pzsSlice->unTimestamp;
      pzrpPkg->unTimeout   = m_unMaxTimeout;
      pzrpPkg->unRetry     = m_unMaxRetry;

    } else {

      // Check if there is something wrong
      ASSERT ( pzsSlice->usSliceSN  <  pzsSlice->usSliceNum );
      ASSERT ( pzrpPkg->usSliceNum  == pzsSlice->usSliceNum );
      if (pzrpPkg->unTimestamp != pzsSlice->unTimestamp) {
        HLH_DEBUG ( HLH_DEBUG_MAIN,
            ("pzrpPkg->unTimestamp = %u, pzsSlice->unTimestamp = %u",
             pzrpPkg->unTimestamp, pzsSlice->unTimestamp) );
      }
      ASSERT ( pzrpPkg->unTimestamp == pzsSlice->unTimestamp );
      ASSERT ( pzrpPkg->usSliceLeft != 0 );

    }

    // Insert this slice into package list

    // Check if already received
    if (ppzsSlices [pzsSlice->usSliceSN] != NULL) {
      HLH_DEBUG ( HLH_DEBUG_LUDP, ("slice already received") );
      goto finished;
    }

    // Clone slice
    pzsSliceNew = CloneSlice (pzsSlice);
    if (pzsSliceNew == NULL) {
      HLH_DEBUG ( HLH_DEBUG_LUDP, ("clone slice failed") );
      goto failed;
    }
    ppzsSlices [pzsSlice->usSliceSN] = pzsSliceNew;

    // Check if finish receiving of this package
    if ( --pzrpPkg->usSliceLeft == 0 ) {
      HLH_DEBUG ( HLH_DEBUG_LUDP,
          ("package %u finished", pzrpPkg->unTimestamp) );
      // Make a copy of that package
      pzrpPkg->bFinished = true;
      memcpy ( &zrpPkg, pzrpPkg, sizeof (ReceiverPackage) );
      pzrpPkg->ppzsSlices = NULL;

      // Unlock this instance
      m_zhmMutex.Unlock ();

      // Call callback function
      m_zolOnLudpRecv (m_pvOnLudpRecvParam, zrpPkg);
    } else {
finished:
      // Unlock this instance
      m_zhmMutex.Unlock ();
    }

    return 0;
  }

failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

  return LUDP_RECEIVER_ERR_FAILED;

}



/******************************************************************************
 * Func : ReceiverThread
 * Desc : Receive data from socket, callback function of socket
 * Args : pvThis         pointer to this instance
 *        pvBuf           buffer for the data received
 *        unLen           length of data received
 *        zhsPeerAddr     peer address of data received
 * Outs : NONE
 ******************************************************************************/
void LudpReceiver::OnReceived (
    void *pvThis,
    void *pvBuf, UINT32 unLen,
    HLH_SockAddr &zhsPeerAddr)
{

  LudpReceiver   *pzlrReceiver;
  Slice          *pzsSlice;

  ASSERT (pvThis != NULL);
  ASSERT (pvBuf != NULL);
  ASSERT (unLen > 0);

  (void)zhsPeerAddr;

  pzlrReceiver = (LudpReceiver*) pvThis;
  pzsSlice = (Slice*) pvBuf;

  // Check for slice length
  if ( unLen < HLH_SLICE_LENGTH (pzsSlice) ) {
    HLH_DEBUG ( HLH_DEBUG_LUDP, ("length of slice too short") );
    return;
  }

  // Process slice according to type
  if (  LUDP_SLICE_BASE_TYPE (pzsSlice->usType) == pzlrReceiver->m_ucBaseType
      && LUDP_SLICE_SUB_TYPE (pzsSlice->usType) == LUDP_SLICE_SUB_TYPE_DATA ) {
    pzlrReceiver->ProcessSlice (pzsSlice);
  } else {
    HLH_DEBUG ( HLH_DEBUG_LUDP, ("unknown slice type") );
  }

}


/******************************************************************************
 * Func : SendRequest
 * Desc : Send Request to sender to resend some slice of package (unTimestamp)
 *            Data Format:
 *              .0........1.||.2.......3.|.4.......5.| ...
 *               usSliceNum || usSliceSN | usSliceSN | ...
 * Args : unTimestamp         timestamp of package to be requested to be resent
 * Outs : If operation successed, return 0, otherwise return -1
 ******************************************************************************/
int LudpReceiver::SendRequest (UINT32 unTimestamp)
{
  UINT32            unPos;
  UINT16            usSliceLeft;
  UINT16            usSliceCur;
  UINT16            usSliceIt;
  UINT16            usSliceNum;

  ReceiverPackage  *pzrpPkg;
  Slice           **ppzsSlices;

  UINT8             aucBuf [ sizeof (Slice) - 4 + 2 + LUDP_RECEIVER_MAX_RESEND_SLICE_SN_PER_SLICE*2 ];
  Slice            *pzsSlice;
  UINT16           *pusSliceSN;

  // Check if requested unTimestamp is in current receive range
  if ( !IsInRecvRange (unTimestamp) ) {
    return LUDP_RECEIVER_ERR_FAILED;
  }

  // Get info of package
  unPos = RoundSub ( m_unPkgNum, m_unHighPos,
      (UINT32) m_zhrTimestampHigh - unTimestamp );
  pzrpPkg = & m_pzrPkgList [unPos];

  ASSERT (unTimestamp == pzrpPkg->unTimestamp);
  ASSERT (pzrpPkg->ppzsSlices != NULL);
  ASSERT (pzrpPkg->usSliceLeft <= pzrpPkg->usSliceNum);

  usSliceLeft = pzrpPkg->usSliceLeft;
  usSliceIt   = 0;
  usSliceNum  = pzrpPkg->usSliceNum;
  ppzsSlices  = pzrpPkg->ppzsSlices;

  while (usSliceLeft > 0) {

    // Caculate number of slices in current request slice
    usSliceCur
      = usSliceLeft >= LUDP_RECEIVER_MAX_RESEND_SLICE_SN_PER_SLICE
      ? LUDP_RECEIVER_MAX_RESEND_SLICE_SN_PER_SLICE : usSliceLeft;
    usSliceLeft -= usSliceCur;

    // Fill resend request
    pzsSlice = (Slice*) aucBuf;
    pusSliceSN  = (UINT16*) &pzsSlice->aucData [2];

    pzsSlice->usType      = LUDP_SLICE_TYPE (m_ucBaseType, LUDP_SLICE_SUB_TYPE_RESEND);
    pzsSlice->usDataLen   = 2 + usSliceCur*2;
    pzsSlice->usSliceNum  = 1;
    pzsSlice->usSliceSN   = 0;
    pzsSlice->unTimestamp = unTimestamp;

    // Number of slices to resend requested by this slice
    *(UINT16*) (pzsSlice->aucData) = usSliceCur;

    for ( ; usSliceIt < usSliceNum; usSliceIt ++ ) {
      if (ppzsSlices [usSliceIt] == NULL) {
        // Need resend
        *pusSliceSN++ = usSliceIt;
        HLH_DEBUG ( HLH_DEBUG_LUDP,
            ("request resend of slice %u of pkg %u",
             usSliceIt, unTimestamp) );

        if (--usSliceCur == 0) {
          break;
        }
      }
    }

    ASSERT (usSliceCur == 0);

    // Send the request slice
    m_pzhuSock->Send ( pzsSlice, HLH_SLICE_LENGTH (pzsSlice) );

  }

  return 0;

}


/******************************************************************************
 * Func : LudpReceiver::CheckTimeout
 * Desc : Check for timeout and Send request to sender to resend some slice
 * Args : zhtTimeoutThread        The thread which called this function
 * Outs : If success, return (void*) 0,  otherwise return (void*) (error code)
 ******************************************************************************/
void LudpReceiver::CheckTimeout (HLH_Thread &zhtTimeoutThread)
{

  UINT32            unPkgIt;
  UINT32            unPkgNum;

  ReceiverPackage  *pzrpPkg;

  HLH_Time          zhtTimeStart;
  HLH_Time          zhtTimeEnd;
  HLH_Time          zhtTime;

  // Notify that this function started
  zhtTimeoutThread.ThreadStarted ();

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_LUDP, ("not created") );
    return;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();


  // Loop

  do {

    // Lock this instance
    m_zhmMutex.Lock ();

    // Get start time
    zhtTimeStart = HLH_Time::GetCurrentTime ();

    // Check for timeout
    unPkgNum = m_unPkgNum;
    for (unPkgIt = 0; unPkgIt < unPkgNum; unPkgIt++) {
      pzrpPkg = & m_pzrPkgList [unPkgIt];
      // We only request to resend package with some slice received
      if ( pzrpPkg->ppzsSlices != NULL ) {
        if (pzrpPkg->unTimeout != 0) {
          pzrpPkg->unTimeout--;
          // Package timeout
          if (pzrpPkg->unTimeout == 0) {
            if (pzrpPkg->unRetry != 0) {
              pzrpPkg->unRetry --;
              pzrpPkg->unTimeout = m_unMaxTimeout;
              // Resend slices
              SendRequest (pzrpPkg->unTimestamp);
              HLH_DEBUG ( HLH_DEBUG_LUDP,
                  ("request resend of pkg %u:", pzrpPkg->unTimestamp) );
            } else {
              HLH_DEBUG ( HLH_DEBUG_LUDP, ("abort resend of pkg %u.") );
            }
          }
        }
      }
    }

    // Get end time
    zhtTimeEnd = HLH_Time::GetCurrentTime ();

    // Wait until next time tick

    zhtTime = zhtTimeEnd - zhtTimeStart;
    if (m_zhtTimeTick > zhtTime) {
      // Cacultate the time to wait
      zhtTime = m_zhtTimeTick - zhtTime;

      // Unlock this instance
      m_zhmMutex.Unlock ();

      // Wait for another time tick
      HLH_Time::Wait (zhtTime);
    } else {
      // Unlock this instance
      m_zhmMutex.Unlock ();
    }

    // Check for stop request.
    if ( zhtTimeoutThread.IsStopping () ) {
      HLH_DEBUG ( HLH_DEBUG_LUDP, ("stop on request") );
      return;
    }

  } while (1);



}


/******************************************************************************
 * Func : TimeoutThreadFunc
 * Desc : Simple wrapper of CheckTimeout to make it easy to call by thread
 * Args : pvParam             pointer to LudpReceiver
 * Outs : Always return (void*)0
 ******************************************************************************/
void * LudpReceiver::TimeoutThreadFunc (HLH_Thread &zhtTimeoutThread, void *pvThis)
{
  ASSERT (pvThis != NULL);
  ( (LudpReceiver *) pvThis )->CheckTimeout (zhtTimeoutThread);

  return (void*) 0;
}


