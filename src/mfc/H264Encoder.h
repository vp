/*******************************************************************************
 *         File : H264Encoder.h
 * 
 *       Author : Henry He
 *      Created : Fri 30 Oct 2009 01:47:53 PM CST
 *  Description : 
 ******************************************************************************/

#ifndef __H264ENCODER_INC_20091030_134753_HENRY__
#define __H264ENCODER_INC_20091030_134753_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "utils/common.h"

#include  "mfc/SsbSipH264Encode.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


//===========================  Error Code  =====================================

#define H264_ENCODER_ERR_FAILED          (-1)
#define H264_ENCODER_ERR_ALREADY_CREATED (-2)
#define H264_ENCODER_ERR_NOT_CREATED     (-3)
#define H264_ENCODER_ERR_CANT_CREATE     (-4)





/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/




/******************************************************************************
 * Desc : Classes
 ******************************************************************************/



/******************************************************************************
 * Class : H264Encoder
 * Desc  : Simple wrapper of h264 encoder functions
 ******************************************************************************/

class H264Encoder
{

  public:

    // ====================  LIFECYCLE    ========================================= 

    // Constructor of H264Encoder
    H264Encoder ();

    // Decontructor of H264Encoder
    ~ H264Encoder ();


  public:

    // ====================  OPERATORS    ========================================= 

    // ====================  OPERATIONS   ========================================= 
    
    // Create H264 encoder
    int Create (
        UINT32 unWidth, UINT32 unHeight,
        UINT32 unFramerate, UINT32 unBitrate_kbps,
        UINT32 unGOPNum);

    // Destroy H264 encoder
    void Destroy ();

    // Excute encode command for one time
    void * Encode (void *pvYUVBuf, UINT32 unYUVSize, UINT32 &unEncSize);

    // Get input buffer pointer
    void * GetInBuf (UINT32 unSize);

    // Get output buffer pointer
    void * GetOutBuf (UINT32 &unSize);

    // Set configuration of encoder
    // zheCfgType:
    //    H264_ENC_SETCONF_NUM_SLICES
    //    H264_ENC_SETCONF_PARAM_CHANGE
    //    H264_ENC_SETCONF_CUR_PIC_OPT
    int SetConfig (H264_ENC_CONF zheCfgType, void *pvVal);

    // Get configuration of encoder
    // zheCfgType:
    //    H264_ENC_GETCONF_HEADER_SIZE
    int GetConfig (H264_ENC_CONF zheCfgType, void *pvVal);

    // ====================  ACCESS       ========================================= 


  private:

    // ====================  OPERATIONS   ========================================= 


  private:

    // ====================  MEMBER DATA  ========================================= 

    // Mutex of this instance
    HLH_Mutex         m_zhmMutex;

    // Whether this instance created
    bool              m_bCreated;

    // Pointer of _MFCLIB_H264_ENC
    void             *m_pvH264Encoder;

};  // -----  end of class  H264Encoder  ----- 


#endif  // __H264ENCODER_INC_20091030_134753_HENRY__
