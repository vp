/*******************************************************************************
 *    File Name : H264Decoder.cpp
 * 
 *       Author : Henry He
 * Created Time : Fri 30 Oct 2009 01:48:19 PM CST
 *  Description : 
 ******************************************************************************/



/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "mfc/H264Decoder.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/




/******************************************************************************
 * Func : H264Decoder::H264Decoder
 * Desc : Constructor of H264Decoder
 * Args : 
 * Outs : 
 ******************************************************************************/
H264Decoder::H264Decoder ()
{

  m_zhmMutex.Init ();
  m_bCreated = false;

}   // -----  end of method H264Decoder::H264Decoder  (constructor)  ----- 


/******************************************************************************
 * Func : H264Decoder::~H264Decoder
 * Desc :  Deconstructor of H264Decoder
 * Args : 
 * Outs : 
 ******************************************************************************/
H264Decoder::~H264Decoder ()
{

  Destroy ();
  m_zhmMutex.Destroy ();

}   // -----  end of method H264Decoder::~H264Decoder  (deconstructor)  ----- 





// ====================  OPERATIONS   ========================================= 





/******************************************************************************
 * Func : H264Decoder::Create
 * Desc : Create H264 decoder
 * Args : pvEncBuf           First frame encoded by encoder,
 *                            contain config stream
 *        unEncSize           size of first frame
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int H264Decoder::Create (void *pvEncBuf, UINT32 unEncSize)
{
  int     nRetVal;
  void   *pvDecInBuf;

	SSBSIP_H264_STREAM_INFO stream_info;	

  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("already created") );
    return H264_DECODER_ERR_ALREADY_CREATED;
  }

  // Initialize H264 decoder
  m_pvH264Decoder = SsbSipH264DecodeInit ();
  if (m_pvH264Decoder == NULL) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("init failed") );
    goto failed;
  }

  pvDecInBuf = SsbSipH264DecodeGetInBuf (m_pvH264Decoder, 0);
  if (pvDecInBuf == NULL) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("init get in buffer failed") );
    goto failed;
  }

  memcpy (pvDecInBuf, pvEncBuf, unEncSize);

	nRetVal = SsbSipH264DecodeExe (m_pvH264Decoder, unEncSize);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("init execute failed") );
    goto failed;
  }

	SsbSipH264DecodeGetConfig (m_pvH264Decoder, H264_DEC_GETCONF_STREAMINFO, &stream_info);
  HLH_DEBUG ( HLH_DEBUG_MAIN, ("stream: width=%d   height=%d", stream_info.width, stream_info.height) );

  // Notify that this instance created
  m_bCreated = true;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:
  if (m_pvH264Decoder != NULL) {
    SsbSipH264DecodeDeInit (m_pvH264Decoder);
    m_pvH264Decoder = NULL;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return H264_DECODER_ERR_CANT_CREATE;
}



/******************************************************************************
 * Func : H264Decoder::Destroy
 * Desc : Destroy H264 decoder
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void H264Decoder::Destroy ()
{

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // DeInitialize H264 decoder
  SsbSipH264DecodeDeInit (m_pvH264Decoder);
  m_pvH264Decoder = NULL;

  // Notify that this instance not created
  m_bCreated = false;

  // Unlock this instance
  m_zhmMutex.Unlock ();

}





/******************************************************************************
 * Func : H264Decoder::Decode
 * Desc : Excute decode command for one time
 * Args : pvEncBuf        buffer of encoded stream
 *        unEncSize       length of encoded stream
 * Outs : If success return buffer of data decoded, otherwise return NULL
 ******************************************************************************/
void * H264Decoder::Decode (void *pvEncBuf, UINT32 unEncSize)
{
  int   nRetVal;
  void *pvDecInBuf;
	int   anYUVBuf [2];

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return NULL;
  }

  // Get decoder input buffer
  pvDecInBuf = SsbSipH264DecodeGetInBuf (m_pvH264Decoder, unEncSize);
  if (pvDecInBuf == NULL) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("get in buffer failed") );
    goto failed;
  }

  // Copy data into decode buffer
  memcpy (pvDecInBuf, pvEncBuf, unEncSize);

  // Excute decode command
  nRetVal = SsbSipH264DecodeExe (m_pvH264Decoder, (long)unEncSize);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("decode buffer failed") );
    goto failed;
  }

  // Get decode output buffer
	nRetVal = SsbSipH264DecodeGetConfig (m_pvH264Decoder, H264_DEC_GETCONF_PHYADDR_FRAM_BUF, anYUVBuf);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("decode get output buffer failed") );
    goto failed;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return (void*)anYUVBuf [0];

failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

  return NULL;
}




/******************************************************************************
 * Func : H264Decoder::GetInBuf
 * Desc : Get input buffer pointer
 * Args : unSize          size of input buffer to get
 * Outs : If success return input buffer, otherwise return NULL
 ******************************************************************************/
void * H264Decoder::GetInBuf (UINT32 unSize)
{
  void   *pvInBuf = NULL;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    goto failed;
  }

  // Excute decode command
  pvInBuf = SsbSipH264DecodeGetInBuf (m_pvH264Decoder, (long)unSize);

  // Fall through
failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

  return pvInBuf;
}




/******************************************************************************
 * Func : H264Decoder::GetOutBuf
 * Desc : Get output buffer pointer
 * Args : NONE
 * Outs : Return output buffer and size in unSize, if failed return NULL
 ******************************************************************************/
void * H264Decoder::GetOutBuf (UINT32 &unSize)
{
  void   *pvOutBuf = NULL;

  unSize = 0;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    goto failed;
  }

  // Excute decode command
  pvOutBuf = SsbSipH264DecodeGetOutBuf (m_pvH264Decoder, (long*)&unSize);

  // Fall through
failed:

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return pvOutBuf;
}


/******************************************************************************
 * Func : H264Decoder::SetConfig
 * Desc : Set configuration of decoder
 *        zheCfgType:
 *            H264_DEC_SETCONF_POST_ROTATE
 * Args : zheCfgType        Type of configuration
 *        pvCfgVal          Value of configuration
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int H264Decoder::SetConfig (H264_DEC_CONF zheCfgType, void *pvCfgVal)
{
  int nRetVal;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return H264_DECODER_ERR_NOT_CREATED;
  }

  // Excute decode command
  nRetVal = SsbSipH264DecodeSetConfig (m_pvH264Decoder, zheCfgType, pvCfgVal);
  if (nRetVal < 0) {
    nRetVal = H264_DECODER_ERR_FAILED;
  } else {
    nRetVal = 0;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return nRetVal;
}


/******************************************************************************
 * Func : H264Decoder::GetConfig
 * Desc : Get configuration of decoder
 *        zheCfgType:
 *            H264_DEC_GETCONF_STREAMINFO
 *            H264_DEC_GETCONF_PHYADDR_FRAM_BUF
 *            H264_DEC_GETCONF_FRAM_NEED_COUNT
 * Args : zheCfgType        Type of configuration
 *        pvCfgVal          Value of configuration
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int H264Decoder::GetConfig (H264_DEC_CONF zheCfgType, void *pvCfgVal)
{
  int nRetVal;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return H264_DECODER_ERR_NOT_CREATED;
  }

  // Excute decode command
  nRetVal = SsbSipH264DecodeGetConfig (m_pvH264Decoder, zheCfgType, pvCfgVal);
  if (nRetVal < 0) {
    nRetVal = H264_DECODER_ERR_FAILED;
  } else {
    nRetVal = 0;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return nRetVal;
}




