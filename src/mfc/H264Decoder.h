/*******************************************************************************
 *         File : H264Decoder.h
 * 
 *       Author : Henry He
 *      Created : Fri 30 Oct 2009 01:48:11 PM CST
 *  Description : 
 ******************************************************************************/

#ifndef __H264DECODER_INC_20091030_134811_HENRY__
#define __H264DECODER_INC_20091030_134811_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "utils/common.h"

#include  "mfc/SsbSipH264Decode.h"




/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

//===========================  Error Code  =====================================

#define H264_DECODER_ERR_FAILED          (-1)
#define H264_DECODER_ERR_ALREADY_CREATED (-2)
#define H264_DECODER_ERR_NOT_CREATED     (-3)
#define H264_DECODER_ERR_CANT_CREATE     (-4)



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/



/******************************************************************************
 * Class : H264Decoder
 * Desc  : Simple wrapper of h264 decoder functions
 ******************************************************************************/

class H264Decoder
{

  public:

    // ====================  LIFECYCLE    ========================================= 

    // Constructor of H264Decoder
    H264Decoder ();

    // Decontructor of H264Decoder
    ~ H264Decoder ();


  public:

    // ====================  OPERATORS    ========================================= 

    // ====================  OPERATIONS   ========================================= 
    
    // Create H264 decoder
    int Create (void *pvEncBuf, UINT32 unEncSize);

    // Destroy H264 decoder
    void Destroy ();

    // Excute decode command for one time
    void * Decode (void *pvEncBuf, UINT32 unEncSize);

    // Get input buffer pointer
    void * GetInBuf (UINT32 unSize);

    // Get output buffer pointer
    void * GetOutBuf (UINT32 &unSize);

    // Set configuration of decoder
    // zheCfgType:
    //    H264_DEC_SETCONF_POST_ROTATE
    int SetConfig (H264_DEC_CONF zheCfgType, void *pvVal);

    // Get configuration of decoder
    // zheCfgType:
    //    H264_DEC_GETCONF_STREAMINFO
    //    H264_DEC_GETCONF_PHYADDR_FRAM_BUF
    //    H264_DEC_GETCONF_FRAM_NEED_COUNT
    int GetConfig (H264_DEC_CONF zheCfgType, void *pvVal);

    // ====================  ACCESS       ========================================= 


  private:

    // ====================  OPERATIONS   ========================================= 


  private:

    // ====================  MEMBER DATA  ========================================= 

    // Mutex of this instance
    HLH_Mutex         m_zhmMutex;

    // Whether this instance created
    bool              m_bCreated;

    // Pointer of _MFCLIB_H264_DEC
    void             *m_pvH264Decoder;

};  // -----  end of class  H264Decoder  ----- 






#endif /* __H264DECODER_INC_20091030_134811_HENRY__ */
