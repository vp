/*******************************************************************************
 *    File Name : H264Encoder.cpp
 * 
 *       Author : Henry He
 * Created Time : Fri 30 Oct 2009 01:45:59 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "mfc/H264Encoder.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/




/******************************************************************************
 * Func : H264Encoder::H264Encoder
 * Desc : Constructor of H264Encoder
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
H264Encoder::H264Encoder ()
{

  m_zhmMutex.Init ();
  m_bCreated = false;

}   // -----  end of method H264Encoder::H264Encoder  (constructor)  ----- 


/******************************************************************************
 * Func : H264Encoder::~H264Encoder
 * Desc :  Deconstructor of H264Encoder
 * Args : 
 * Outs : 
 ******************************************************************************/
H264Encoder::~H264Encoder ()
{

  Destroy ();
  m_zhmMutex.Destroy ();

}   // -----  end of method H264Encoder::~H264Encoder  (deconstructor)  ----- 





// ====================  OPERATIONS   ========================================= 





/******************************************************************************
 * Func : H264Encoder::Create
 * Desc : Create H264 encoder
 * Args : unWidth         Width  of images to encode
 *        unHeight        Heigth of images to encode
 *        unFramerate     Framerate of encoded video
 *        unBitrate_kbps  Bit rate of encoded video
 *        unGOPNum        gop of encoded video
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int H264Encoder::Create (
    UINT32 unWidth, UINT32 unHeight,
    UINT32 unFramerate, UINT32 unBitrate_kbps,
    UINT32 unGOPNum)
{
	int nRetVal;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("already created") );
    return H264_ENCODER_ERR_ALREADY_CREATED;
  }

  // Initialize H264 encoder
  m_pvH264Encoder = SsbSipH264EncodeInit (unWidth, unHeight,
      unFramerate, unBitrate_kbps, unGOPNum);
  if (m_pvH264Encoder == NULL) {
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("init failed") );
    goto failed;
  }

	// Excute for the first time
	nRetVal = SsbSipH264EncodeExe (m_pvH264Encoder);
	if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("execute for the first time failed") );
		goto failed;
	}

  // Notify that this instance created
  m_bCreated = true;

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return 0;

failed:
	if (m_pvH264Encoder != NULL) {
		SsbSipH264EncodeDeInit (m_pvH264Encoder);
		m_pvH264Encoder = NULL;
	}

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return H264_ENCODER_ERR_CANT_CREATE;
}



/******************************************************************************
 * Func : H264Encoder::Destroy
 * Desc : Destroy H264 encoder
 * Args : NONE
 * Outs : NONE
 ******************************************************************************/
void H264Encoder::Destroy ()
{

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return;
  }

  // DeInitialize H264 encoder
  SsbSipH264EncodeDeInit (m_pvH264Encoder);
  m_pvH264Encoder = NULL;

  // Notify that this instance not created
  m_bCreated = false;

  // Unlock this instance
  m_zhmMutex.Unlock ();

}





/******************************************************************************
 * Func : H264Encoder::Execute
 * Desc : Excute encode command for one time
 * Args : pvYUVBuf					buffer of YUV data
 * 				unYUVSize					size   of YUV data
 * 				unEncSize					used to save the encoded size returned
 * Outs : If success return encoded buffer, otherwise return NULL
 ******************************************************************************/
void * H264Encoder::Encode (void *pvYUVBuf, UINT32 unYUVSize, UINT32 &unEncSize)
{
  int   nRetVal;
  void *pvEncInBuf;
	void *pvEncOutBuf;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("not created") );
    return NULL;
  }

	// Copy YUV data into encode buffer
	pvEncInBuf = SsbSipH264EncodeGetInBuf(m_pvH264Encoder, unYUVSize);
	if (pvEncInBuf == NULL) {
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("get input buffer failed") );
		goto failed;
	}
	
	memcpy (pvEncInBuf, pvYUVBuf, unYUVSize);

  // Excute encode command
  nRetVal = SsbSipH264EncodeExe (m_pvH264Encoder);
	if (nRetVal < 0) {
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("excute failed") );
		goto failed;
	}

	pvEncOutBuf = SsbSipH264EncodeGetOutBuf (m_pvH264Encoder, (long*)&unEncSize);

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return pvEncOutBuf;

failed:

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return NULL;
}




/******************************************************************************
 * Func : H264Encoder::GetInBuf
 * Desc : Get input buffer pointer
 * Args : unSize          size of input buffer
 * Outs : If success return input buffer, otherwise return NULL
 ******************************************************************************/
void * H264Encoder::GetInBuf (UINT32 unSize)
{
  void   *pvInBuf = NULL;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    goto failed;
  }

  // Excute encode command
  pvInBuf = SsbSipH264EncodeGetInBuf (m_pvH264Encoder, (long)unSize);

  // Fall through
failed:
  // Unlock this instance
  m_zhmMutex.Unlock ();

  return pvInBuf;
}




/******************************************************************************
 * Func : H264Encoder::GetOutBuf
 * Desc : Get output buffer pointer
 * Args : NONE
 * Outs : Return output buffer and size in unSize, if failed return NULL
 ******************************************************************************/
void * H264Encoder::GetOutBuf (UINT32 &unSize)
{
  void   *pvOutBuf = NULL;

  unSize = 0;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    goto failed;
  }

  // Excute encode command
  pvOutBuf = SsbSipH264EncodeGetOutBuf (m_pvH264Encoder, (long*)&unSize);

  // Fall through
failed:

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return pvOutBuf;
}


/******************************************************************************
 * Func : H264Encoder::SetConfig
 * Desc : Set configuration of encoder
 *        zheCfgType:
 *           H264_ENC_SETCONF_NUM_SLICES
 *           H264_ENC_SETCONF_PARAM_CHANGE
 *           H264_ENC_SETCONF_CUR_PIC_OPT
 * Args : zheCfgType        Type of configuration
 *        pvVal             Value of configuration
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int H264Encoder::SetConfig (H264_ENC_CONF zheCfgType, void *pvCfgVal)
{
  int nRetVal;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return H264_ENCODER_ERR_NOT_CREATED;
  }

  // Excute encode command
  nRetVal = SsbSipH264EncodeSetConfig (m_pvH264Encoder, zheCfgType, pvCfgVal);
  if (nRetVal < 0) {
    nRetVal = H264_ENCODER_ERR_FAILED;
  } else {
    nRetVal = 0;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return nRetVal;
}


/******************************************************************************
 * Func : H264Encoder::GetConfig
 * Desc : Get configuration of encoder
 *        zheCfgType:
 *           H264_ENC_GETCONF_HEADER_SIZE
 * Args : zheCfgType        Type of configuration
 *        pvVal             Value of configuration
 * Outs : If success return 0, otherwise return error code
 ******************************************************************************/
int H264Encoder::GetConfig (H264_ENC_CONF zheCfgType, void *pvCfgVal)
{
  int nRetVal;

  // Lock this instance
  m_zhmMutex.Lock ();

  if (!m_bCreated) {
    // Unlock this instance
    m_zhmMutex.Unlock ();
    return H264_ENCODER_ERR_NOT_CREATED;
  }

  // Excute encode command
  nRetVal = SsbSipH264EncodeGetConfig (m_pvH264Encoder, zheCfgType, pvCfgVal);
  if (nRetVal < 0) {
    nRetVal = H264_ENCODER_ERR_FAILED;
  } else {
    nRetVal = 0;
  }

  // Unlock this instance
  m_zhmMutex.Unlock ();

  return nRetVal;
}



