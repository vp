/*******************************************************************************
 *         File : HLH_Audio.h
 * 
 *       Author : Henry He
 *      Created : 2009-10-16 10:03:49
 *  Description : 
 ******************************************************************************/

#ifndef __HLH_AUDIO_INC_20091016_100349_HENRY__
#define __HLH_AUDIO_INC_20091016_100349_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/

#include "utils/common.h"


#if (__USE_FAKE_AUDIO__ <= 0)

/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Error Codes

#define HLH_AUDIO_ERR_FAILED            (-1)
#define HLH_AUDIO_ERR_ALREADYCREATED    (-2)
#define HLH_AUDIO_ERR_NOT_CREATED       (-3)

////////////////////////////////////////////////////////////////////////////////
// Default

#define HLH_AUDIO_DEFAULT_BUF_NUMBER    8
#define HLH_AUDIO_DEFAULT_BUF_SIZE_LOG2 8       /* 2^8 = 256 */
#define HLH_AUDIO_DEFAULT_SAMPLE_BITS   AFMT_U8
#define HLH_AUDIO_DEFAULT_SAMPLE_RATE   8000
#define HLH_AUDIO_DEFAULT_CHANNELS      1


////////////////////////////////////////////////////////////////////////////////
// Timeouts

#define HLH_AUDIO_TIMEOUT_POLLWAIT_SECS 2

////////////////////////////////////////////////////////////////////////////////
// Poll Types

#define HLH_AUDIO_POLL_READ             0x01
#define HLH_AUDIO_POLL_WRITE            0x02
#define HLH_AUDIO_POLL_ERROR            0x04



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Typedef of callback function called when audio read
typedef void (*OnAudioReadFunc) (void *pvParam, void *pvBuf, UINT32 unLen);


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/





/*******************************************************************************
 * Desc : Classes
 ******************************************************************************/




////////////////////////////////////////////////////////////////////////////////
// Simple wrapper of audio device

class HLH_Audio
{

  public:

    ////////////////////////////////////////////////////////////////////////////////
    // Constructor / Deconstructor
    HLH_Audio ();
    ~HLH_Audio ();

  public:

    ////////////////////////////////////////////////////////////////////////////////
    // Create audio for read/write
    int Create (
        OnAudioReadFunc    zoaOnRead     = NULL,
        void             * pvOnReadParam = NULL,
        UINT32             unSampleBits  = HLH_AUDIO_DEFAULT_SAMPLE_BITS,
        UINT32             unSampleRate  = HLH_AUDIO_DEFAULT_SAMPLE_RATE,
        UINT32             unChannels    = HLH_AUDIO_DEFAULT_CHANNELS,
        UINT16             unBufSizeLog2 = HLH_AUDIO_DEFAULT_BUF_SIZE_LOG2,
        UINT16             unBufNum      = HLH_AUDIO_DEFAULT_BUF_NUMBER
        );

    // Close the audio device and 
    void Destroy ();

    // Whether this instance is created
    bool IsCreated ();

    // Read for audio device
    int Read (void *pvBuf, UINT32 unLen);

    // Write to audio device
    int Write (void *pvBuf, UINT32 unLen);

    ////////////////////////////////////////////////////////////////////////////////
    // Poll for specific event
    int Poll (UINT32 &unPollType);

    // Poll for specific event util time run out
    int PollWait (UINT32 &unPollType, HLH_Time &zhtTime);

  private:
    ////////////////////////////////////////////////////////////////////////////////
    // Private operations

    // Read thread to poll for read event and call callback function of read
    void __ReadThreadFunc (HLH_Thread &zhtThread);
    static void * ReadThreadFunc (HLH_Thread &zhtThread, void *pvThis);

  private:
    ////////////////////////////////////////////////////////////////////////////////
    // Mutex for this instance
    HLH_Mutex       m_zhmMutex;

    // Mutex for read
    HLH_Mutex       m_zhmReadMutex;

    // Mutex for write
    HLH_Mutex       m_zhmWriteMutex;

    // Whether this instance created
    bool            m_bCreated;


    ////////////////////////////////////////////////////////////////////////////////
    // Thread to read audio and call callback function of read
    HLH_Thread      m_zhtReadThread;

    // Read Handler
    OnAudioReadFunc m_zoaOnRead;

    // Parameter to m_zoaOnRead;
    void          * m_pvOnReadParam;

    char          * m_pcReadBuf;
    UINT32          m_unReadLen;

    ////////////////////////////////////////////////////////////////////////////////
    // File description of audio device
    int             m_fdAudio;

    // Poll file discriptor set: initialized when created
    fd_set          m_fsReadSet;
    fd_set          m_fsWriteSet;
    fd_set          m_fsErrorSet;

};

#endif /* (__USE_FAKE_AUDIO__ <= 0) */

#endif /* __HLH_AUDIO_INC_20091016_100349_HENRY__ */



