/*******************************************************************************
 *         File : common.h
 * 
 *       Author : Henry He
 *      Created : Mon 02 Nov 2009 09:40:43 AM CST
 *  Description : 
 ******************************************************************************/

#ifndef __COMMON_INC_20091102_094043_HENRY__
#define __COMMON_INC_20091102_094043_HENRY__


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "audio/HLH_Audio.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/


#endif /* __COMMON_INC_20091102_094043_HENRY__ */
