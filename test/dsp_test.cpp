/*******************************************************************************
 *    File Name : test/dsp_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Fri 23 Oct 2009 10:49:00 AM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "src/common.h"
#include  <sys/soundcard.h>


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/
#define DSP_TEST_TIME_SECS   10

/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/
#if 0
static UINT32   m_unFrameCnt;
static UINT32   m_unFramesLength;
#endif


// Audio buffer
static UINT8    m_aucAudioBuffer [8000 * DSP_TEST_TIME_SECS];
static volatile UINT8   *m_pucAudioBuffer;
static volatile UINT32   m_unAudioCntLeft;


/******************************************************************************
 * Desc : Functions
 ******************************************************************************/
void OnAudioRead (void *pvParam, void *pvBuf, UINT32 unLen)
{
  HLH_Audio    *pzhaAudioDev;

  ASSERT (pvParam != NULL);

#if 0
  m_unFrameCnt++;
  m_unFramesLength += unLen;
#endif

  pzhaAudioDev = (HLH_Audio *) pvParam;

#if 0
  pzhaAudioDev->Write (pvBuf, unLen);
#else
  if ( m_unAudioCntLeft > 0 ) {
    UINT32 unCopyLen;
    unCopyLen = m_unAudioCntLeft >= unLen ? unLen : m_unAudioCntLeft;
    memcpy ((void*)m_pucAudioBuffer, pvBuf, unCopyLen);
    m_pucAudioBuffer += unCopyLen;
    m_unAudioCntLeft -= unCopyLen;
  }
#endif
}





/******************************************************************************
 * Func : main
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ()
{
  int       nRetVal;
  HLH_Audio zhaAudioDev;

#if 0
  HLH_Time  m_zhtStartTime;
  HLH_Time  m_zhtEndTime;

  UINT32    m_unSeconds;

  m_unFrameCnt = 0;
  m_unFramesLength = 0;
  m_zhtStartTime = HLH_Time::GetCurrentTime ();
#endif

#if 1
  int vol;
  int mixer_fd;

  mixer_fd = open ("/dev/mixer", O_RDWR);
  if (mixer_fd < 0) {
    Printf ("can't open mixer\n");
    goto failed;
  }

  nRetVal = ioctl (mixer_fd, SOUND_MIXER_READ_MIC, &vol);
  if (nRetVal == -1) {
    Printf ("can't get mic volumn");
    goto failed;
  }
  Printf ("volume = %d\n", vol);

  vol = 80;
  nRetVal = ioctl (mixer_fd, SOUND_MIXER_WRITE_MIC, &vol);
  if (nRetVal == -1) {
    Printf ("can't set mic volumn\n");
    goto failed;
  }
  Printf ("set volumn to %d\n", vol);
#endif

  m_pucAudioBuffer = m_aucAudioBuffer;
  m_unAudioCntLeft = sizeof(m_aucAudioBuffer);

  nRetVal = zhaAudioDev.Create (OnAudioRead, &zhaAudioDev, AFMT_U8, 8000, 1, 8, 16) ;

  if (nRetVal < 0) {
    goto failed;
  }

  Printf ("Start to record (%ds)\n", DSP_TEST_TIME_SECS);
  while ( m_unAudioCntLeft != 0 ) {
    HLH_Time::Wait (HLH_Time(1, 0));
  }

  Printf ("Playback\n");
  zhaAudioDev.Write ( m_aucAudioBuffer, sizeof(m_aucAudioBuffer) );


#if 0
  m_zhtEndTime = HLH_Time::GetCurrentTime ();

  m_unSeconds = m_zhtEndTime.GetSeconds () - m_zhtStartTime.GetSeconds ();
  Printf ("Time = %u, Frames = %u, Frames Length = %u\n",
      m_unSeconds, m_unFrameCnt, m_unFramesLength);
  Printf ("FPS = %u fps, BPS = %u Bps", m_unFrameCnt/m_unSeconds, m_unFramesLength/m_unSeconds);
#endif

  zhaAudioDev.Destroy ();

  return 0;

failed:
  return -1;

} /* ----------  end of function main  ---------- */


