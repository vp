/*******************************************************************************
 *    File Name : h264_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Sat 31 Oct 2009 11:35:04 AM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "fb/common.h"
#include  "pp/common.h"
#include  "mfc/common.h"
#include  "test/FrameExtractor.h"
#include  "test/H264Frames.h"

#include  <sys/time.h>
#include  <sys/mman.h>

/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

#define INPUT_BUFFER_SIZE		(204800)


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/
static unsigned char delimiter_h264[4]  = {0x00, 0x00, 0x00, 0x01};


unsigned int measureTime(struct timeval *start, struct timeval *stop)
{
	unsigned int sec, usec, time;

	sec = stop->tv_sec - start->tv_sec;
	if(stop->tv_usec >= start->tv_usec)
	{
		usec = stop->tv_usec - start->tv_usec;
	}
   	else
	{	
		usec = stop->tv_usec + 1000000 - start->tv_usec;
		sec--;
  	}
	time = sec*1000 + (unsigned int)(((double)usec)/1000);
	return time;
}

/******************************************************************************
 * Func : main
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ( int argc, char *argv[] )
{
  int                      nRetVal;
  FrameBuffer              zfbFrameBuffer;
  PostProcessor            zppPostProcessor;
  H264Decoder              zhdDecoder;

  int                      in_fd            = -1;
  char                    *in_addr          = NULL;
  struct stat              s;
  int                      file_size        = 0;

  FRAMEX_CTX              *pFrameExCtx;             // frame extractor context
  FRAMEX_STRM_PTR          file_strm;

  void                    *pStrmBuf         = NULL;
  int                      nFrameLeng       = 0;
  void                    *pYUVBuf;

  SSBSIP_H264_STREAM_INFO  stream_info;

  struct timeval           start;
  struct timeval           stop;
  unsigned int             time             = 0;
  int                      frame_cnt        = 0;

  UINT32                   unTime;

  if (argc != 2) {
    Printf("Usage : #./mfc <file name>\n");
    Printf("   - <file name> : H.264 file to be displayed.\n");
    return -1;
  }


  nRetVal = zfbFrameBuffer.Create ("/dev/fb0");
  if (nRetVal < 0) {
    Printf ("Can't create frame buffer\n");
    goto failed;
  }

  nRetVal = zppPostProcessor.Create ("/dev/misc/s3c-pp");
  if (nRetVal < 0) {
    Printf ("Can't create post processor\n");
    goto failed;
  }

  // in file open
  in_fd	= open (argv[1], O_RDONLY);
  if(in_fd < 0) {
    printf("Input file open failed\n");
    goto failed;
  }

  // get input file size
  fstat(in_fd, &s);
  file_size = s.st_size;

  // mapping input file to memory
  in_addr = (char *)mmap(0, file_size, PROT_READ, MAP_SHARED, in_fd, 0);
  if(in_addr == NULL) {
    Printf("input file memory mapping failed\n");
    return -1;
  }


  ///////////////////////////////////
  // FrameExtractor Initialization //
  ///////////////////////////////////
  pFrameExCtx = FrameExtractorInit(FRAMEX_IN_TYPE_MEM, delimiter_h264, sizeof(delimiter_h264), 1);   
  file_strm.p_start = file_strm.p_cur = (unsigned char *)in_addr;
  file_strm.p_end = (unsigned char *)(in_addr + file_size);
  FrameExtractorFirst(pFrameExCtx, &file_strm);

  ////////////////////////////////////
  //  H264 CONFIG stream extraction //
  ////////////////////////////////////
  pStrmBuf = malloc (INPUT_BUFFER_SIZE);
  if (pStrmBuf < 0) {
    Printf ("can't allocate memory");
    goto failed;
  }
  nFrameLeng = ExtractConfigStreamH264(pFrameExCtx, &file_strm, (unsigned char *)pStrmBuf, INPUT_BUFFER_SIZE, NULL);
  printf ("nFrameLeng = %d\n", nFrameLeng);

  nRetVal = zhdDecoder.Create (pStrmBuf, nFrameLeng);
  if (nRetVal < 0) {
    Printf ("Can't create decoder\n");
    goto failed;
  }


  /////////////////////////////////////
  ///   4. Get stream information   ///
  /////////////////////////////////////
#if 1
  zhdDecoder.GetConfig (H264_DEC_GETCONF_STREAMINFO, &stream_info);

  printf("\t<STREAMINFO> width=%d   height=%d    buf_width=%d    buf_height=%d.\n", 	\
      stream_info.width, stream_info.height, stream_info.buf_width, stream_info.buf_height);
#else
  stream_info.width      = 320;
  stream_info.height     = 240;
  stream_info.buf_width  = 320;
  stream_info.buf_height = 240;
#endif

  // Set post processor configuration
#if 0
  zppPostProcessor.SetParam ( stream_info.buf_width, stream_info.buf_height,
      0, 0, stream_info.buf_width, stream_info.buf_height, YC420,
      zfbFrameBuffer.GetVirtualWidth (), zfbFrameBuffer.GetVirtualHeight (),
      0, 0, zfbFrameBuffer.GetWidth (), zfbFrameBuffer.GetHeight (),
      zfbFrameBuffer.GetColor () );
#else
  zppPostProcessor.SetParam ( stream_info.buf_width, stream_info.buf_height,
      0, 0, stream_info.buf_width, stream_info.buf_height, YC420,
      zfbFrameBuffer.GetVirtualWidth (), zfbFrameBuffer.GetVirtualHeight (),
      50, 80, 480, 320,
      zfbFrameBuffer.GetColor () );
#endif

  while (1) {
    gettimeofday(&start, NULL);

    //////////////////////////////////
    ///       5. DECODE            ///
    ///    (SsbSipH264DecodeExe)   ///
    //////////////////////////////////
    pYUVBuf = zhdDecoder.Decode (pStrmBuf, nFrameLeng);
    if (pYUVBuf == NULL)
      break;

    /////////////////////////////
    // Next H.264 VIDEO stream //
    /////////////////////////////
    nFrameLeng = NextFrameH264 (pFrameExCtx, &file_strm, (unsigned char *)pStrmBuf, INPUT_BUFFER_SIZE, NULL);
    if (nFrameLeng < 4)
      break;

    // Post processing

    zppPostProcessor.Process ( pYUVBuf, zfbFrameBuffer.GetFrame(0) );

    gettimeofday(&stop, NULL);

    unTime = measureTime (&start, &stop);
    if (unTime < 40*1000) {
      usleep (40*1000 - unTime);
    }

    time += unTime;
    frame_cnt++;
  }

  printf ("Display Time : %u, Frame Count : %d, FPS : %f\n", time, frame_cnt, (float)frame_cnt*1000/time);

  munmap(in_addr, file_size);
  close(in_fd);

  zfbFrameBuffer.Destroy ();
  zppPostProcessor.Destroy ();
  zhdDecoder.Destroy ();

	return 0;

failed:
  zfbFrameBuffer.Destroy ();
  zppPostProcessor.Destroy ();
  zhdDecoder.Destroy ();

  if (in_addr) {
    munmap(in_addr, file_size);
  }

  if (in_fd) {
    close(in_fd);
  }

  return -1;

}				/* ----------  end of function main  ---------- */
