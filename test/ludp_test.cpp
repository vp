/*******************************************************************************
 *    File Name : ludp_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Tue 03 Nov 2009 04:23:20 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/

#include  "utils/common.h"
#include  "ludp/common.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Audio LudpReceiver of VPhone

#define VPHONE_ALR_PKG_NUM                    (8000/160 * 1/3 * 15/10)
#define VPHONE_ALR_TIMETICK_US                (50 * 1000)
#define VPHONE_ALR_TIMOUT_IN_TIMETICK         5
#define VPHONE_ALR_MAX_RETRY                  3

////////////////////////////////////////////////////////////////////////////////
// Audio LudpSender of VPhone

#define VPHONE_ALS_PKG_NUM                    (8000/160 * 1/3 * 20/10)



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/


static char message [1000*3+100];

static const char small_msg [] = "  - abcdefgh";

static HLH_UDPSock   zhuSenderSock;
static HLH_UDPSock   zhuReceiverSock;

static LudpSender    zlsSender;
static LudpReceiver  zlrReceiver;


/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void dump_message (void *pvBuf, UINT32 unLen)
{
  UINT32    unIt;
  char     *pcBuf;
  char      cChar;
  UINT32    unMsgIt;

  pcBuf = (char *) pvBuf;

  for (unIt = 0; unIt < unLen; unIt += 1000, pcBuf += 1000) {
    unMsgIt = unLen - unIt;
    unMsgIt = unMsgIt < sizeof(small_msg) ? unMsgIt - 1 : sizeof(small_msg) - 1 ;
    cChar = pcBuf[unMsgIt];
    pcBuf[unMsgIt] = '\0';
    Printf ("%s\n", pcBuf);
    pcBuf[unMsgIt] = cChar;
  }
}

/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void OnAudioLudpRecv (void *pvNULL, ReceiverPackage &zrpPkg)
{
  UINT32   unIt;
  UINT32   unNum;
  Slice  **ppzsSlices;
  Slice   *pzsSlice;

  (void)pvNULL;

  ASSERT (zrpPkg.bFinished);
  ASSERT (zrpPkg.ppzsSlices);

  Printf ("--------------------------------------------------------------------------------\n");
  HLH_DEBUG ( HLH_DEBUG_MAIN, ("Get package %d:\n", zrpPkg.unTimestamp) );
  unNum=zrpPkg.usSliceNum;
  ppzsSlices = zrpPkg.ppzsSlices;
  for (unIt=0; unIt < unNum; unIt++) {
    pzsSlice = ppzsSlices [unIt];
    ASSERT (pzsSlice != NULL);
    pzsSlice->aucData[pzsSlice->usDataLen-1] = '\0';
    Printf ("%s\n", pzsSlice->aucData);
  }
  Printf ("--------------------------------------------------------------------------------\n");
}

/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ( int argc, char *argv[] )
{
  UINT32       unCnt;
  HLH_SockAddr zhsSockAddr;

  (void) argc;
  (void) argv;

  HLH_ENTER_FUNC (HLH_DEBUG_MAIN);

  // Prepare the messeage for check
  memcpy (message + 1000 * 0, small_msg, sizeof(small_msg));
  message [1000 * 0] = '0';
  memcpy (message + 1000 * 1, small_msg, sizeof(small_msg));
  message [1000 * 1] = '1';
  memcpy (message + 1000 * 2, small_msg, sizeof(small_msg));
  message [1000 * 2] = '2';
  memcpy (message + 1000 * 3, small_msg, sizeof(small_msg));
  message [1000 * 3] = '3';

  // Create Sender Socket
	zhsSockAddr.SetAddr      ( INADDR_ANY,    8000);
	zhuSenderSock.Create     ( zhsSockAddr);
	zhsSockAddr.SetAddr      ( "127.0.0.1",   8002);
	zhuSenderSock.Connect    ( zhsSockAddr);
  HLH_DEBUG ( HLH_DEBUG_MAIN, ("zhuSenderSock created") );

	// Create Receiver Socket
	zhsSockAddr.SetAddr      ( INADDR_ANY,    8002);
	zhuReceiverSock.Create   ( zhsSockAddr);
	zhsSockAddr.SetAddr      ( "127.0.0.1",   8000);
	zhuReceiverSock.Connect  ( zhsSockAddr);
  HLH_DEBUG ( HLH_DEBUG_MAIN, ("zhuReceiverSock created") );

  // Create Ludp Receiver
  zlrReceiver.Create (
      zhuReceiverSock, LUDP_SLICE_BASE_TYPE_AUDIO,
      0, OnAudioLudpRecv, NULL,
      VPHONE_ALR_PKG_NUM,
      VPHONE_ALR_TIMETICK_US,
      VPHONE_ALR_TIMOUT_IN_TIMETICK,
      VPHONE_ALR_MAX_RETRY );
  HLH_DEBUG ( HLH_DEBUG_MAIN, ("zlrReceiver created") );

  // Create Ludp Sender
  zlsSender.Create ( zhuSenderSock, LUDP_SLICE_BASE_TYPE_AUDIO,
      0, VPHONE_ALS_PKG_NUM );
  HLH_DEBUG ( HLH_DEBUG_MAIN, ("zlsSender created") );

  char cChar = 'A';
  unCnt = 0;
  do {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("zlsSender send message") );

    message [0] = cChar++;
    if (cChar > 'Z') {
      cChar = 'A';
    }
    zlsSender.SendPackage ((UINT8*)message, sizeof(message));

    HLH_DEBUG ( HLH_DEBUG_MAIN, ("zlsSender send message end") );

    HLH_Time::Wait (HLH_Time (0, 500*1000));
    if (unCnt++ == 50) {
      break;
    }
  } while (1);

  zhuSenderSock.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_MAIN, ("Destroy zhuSenderSock") );
  zhuReceiverSock.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_MAIN, ("Destroy zhuReceiverSock") );


  zlrReceiver.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_MAIN, ("Destroy zlrReceiver") );
  zlsSender.Destroy ();
  HLH_DEBUG ( HLH_DEBUG_MAIN, ("Destroy zlsSender") );

  return 0;
}				/* ----------  end of function main  ---------- */
