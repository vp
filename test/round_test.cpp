/*******************************************************************************
 *    File Name : round_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Tue 03 Nov 2009 01:02:39 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

#define EXE_ECHO(expr)                     \
  do {                                     \
    (expr);                                \
    HLH_DEBUG ( HLH_DEBUG_MAIN, (#expr) ); \
  } while (0)

#define EXE_CHK(expr, expr_eq)             \
  do {                                     \
    if ( (expr) != (expr_eq) ) {           \
      HLH_DEBUG (  HLH_DEBUG_MAIN,         \
          ( #expr" != "#expr_eq )  );      \
    } else {                               \
      HLH_DEBUG (  HLH_DEBUG_MAIN,         \
          ( #expr" == "#expr_eq )  );      \
    }                                      \
  } while (0)


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/


#include  "utils/common.h"



/******************************************************************************
 * Func : main
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ( int argc, char *argv[] )
{
  HLH_RoundU32 zhrVal1;
  HLH_RoundU32 zhrVal2;

  UINT32 unVal1;
  UINT32 unVal2;


  (void) argc;
  (void) argv;

  EXE_ECHO (zhrVal1 = 11);
  EXE_ECHO (zhrVal2 = 5);
  EXE_ECHO (unVal1  = 11);
  EXE_ECHO (unVal2  = 5);

  EXE_CHK ( (UINT32)(zhrVal1 + zhrVal2), (UINT32)(unVal1 + unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 - zhrVal2), (UINT32)(unVal1 - unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 * zhrVal2), (UINT32)(unVal1 * unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 / zhrVal2), (UINT32)(unVal1 / unVal2) );

  EXE_CHK ( (zhrVal1 >  zhrVal2), (unVal1 >  unVal2) );
  EXE_CHK ( (zhrVal1 <  zhrVal2), (unVal1 <  unVal2) );
  EXE_CHK ( (zhrVal1 >= zhrVal2), (unVal1 >= unVal2) );
  EXE_CHK ( (zhrVal1 <= zhrVal2), (unVal1 <= unVal2) );

  EXE_ECHO (zhrVal1 = 5);
  EXE_ECHO (zhrVal2 = 11);
  EXE_ECHO (unVal1  = 5);
  EXE_ECHO (unVal2  = 11);

  EXE_CHK ( (UINT32)(zhrVal1 + zhrVal2), (UINT32)(unVal1 + unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 - zhrVal2), (UINT32)(unVal1 - unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 * zhrVal2), (UINT32)(unVal1 * unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 / zhrVal2), (UINT32)(unVal1 / unVal2) );

  EXE_CHK ( (zhrVal1 >  zhrVal2), (unVal1 >  unVal2) );
  EXE_CHK ( (zhrVal1 <  zhrVal2), (unVal1 <  unVal2) );
  EXE_CHK ( (zhrVal1 >= zhrVal2), (unVal1 >= unVal2) );
  EXE_CHK ( (zhrVal1 <= zhrVal2), (unVal1 <= unVal2) );

  EXE_ECHO (zhrVal1 = -5);
  EXE_ECHO (zhrVal2 = 11);
  EXE_ECHO (unVal1  = -5);
  EXE_ECHO (unVal2  = 11);

  EXE_CHK ( (UINT32)(zhrVal1 + zhrVal2), (UINT32)(unVal1 + unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 - zhrVal2), (UINT32)(unVal1 - unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 * zhrVal2), (UINT32)(unVal1 * unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 / zhrVal2), (UINT32)(unVal1 / unVal2) );

  EXE_CHK ( (zhrVal1 >  zhrVal2), (unVal1 >  unVal2) );
  EXE_CHK ( (zhrVal1 <  zhrVal2), (unVal1 <  unVal2) );
  EXE_CHK ( (zhrVal1 >= zhrVal2), (unVal1 >= unVal2) );
  EXE_CHK ( (zhrVal1 <= zhrVal2), (unVal1 <= unVal2) );


  EXE_ECHO (zhrVal1 = -5);
  EXE_ECHO (zhrVal2 = -5);
  EXE_ECHO (unVal1  = -5);
  EXE_ECHO (unVal2  = -5);

  EXE_CHK ( (UINT32)(zhrVal1 + zhrVal2), (UINT32)(unVal1 + unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 - zhrVal2), (UINT32)(unVal1 - unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 * zhrVal2), (UINT32)(unVal1 * unVal2) );
  EXE_CHK ( (UINT32)(zhrVal1 / zhrVal2), (UINT32)(unVal1 / unVal2) );

  EXE_CHK ( (zhrVal1 >  zhrVal2), (unVal1 >  unVal2) );
  EXE_CHK ( (zhrVal1 <  zhrVal2), (unVal1 <  unVal2) );
  EXE_CHK ( (zhrVal1 >= zhrVal2), (unVal1 >= unVal2) );
  EXE_CHK ( (zhrVal1 <= zhrVal2), (unVal1 <= unVal2) );


  HLH_DEBUG (HLH_DEBUG_MAIN, ("zhrVal1++ = %d", (UINT32)zhrVal1 ++) );
  HLH_DEBUG (HLH_DEBUG_MAIN, ("zhrVal1 = %d", (UINT32)zhrVal1 ) );
  


  return 0;
}				/* ----------  end of function main  ---------- */
