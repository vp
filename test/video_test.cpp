/*******************************************************************************
 *    File Name : test/video_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Mon 26 Oct 2009 02:32:26 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "src/common.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/
//#define VIDEO_TEST_WIDTH                  320
//#define VIDEO_TEST_HEIGHT                 240

#define VIDEO_TEST_WIDTH                  640
#define VIDEO_TEST_HEIGHT                 480

/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/
typedef struct abc 
{
	UINT8 abc;
}__attribute__ ((__packed__)) abc;


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/
static UINT8 aucVideoBuffer [VIDEO_TEST_WIDTH*VIDEO_TEST_HEIGHT*2];






/******************************************************************************
 * Desc : Functions
 ******************************************************************************/

#if 0
#define YCbCrtoR(Y,Cb,Cr) (1000 *Y + 1371*(Cr-128))/1000
#define YCbCrtoG(Y,Cb,Cr) (1000 *Y - 336*(Cb-128) - 698*(Cr-128))/1000
#define YCbCrtoB(Y,Cb,Cr) (1000 *Y + 1732*(Cb-128))/1000
#define min(x1,x2)        (((x1)<(x2))?(x1):(x2))


static __u32 Conv_YCbCr_Rgb(__u8 y0, __u8 y1, __u8 cb0, __u8 cr0)
{
  // bit order is
  // YCbCr = [Cr0 Y1 Cb0 Y0], RGB=[R1,G1,B1,R0,G0,B0].

  int r0, g0, b0, r1, g1, b1;
  __u16 rgb0, rgb1;
  __u32 rgb;

#if 1 // 4 frames/s @192MHz, 12MHz ; 6 frames/s @450MHz, 12MHz
  r0 = YCbCrtoR(y0, cb0, cr0);
  g0 = YCbCrtoG(y0, cb0, cr0);
  b0 = YCbCrtoB(y0, cb0, cr0);
  r1 = YCbCrtoR(y1, cb0, cr0);
  g1 = YCbCrtoG(y1, cb0, cr0);
  b1 = YCbCrtoB(y1, cb0, cr0);
#endif

  if (r0>255 ) r0 = 255;
  if (r0<0) r0 = 0;
  if (g0>255 ) g0 = 255;
  if (g0<0) g0 = 0;
  if (b0>255 ) b0 = 255;
  if (b0<0) b0 = 0;

  if (r1>255 ) r1 = 255;
  if (r1<0) r1 = 0;
  if (g1>255 ) g1 = 255;
  if (g1<0) g1 = 0;
  if (b1>255 ) b1 = 255;
  if (b1<0) b1 = 0;

  // 5:6:5 16bit format
  rgb0 = (((__u16)r0>>3)<<11) | (((__u16)g0>>2)<<5) | (((__u16)b0>>3)<<0);	//RGB565.
  rgb1 = (((__u16)r1>>3)<<11) | (((__u16)g1>>2)<<5) | (((__u16)b1>>3)<<0);	//RGB565.

  rgb = (rgb1<<16) | rgb0;

  return(rgb);
}



static void show_cam_img(int w, int h, void *scr, __u8 *y_buf, __u8 *cb_buf, __u8 *cr_buf)
{
  __u16 x, y;                       // , i, f;
  __u16 *fb_buf = (__u16 *)scr;
  __u32 rgb_data;
  //int palette = optional_image_format[image_format].palette;

  for(y=0; y<h; y++) {
    for(x=0; x<w; x+=2) {                 // calculate 2 times


      rgb_data = Conv_YCbCr_Rgb(y_buf[x],
          y_buf[(x)+1],
          cb_buf[(x)>>1],
          cr_buf[(x)>>1]);
      fb_buf[x]   = rgb_data;
      fb_buf[x+1] = rgb_data>>16;

    }
    fb_buf += 800;
    y_buf += w;
    cb_buf += (w)>>1;
    cr_buf += (w)>>1;
  }

}
#endif






/******************************************************************************
 * Func : main
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ()
{
  int                 nRetVal;

  HLH_Video           zhvVideo;
  HLH_FrameBuffer     zhfFB;

	HLH_ENTER_FUNC (HLH_DEBUG_MAIN);

	HLH_DEBUG (HLH_DEBUG_MAIN, ("Test Debug"));

  Printf ("******************************  Test Camera Device  ****************************\n");
  nRetVal = zhvVideo.Create (HLH_VIDEO_V4L2_DEV_CODEC, V4L2_PIX_FMT_YUV422P,
      VIDEO_TEST_WIDTH, VIDEO_TEST_HEIGHT);
  if (nRetVal < 0) {
    Printf ("Can't create video device\n");
    goto failed;
  }

  nRetVal = zhfFB.Create ("/dev/fb0");
  if (nRetVal < 0) {
    Printf ("Can't create frame buffer\n");
    goto failed;
  }

  // Main loop

  Printf ("**************************  Start to Read Camera Data  *************************\n");

	nRetVal = zhvVideo.Start ();
	if (nRetVal < 0) {
		Printf ("Can't start video device for capture\n");
	}

  do {
    // Read from video device
    nRetVal = zhvVideo.Read (aucVideoBuffer, sizeof(aucVideoBuffer));
    if (nRetVal < 0) {
      Printf ("Can't read from video device");
      goto failed;
    }

    // Draw on FrameBuffer
#if 1
    zhfFB.ShowYUV422PImage (
        aucVideoBuffer,
        aucVideoBuffer + VIDEO_TEST_WIDTH * VIDEO_TEST_HEIGHT,
        aucVideoBuffer + VIDEO_TEST_WIDTH * VIDEO_TEST_HEIGHT * 3 / 2,
        0, 0, VIDEO_TEST_WIDTH, VIDEO_TEST_HEIGHT, 0,
        0, 0 );
#else
		Printf ("before show_cam_img\n");
		show_cam_img ( VIDEO_TEST_WIDTH, VIDEO_TEST_HEIGHT,
				zhfFB.m_pusFrame,
        aucVideoBuffer,
        aucVideoBuffer + VIDEO_TEST_WIDTH * VIDEO_TEST_HEIGHT,
        aucVideoBuffer + VIDEO_TEST_WIDTH * VIDEO_TEST_HEIGHT * 3/2 );
		Printf ("after show_cam_img\n");
#endif

  } while (1);
	

	HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

  return 0;

failed:

	HLH_EXIT_FUNC (HLH_DEBUG_VIDEO);

  return -1;
}				/* ----------  end of function main  ---------- */





