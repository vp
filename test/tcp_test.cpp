/*******************************************************************************
 *    File Name : tcp_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Thu 05 Nov 2009 01:57:37 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "utils/common.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/

HLH_Thread  zhtListenThread;

HLH_Sock    zhsListenSock;
HLH_Sock    zhsConnectSock;
HLH_Sock    zhsAcceptSock;


void * ListenThreadFunc (HLH_Thread & zhtThread, void *pvNULL)
{
  int          nRetVal;
  UINT32       unPollType;
  HLH_Time     zhtTime;
  UINT8        ucChar;
  HLH_SockAddr zhsAddr;
  int          nAcceptSock;

  (void)pvNULL;

  zhtThread.ThreadStarted ();

  do {
    unPollType = HLH_SOCK_POLL_READ;
    zhtTime = HLH_Time (2, 0);
    nRetVal = zhsListenSock.PollWait (
        unPollType, zhtTime );
    if (nRetVal <= 0) {
      //HLH_DEBUG ( HLH_DEBUG_VPHONE, ("timeout and no connection") );
      goto check_stop;
    }

    // Connection established
    nRetVal = zhsListenSock.Accept (zhsAddr);
    if (nRetVal == -1) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("accept peer connection failed") );
      goto check_stop;
    }

    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("accepted") );

#if 0
    ucChar = '0';
    do {
    nAcceptSock = nRetVal;
    recv (nAcceptSock, &ucChar, sizeof(ucChar), 0);
    Printf ("%c\n", ucChar);
    } while (1);
#else
    (void)nAcceptSock;
    nRetVal = zhsAcceptSock.Create (nRetVal);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("create socket from accept failed") );
      goto check_stop;
    }

    do {
      unPollType = HLH_SOCK_POLL_READ;
      zhtTime = HLH_Time (2, 0);
      nRetVal = zhsAcceptSock.PollWait ( unPollType, zhtTime );
      if (nRetVal <= 0) {
        //HLH_DEBUG ( HLH_DEBUG_VPHONE, ("timeout and no connection") );
        goto check_stop;
      }

      nRetVal = zhsAcceptSock.Recv (&ucChar, sizeof(ucChar));
      if (nRetVal < 0) {
        HLH_DEBUG ( HLH_DEBUG_VPHONE, ("receive failed") );
        goto check_stop;
      }
      Printf ("%c\n", ucChar);
    } while (1);
#endif


check_stop:
    zhsAcceptSock.Destroy ();
    if (!zhtThread.IsStopping()) {
      HLH_DEBUG ( HLH_DEBUG_VPHONE, ("stop on request") );
      break;
    }
  } while (1);

  return NULL;

}

/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ( int argc, char *argv[] )
{
  int          nRetVal;
  int          nCnt;
  HLH_SockAddr zhsAddr;
  UINT8        ucChar;


  (void)argc;
  (void)argv;

  zhsAddr.SetAddr (INADDR_ANY, 8000);
  zhsListenSock.Create (HLH_SOCK_TYPE_STREAM, zhsAddr);
  zhsListenSock.Listen ();

  zhsAddr.SetAddr (INADDR_ANY, 8002);
  zhsConnectSock.Create (HLH_SOCK_TYPE_STREAM, zhsAddr);

  nRetVal = zhtListenThread.Start (ListenThreadFunc, NULL);
  if (nRetVal < 0) {
    HLH_DEBUG (HLH_DEBUG_MAIN, ("can't start listen thread"));
  }

  for (nCnt = 50; nCnt > 0; nCnt--) {
    zhsAddr.SetAddr ("127.0.0.1", 8000);
    nRetVal = zhsConnectSock.Connect (zhsAddr);
    if (nRetVal < 0) {
      HLH_DEBUG (HLH_DEBUG_MAIN, ("can't connect to peer"));
      HLH_Time::Wait (HLH_Time(0, 500*1000));
      continue;
    }
    break;
  }

  HLH_DEBUG (HLH_DEBUG_MAIN, ("connected"));

  ucChar = 'A';
  for (nCnt = 50; nCnt > 0; nCnt--) {
    zhsConnectSock.Send (&ucChar, sizeof(ucChar));
    if (++ucChar > 'Z') {
      ucChar = 'A';
    }
    HLH_Time::Wait (HLH_Time(0, 500*1000));
  }

  return 0;
}				/* ----------  end of function main  ---------- */

