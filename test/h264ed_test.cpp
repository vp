/*******************************************************************************
 *    File Name : h264ed_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Mon 09 Nov 2009 08:53:44 AM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "src/common.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/

//===========================  H264 Encoder Configure  =========================
#define H264ED_H264_ENC_WIDTH            320
#define H264ED_H264_ENC_HEIGHT           240
#define H264ED_H264_ENC_FRAME_RATE       15
#define H264ED_H264_ENC_BIT_RATE         600 /* kbps */
#define H264ED_H264_ENC_GOP_NUM          20



/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/
typedef struct H264EDPackage
{
  UINT32      unLen;
  UINT8       aucData[4];
};


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/

// Frame size
static UINT32                  m_unFrameSize;

//===========================  Capture  ========================================

static UINT8                   m_aucCameraBuf [H264ED_H264_ENC_WIDTH * H264ED_H264_ENC_HEIGHT * 3 / 2];

// Camera for video
static HLH_Video               m_zhvCamera;

// H264 Encoder
static H264Encoder             m_zheEncoder;


//===========================  Playback  =======================================

// H264 Decoder
static H264Decoder             m_zhdDecoder;

// Post Processor
static PostProcessor           m_zppPostProcessor;

// Framebuffer
static FrameBuffer             m_zfbFrameBuffer;



//===========================  Access  =========================================

static HLH_Mutex                      m_zhmPkgMutex;
static HLH_Cond                       m_zhcPkgCond;
static std::list<H264EDPackage*>      m_slhPkgList;







/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void * CaptureThreadFunc (
    HLH_Thread    & zhtCaptureThread,
    void          * pvNULL )
{
  int            nRetVal;
  void          *pvEncOutBuf;
  UINT32         unEncOutSize = 0;
  HLH_Time       zhtTime;
  H264EDPackage *pzhpPkg;

  (void)pvNULL;

  // Notify that this thread started
  zhtCaptureThread.ThreadStarted ();
  HLH_DEBUG ( HLH_DEBUG_TEST, ("start capture thread") );

  // Start capture
  nRetVal = m_zhvCamera.Start ();
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_TEST, ("start camera failed") );
    return NULL;
  }

  // Capture video, encode and send it.

  do {
#if 1
    // Wait for image ready
    zhtTime = HLH_TIME_FROM_USECS (200*1000);
    nRetVal = m_zhvCamera.TimedPollRead (zhtTime);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_MAIN, ("poll to read camera timeout") );
      break;
    }
#endif

    // Read camera image
    nRetVal = m_zhvCamera.Read (m_aucCameraBuf, m_unFrameSize);
    if (nRetVal < 0) {
      HLH_DEBUG ( HLH_DEBUG_MAIN, ("read camera failed") );
      break;
    }
    HLH_DEBUG ( HLH_DEBUG_TEST, ("read one frame") );

    // Encode this image
    pvEncOutBuf = m_zheEncoder.Encode (m_aucCameraBuf, m_unFrameSize, unEncOutSize);
    if (pvEncOutBuf < 0) {
      HLH_DEBUG ( HLH_DEBUG_MAIN, ("encode image failed") );
      break;
    }
    HLH_DEBUG ( HLH_DEBUG_TEST, ("encode one frame") );

    // Put into playback queue
    pzhpPkg = (H264EDPackage *) malloc (sizeof(H264EDPackage) - sizeof(pzhpPkg->aucData) + unEncOutSize);
    if (pzhpPkg == NULL) {
      HLH_DEBUG ( HLH_DEBUG_MAIN, ("allocate memory failed") );
      continue;
    }

    memcpy (pzhpPkg->aucData, pvEncOutBuf, unEncOutSize);
    pzhpPkg->unLen = unEncOutSize;

    m_zhmPkgMutex.Lock ();
    m_slhPkgList.push_back (pzhpPkg);
    m_zhcPkgCond.Signal ();
    m_zhmPkgMutex.Unlock ();

  } while ( !zhtCaptureThread.IsStopping () );

  HLH_DEBUG ( HLH_DEBUG_TEST, ("return from capture thread") );

  return NULL;

}



/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void * PlaybackThreadFunc (
    HLH_Thread    & zhtPlaybackThread,
    void          * pvNULL )
{
  HLH_Time       zhtTime;
  H264EDPackage *pzhpPkg;

  void          *pvDecOutBuf;


  (void)pvNULL;

  // Notify that this thread started
  zhtPlaybackThread.ThreadStarted ();
  HLH_DEBUG ( HLH_DEBUG_TEST, ("playback thread started") );

  do {

    m_zhmPkgMutex.Lock ();
    // Wait for package if empty
    if ( m_slhPkgList.empty () ) {
      zhtTime.SetTime (2, 0);
      m_zhcPkgCond.TimeWait (m_zhmPkgMutex, zhtTime);
      if (m_slhPkgList.empty ()) {
        m_zhmPkgMutex.Unlock ();
        goto check_out;
      }
    }

    HLH_DEBUG ( HLH_DEBUG_TEST, ("get one package") );
    // Get the package
    pzhpPkg = m_slhPkgList.front ();
    ASSERT (pzhpPkg != NULL);
    m_slhPkgList.pop_front ();

    m_zhmPkgMutex.Unlock ();

    // Decode this image
    pvDecOutBuf = m_zhdDecoder.Decode ((void*)pzhpPkg->aucData, pzhpPkg->unLen);
    if (pvDecOutBuf < 0) {
      HLH_DEBUG ( HLH_DEBUG_MAIN, ("decode failed") );
      goto failed;
    }
    HLH_DEBUG ( HLH_DEBUG_TEST, ("decode one frame") );

    // Show the image
    m_zppPostProcessor.Process ( pvDecOutBuf, m_zfbFrameBuffer.GetFrame(0) );
    HLH_DEBUG ( HLH_DEBUG_TEST, ("display one frame") );

failed:
    free (pzhpPkg);
    pzhpPkg = NULL;


check_out:;
  } while ( !zhtPlaybackThread.IsStopping () );

  HLH_DEBUG ( HLH_DEBUG_MAIN, ("return from capture thread") );

  return NULL;
}


/******************************************************************************
 * Func : main
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ( int argc, char *argv[] )
{
  int nRetVal;

  void            * pvEncOutBuf;
  UINT32            unEncOutSize = 0;
  void            * pvDecOutBuf;

  (void) argc;
  (void) argv;

  HLH_Thread        m_zhtCaptureThread;
  HLH_Thread        m_zhtPlaybackThread;


  // Calculate frame size
  m_unFrameSize = H264ED_H264_ENC_WIDTH * H264ED_H264_ENC_HEIGHT * 3 / 2;


  //===========================  Encoder  ========================================

  // Create H264 encoder
  nRetVal = m_zheEncoder.Create (
      H264ED_H264_ENC_WIDTH, H264ED_H264_ENC_HEIGHT,
      H264ED_H264_ENC_FRAME_RATE, H264ED_H264_ENC_BIT_RATE,
      H264ED_H264_ENC_GOP_NUM);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("create H264 encoder failed") );
    goto failed;
  }

  memset (m_aucCameraBuf, 0, sizeof(m_aucCameraBuf));

  pvEncOutBuf = m_zheEncoder.Encode (m_aucCameraBuf, m_unFrameSize, unEncOutSize);
  if (pvEncOutBuf == NULL) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("encode first frame failed") );
    goto failed;
  }

  //===========================  Decoder  ========================================


  // Create H264 decoder
  nRetVal = m_zhdDecoder.Create (pvEncOutBuf, unEncOutSize);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("create H264 decoder failed") );
    goto failed;
  }

	pvDecOutBuf = m_zhdDecoder.Decode (pvEncOutBuf, unEncOutSize);
	if (pvDecOutBuf == NULL) {
		HLH_DEBUG ( HLH_DEBUG_MAIN, ("decode first frame failed") );
    goto failed;
	}


  //===========================  Others for video  ===============================


  // Frame buffer
  nRetVal = m_zfbFrameBuffer.Create ("/dev/fb0");
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("create frame buffer failed") );
    goto failed;
  }

  // Post processor
  nRetVal = m_zppPostProcessor.Create ("/dev/misc/s3c-pp");
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("create post processor failed") );
    goto failed;
  }

#if 0
  nRetVal = m_zppPostProcessor.SetParam (
      H264ED_H264_ENC_WIDTH, H264ED_H264_ENC_HEIGHT,
      0, 0, H264ED_H264_ENC_WIDTH, H264ED_H264_ENC_HEIGHT, YC420,
      m_zfbFrameBuffer.GetVirtualWidth (), m_zfbFrameBuffer.GetVirtualHeight (),
      0, 0, H264ED_H264_ENC_WIDTH, H264ED_H264_ENC_HEIGHT,
      m_zfbFrameBuffer.GetColor () );
#else
  nRetVal = m_zppPostProcessor.SetParam (
      H264ED_H264_ENC_WIDTH, H264ED_H264_ENC_HEIGHT,
      0, 0, H264ED_H264_ENC_WIDTH, H264ED_H264_ENC_HEIGHT, YC420,
      m_zfbFrameBuffer.GetVirtualWidth (), m_zfbFrameBuffer.GetVirtualHeight (),
      0, 0, 480, 320,
      m_zfbFrameBuffer.GetColor () );
#endif
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("set parameter of post process failed") );
    goto failed;
  }


  // Camera for video
  nRetVal = m_zhvCamera.Create (HLH_VIDEO_V4L2_DEV_CODEC, V4L2_PIX_FMT_YUV411P,
      H264ED_H264_ENC_WIDTH, H264ED_H264_ENC_HEIGHT);
  if (nRetVal < 0) {
    HLH_DEBUG ( HLH_DEBUG_MAIN, ("create video device failed") );
    goto failed;
  }

  m_zhtPlaybackThread.Start (PlaybackThreadFunc, NULL);
  m_zhtCaptureThread.Start  (CaptureThreadFunc , NULL);

  // Wait before stop

  HLH_Time::Wait ( HLH_Time (50, 0) );


  m_zhtPlaybackThread.Stop ();
  m_zhtCaptureThread.Stop ();

	m_zheEncoder.Destroy ();
	m_zhdDecoder.Destroy ();

  return 0;

failed:
	m_zheEncoder.Destroy ();
	m_zhdDecoder.Destroy ();
  return -1;

}				/* ----------  end of function main  ---------- */
