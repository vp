/*******************************************************************************
 *    File Name : h264enc_test.c
 * 
 *       Author : Henry He
 * Created Time : Sat 31 Oct 2009 04:26:05 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/

#include  "mfc/common.h"

#include  <sys/time.h>
#include  <sys/mman.h>



/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/







unsigned int measureTime(struct timeval *start, struct timeval *stop)
{
	unsigned int sec, usec, time;

	sec = stop->tv_sec - start->tv_sec;
	if(stop->tv_usec >= start->tv_usec)
	{
		usec = stop->tv_usec - start->tv_usec;
	}
   	else
	{	
		usec = stop->tv_usec + 1000000 - start->tv_usec;
		sec--;
  	}
	time = sec*1000 + usec/1000;
	return time;
}














int Test_H264_Encoder(int argc, char **argv)
{
	int             in_fd;
	int             out_fd;
	char           *in_addr;
	int             file_size;

	int             frame_count;
	int             backup_frame_cnt;
	int             frame_size;

	int             width;
	int             height;
	int             frame_rate;
	int             bitrate;
	int             gop_num;

	void           *p_outbuf;
	UINT32          size;

	int             hdr_size;
//	int             para_change[2];
	int             frame_cnt        = 0;
	struct stat     s;

  H264Encoder     zheEncoder;


	struct timeval  start;
	struct timeval  stop;
	unsigned int    time             = 0;

  int             nRetVal;


	if (argc != 8) {
		printf("Usage : mfc <YUV file name> <output filename> <width> <height> ");
		printf("<frame rate> <bitrate> <GOP number>\n");
		return -1;
	}

	// in/out file open
	in_fd	= open(argv[1], O_RDONLY);
	out_fd	= open(argv[2], O_RDWR | O_CREAT | O_TRUNC, 0644);

	if( (in_fd < 0) || (out_fd < 0) ) {
		printf("input/output file open error\n");
		return -1;
	}

	// get input file size
	fstat(in_fd, &s);
	file_size = s.st_size;
	
	// mapping input file to memory
	in_addr = (char *)mmap(0, file_size, PROT_READ, MAP_SHARED, in_fd, 0);
	if(in_addr == NULL) {
		printf("input file memory mapping failed\n");
		return -1;
	}

  width      = atoi(argv[3]);
  height     = atoi(argv[4]);
  frame_rate = atoi(argv[5]);
  bitrate    = atoi(argv[6]);
  gop_num    = atoi(argv[7]);

	frame_size	= (width * height * 3) >> 1;
	frame_count	= file_size / frame_size;

	printf("file_size : %d, frame_size : %d, frame count : %d\n", file_size, frame_size, frame_count);
	
	
	nRetVal = zheEncoder.Create (width, height, frame_rate, bitrate, gop_num);
	if (nRetVal < 0) {
		LOG_MSG(LOG_ERROR, "Test_Encoder", "create encoder failed\n");
		return -1;
	}

	backup_frame_cnt = frame_count;
	
	while (frame_count > 0) 
	{
		printf ("frame count : %d\n", frame_count);
		
		// copy YUV data into input buffer
		gettimeofday(&start, NULL);

		p_outbuf = zheEncoder.Encode (in_addr, frame_size, size);
		if (frame_count == backup_frame_cnt) {
			zheEncoder.GetConfig(H264_ENC_GETCONF_HEADER_SIZE, &hdr_size);
			printf("Header Size : %d\n", hdr_size);
		}
		in_addr += frame_size;

		gettimeofday (&stop, NULL);
		time += measureTime (&start, &stop);

		frame_cnt++;

		write(out_fd, p_outbuf, size);
	
		frame_count--;
	}

	printf("Decoding Time : %u, Frame Count : %d, FPS : %f\n", time, frame_cnt, (float)frame_cnt*1000/time);

	zheEncoder.Destroy ();

	munmap(in_addr, file_size);
	close(in_fd);
	close(out_fd);
		
	return 0;
}




int main ( int argc, char *argv[] )
{
	Test_H264_Encoder(argc, argv);
	return 0;
}				/* ----------  end of function main  ---------- */

