/*******************************************************************************
 *    File Name : jitter_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Mon 02 Nov 2009 09:37:09 AM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "utils/common.h"
#include  "jitter/common.h"
#include  "audio/common.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/
#define AJB_PER_TIMESTAMP      1
#define AJB_JITTER_TIMESTAMP 	 (8000/160 * 1/3 * 18/10 * AJB_PER_TIMESTAMP)


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/
typedef struct __tagAudioPackage
{
  UINT32 unTimestamp;
  UINT32 unLen;
  UINT8  aucData[4];
} AudioPackage;


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/
static UINT32       g_unTimestamp = 0;
static JitterBuffer g_zjbJitterBuf;
static HLH_Audio    g_zhaAudio;


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/


/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void FreePackageData (void *pvData)
{
  free (pvData);
}

/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void OnAudioRead (void *pvNULL, void *pvBuf, UINT32 unLen)
{
  int            nRetVal;
  AudioPackage * pzapPkg;

  static UINT32 unTimestamp = 0;

  (void) pvNULL;

  pzapPkg = (AudioPackage*) malloc (sizeof(AudioPackage) -4 + unLen);
  if (pzapPkg == NULL) {
    HLH_DEBUG (HLH_DEBUG_MAIN, ("can't allocate package data"));
    return;
  } else {
    memcpy (pzapPkg->aucData, pvBuf, unLen);
    pzapPkg->unLen = unLen;
    pzapPkg->unTimestamp = ++unTimestamp;

    nRetVal = g_zjbJitterBuf.PutPkg (g_unTimestamp++, pzapPkg, FreePackageData);
    if (nRetVal < 0) {
      HLH_DEBUG (HLH_DEBUG_MAIN,
          ( "PutPkg failed (%d)", nRetVal ));
      return;
    }
  }
}

/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void OnAudioJitterBufferGet (void * pvNULL, JPackage & zjpPkg)
{
  AudioPackage *pzapPkg;

  (void) pvNULL;

  pzapPkg = (AudioPackage*) zjpPkg.m_pvData;

#if HLH_DEBUG_CONFIG_MAIN
  static UINT32           unLastTimestamp = 0;
  if (unLastTimestamp + 1 != pzapPkg->unTimestamp) {
    HLH_DEBUG ( HLH_DEBUG_VPHONE, ("jump from %u to %u", unLastTimestamp, pzapPkg->unTimestamp) );
    unLastTimestamp = pzapPkg->unTimestamp;
  } else {
    unLastTimestamp ++;
  }
#endif


  g_zhaAudio.Write (pzapPkg->aucData, pzapPkg->unLen);
}



/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ( int argc, char *argv[] )
{
  int nRetVal;

  (void) argc;
  (void) argv;

  g_unTimestamp = 0;

  // Create audio jitter buffer
  nRetVal = g_zjbJitterBuf.Create ( g_unTimestamp,
      AJB_PER_TIMESTAMP,
      AJB_JITTER_TIMESTAMP,
      OnAudioJitterBufferGet, NULL);
  if (nRetVal < 0) {
    HLH_DEBUG (HLH_DEBUG_MAIN,
        ("can't create audio jitter buffer"));
  }

  // Create audio device
  g_zhaAudio.Create (OnAudioRead, NULL);
  if (nRetVal < 0) {
    HLH_DEBUG (HLH_DEBUG_MAIN,
        ("can't creat audio device"));
  }

  // Wait for 50s
  HLH_Time::Wait ( HLH_Time(50, 0) );

  // Stop jitter buffer and audio device
  g_zjbJitterBuf.Destroy ();
  g_zhaAudio.Destroy ();
	
	return 0;
}				/* ----------  end of function main  ---------- */
