/*******************************************************************************
 *    File Name : pp_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Sat 31 Oct 2009 02:53:48 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <linux/vt.h>
#include <poll.h>

#include "../demo/s3c_pp.h"



/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/


// #define USING_PP_RESERVED_MEMORY

#define SRC_IMAGE_FILE_NAME_1 "./flower_rgb16.raw"
#define SRC_IMAGE_FILE_NAME_2 "./virus_rgb16.raw"
#define SRC_IMAGE_WIDTH  640
#define SRC_IMAGE_HEIGHT 480
#define SRC_IMAGE_COLOR  RGB16


#define PP_DEVICE_FILE_NAME	"/dev/misc/s3c-pp"

#define LCD_WIDTH 	800
#define LCD_HEIGHT 	480

#define FB_NODE0   	"/dev/fb0"
#define FB0_BPP     16

static int fb_fp0 = -1;

/*------------ OSD configuration ----------------------*/
typedef struct {
  int bpp;
  int lefttop_x;
  int lefttop_y;
  int width;
  int height;
}  s3c_win_info_t;

typedef struct
{
  unsigned int map_dma_f1;
  unsigned int map_dma_f2;
} s3c_fb_dma_info_t;

static s3c_win_info_t    fb_info_to_driver;
static s3c_fb_dma_info_t fb_dma_info0;

#define SET_OSD_START		_IO('F', 201)
#define SET_OSD_STOP		_IO('F', 202)
#define SET_OSD_INFO		_IOW('F', 209, s3c_win_info_t)
#define S3C_FB_GET_INFO		_IOR('F', 307, s3c_fb_dma_info_t)

static inline int fb_init(s3c_fb_dma_info_t *dmainfo, const char *devName) 
{
  int dev_fp = -1;

  dev_fp = open(devName, O_RDWR);

  if (dev_fp < 0)
    return -1;

  ioctl(dev_fp, S3C_FB_GET_INFO, dmainfo);

  return dev_fp;
}

static int osd_init(int fd, int bpp, int topx, int topy, int width, int height)
{
  fb_info_to_driver.bpp = bpp;
  fb_info_to_driver.lefttop_x = topx;
  fb_info_to_driver.lefttop_y = topy;
  fb_info_to_driver.width = width;
  fb_info_to_driver.height = height;

  if(ioctl(fd, SET_OSD_INFO, &fb_info_to_driver))
  {
    printf("Some problem with SET_VS_INFO\n");
    return -1;
  }

  if(ioctl(fd, SET_OSD_START))
  {
    printf("Some problem with SET_OSD_START\n");
    perror("ioctl()");
    return -1;
  }

  return 0;
}

int file_open_and_mmap ( const char *file_name, char **mmap_addr, uint *size )
{
  int     in_fd;


  struct stat	s;

  in_fd = open(file_name, O_RDONLY);
  if (in_fd < 0) {
    printf("input file open error\n");
    return -1;
  }

  // get input file size
  fstat(in_fd, &s);
  *size = s.st_size;

  // mapping input file to memory
  *mmap_addr = (char *) mmap(0, *size, PROT_READ, MAP_SHARED, in_fd, 0);
  if(mmap_addr == NULL) {
    printf("input file memory mapping failed\n");
    return -1;
  }

  return in_fd;
}

int main (int argc, char **argv)
{
  int		pp_dev_fd, in_fd, buf_size;
  uint    file_size;
  char    *in_addr;

  struct {
    char *ptr;
    unsigned int phy_addr;
  } pp_src_buf[2];

  s3c_pp_params_t	pp_param;

  struct pollfd test_fd;
  int i;
  int delay_time_msec;

  s3c_pp_mem_alloc_t alloc_info[2];

  if ( argc != 3 )
  {
    printf ( "Check number of arguments!!!\n" );
    printf ( "Usage: #./post_test out_path delay_time\n" );
    printf ( "  - out_path:   0 (DMA_ONESHOT), 1 (FIFO_FREERUN)\n" );
    printf ( "  - delay_time: unit is milisecond.\n" );
    return -1;
  }

  delay_time_msec = atoi ( argv[2] ) * 1000;

  // open frame buffer 0
  if ((fb_fp0 = fb_init(&fb_dma_info0, FB_NODE0)) < 0) 
  {
    printf("fb0_init failed\n");
    return -1;
  }   

  osd_init(fb_fp0, FB0_BPP, 0, 0, LCD_WIDTH, LCD_HEIGHT );


  // open post processor 
  pp_dev_fd = open ( PP_DEVICE_FILE_NAME, O_RDWR|O_NONBLOCK );
  if(pp_dev_fd < 0)
  {
    printf("Post processor open error\n");
    return -1;
  }

  // set post processor configuration
  pp_param.src_full_width	    = SRC_IMAGE_WIDTH;
  pp_param.src_full_height	= SRC_IMAGE_HEIGHT;
  pp_param.src_start_x		= 0;
  pp_param.src_start_y		= 0;
  pp_param.src_width		    = pp_param.src_full_width;
  pp_param.src_height		    = pp_param.src_full_height;
  pp_param.src_color_space	= SRC_IMAGE_COLOR;

  pp_param.dst_full_width	    = LCD_WIDTH;
  pp_param.dst_full_height	= LCD_HEIGHT;
  pp_param.dst_start_x		= 0;
  pp_param.dst_start_y		= 0;	
//  pp_param.dst_width		    = LCD_WIDTH;
//  pp_param.dst_height		    = LCD_HEIGHT;
  pp_param.dst_width		    = SRC_IMAGE_WIDTH;
  pp_param.dst_height		    = SRC_IMAGE_HEIGHT;
  pp_param.dst_color_space    = RGB16;

  pp_param.out_path			= (s3c_pp_run_mode_t) atoi(argv[1]);   // mode: DMA_ONESHOT, FIFO_FREERUN
  pp_param.scan_mode          = PROGRESSIVE_MODE;

  ioctl(pp_dev_fd, S3C_PP_SET_PARAMS, &pp_param);



  // open first file
  in_fd = file_open_and_mmap ( SRC_IMAGE_FILE_NAME_1, &in_addr, &file_size );

#ifdef USING_PP_RESERVED_MEMORY   // using PP reserved memory
  buf_size = ioctl(pp_dev_fd, S3C_PP_GET_RESERVED_MEM_SIZE);
  printf ( "pp reserved memory size = %d\n", buf_size );

  pp_src_buf[0].ptr = (char *) mmap(0, buf_size, PROT_READ | PROT_WRITE, MAP_SHARED, pp_dev_fd, 0);
  if (pp_src_buf[0].ptr == NULL) 
  {
    printf("Post processor mmap failed\n");
    return -1;
  }

  pp_src_buf[0].phy_addr = ioctl(pp_dev_fd, S3C_PP_GET_RESERVED_MEM_ADDR_PHY);

#else // use kernel memory
  buf_size = ioctl(pp_dev_fd, S3C_PP_GET_SRC_BUF_SIZE);

  alloc_info[0].size = buf_size;

  if ( -1 == ioctl(pp_dev_fd, S3C_PP_ALLOC_KMEM, &alloc_info[0]) )
  {
    printf("ioctl S3C_PP_ALLOC_KMEM failed\n");
    return -1;
  }

  pp_src_buf[0].ptr       = (char *) alloc_info[0].vir_addr;
  pp_src_buf[0].phy_addr  = alloc_info[0].phy_addr;
#endif 

  memcpy(pp_src_buf[0].ptr, in_addr, file_size); 

  close(in_fd);




  // open second file
  in_fd = file_open_and_mmap ( SRC_IMAGE_FILE_NAME_2, &in_addr, &file_size );

#ifdef USING_PP_RESERVED_MEMORY
  pp_src_buf[1].ptr = pp_src_buf[0].ptr + file_size;           // alloc after first file
  pp_src_buf[1].phy_addr = pp_src_buf[0].phy_addr + file_size; // alloc after first file
#else
  buf_size = ioctl(pp_dev_fd, S3C_PP_GET_SRC_BUF_SIZE);
  alloc_info[1].size = buf_size;

  if ( -1 == ioctl(pp_dev_fd, S3C_PP_ALLOC_KMEM, &alloc_info[1]) )
  {
    printf("Post processor mmap failed\n");
    return -1;
  }

  pp_src_buf[1].ptr       = (char *) alloc_info[1].vir_addr;
  pp_src_buf[1].phy_addr  = alloc_info[1].phy_addr;
#endif 

  memcpy(pp_src_buf[1].ptr, in_addr, file_size); 

  close(in_fd);

  pp_param.src_buf_addr_phy = pp_src_buf[0].phy_addr;
  ioctl(pp_dev_fd, S3C_PP_SET_SRC_BUF_ADDR_PHY, &pp_param);

  if ( DMA_ONESHOT == pp_param.out_path )
  {
    pp_param.dst_buf_addr_phy = fb_dma_info0.map_dma_f1;
    ioctl(pp_dev_fd, S3C_PP_SET_DST_BUF_ADDR_PHY, &pp_param);

    for ( i = 0; i < 100; i++ )
    {
      test_fd.fd = pp_dev_fd;
      test_fd.events = POLLOUT|POLLERR;

      poll(&test_fd, 1, 3000);

      ioctl(pp_dev_fd, S3C_PP_START);

      usleep(delay_time_msec);

      pp_param.src_buf_addr_phy = pp_src_buf[(i+1)%2].phy_addr;

      ioctl(pp_dev_fd, S3C_PP_SET_SRC_BUF_ADDR_PHY, &pp_param);
    }
  }
  else
  {
    ioctl(pp_dev_fd, S3C_PP_START);

    for ( i = 0; i < 100; i++ )
    {
      pp_param.src_next_buf_addr_phy = pp_src_buf[(i+1)%2].phy_addr;
      ioctl(pp_dev_fd, S3C_PP_SET_SRC_BUF_NEXT_ADDR_PHY, &pp_param);

      usleep(delay_time_msec);
    }
  }

#ifndef USING_PP_RESERVED_MEMORY
  if ( -1 == ioctl(pp_dev_fd, S3C_PP_FREE_KMEM, &alloc_info[0]) )
  {
    printf("Post processor mmap failed\n");
    return -1;
  }

  if ( -1 == ioctl(pp_dev_fd, S3C_PP_FREE_KMEM, &alloc_info[1]) )
  {
    printf("Post processor mmap failed\n");
    return -1;
  }
#endif

  osd_init(fb_fp0, FB0_BPP, 0, 0, LCD_WIDTH, LCD_HEIGHT);

  close(pp_dev_fd);
  close(fb_fp0);

  return 0;
}
