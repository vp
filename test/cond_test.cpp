/*******************************************************************************
 *    File Name : test/cond_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Fri 23 Oct 2009 01:20:16 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include "src/common.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/
static HLH_Cond    s_zhcCond;
static HLH_Mutex   s_zhmMutex;






/*******************************************************************************
 * Desc : Functions
 ******************************************************************************/






/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void * MyThreadFunc (HLH_Thread &zhtThread, void *pvParam)
{
  UINT32    * punCount;

  s_zhcCond.Init ();

  zhtThread.ThreadStarted ();

  Printf ("Enter  %s\n", __FUNCTION__);

  punCount = (UINT32 *) pvParam;

  do {

    // Wait for a while
    HLH_Time::Wait ( HLH_Time (2, 0) );


    // Lock this condition
    s_zhmMutex.Lock ();

    // Count down
    if ( *punCount != 0 ) {
      *punCount -= 1;
    }

    // Signal MyThreadFunc2
    s_zhcCond.Signal ();
    Printf ("Signal MyThreadFunc2\n");

    s_zhmMutex.Unlock ();


    if ( zhtThread.IsStopping () ) {
      Printf ("Exit   %s requested by stop\n", __FUNCTION__);
      return NULL;
    }

  } while (1);

}


/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void * MyThreadFunc2 (HLH_Thread &zhtThread, void *pvParam)
{
  UINT32     *punCount;
 
  ASSERT (pvParam != NULL);

  punCount = (UINT32 *) pvParam;

  zhtThread.ThreadStarted ();

  Printf ("Enter  %s\n", __FUNCTION__);

  do {

    // Lock this condition
    s_zhmMutex.Lock ();

    // Wait for condition until timeout
    s_zhcCond.TimeWait ( s_zhmMutex, HLH_Time (2, 0) );

    // Check if exit request
    if (*punCount == 0) {
      // Unlock this condition
      s_zhmMutex.Unlock ();
      Printf ("Exit   %s requested by condition\n", __FUNCTION__);
      return NULL;
    }

    // Unlock this condition
    s_zhmMutex.Unlock ();

    if ( zhtThread.IsStopping () ) {
      Printf ("Exit   %s requested by stop\n", __FUNCTION__);
      return NULL;
    }

  } while (1);

}


/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ()
{
  HLH_Thread    zhtThread1;
  HLH_Thread    zhtThread2;

  UINT32        unCounter;

  unCounter = 5;

  zhtThread1.Start (MyThreadFunc, (void*)&unCounter);
  zhtThread2.Start (MyThreadFunc2, (void*)&unCounter);

  HLH_Time::Wait ( HLH_Time (12, 0) );

  zhtThread1.Stop ();
  zhtThread2.Stop ();

  Printf ("Exit main\n");

  return 0;
}


