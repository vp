/*******************************************************************************
 *    File Name : vphone_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Thu 05 Nov 2009 10:41:13 AM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include  "utils/common.h"
#include  "vphone/common.h"
#include  <signal.h>


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/
static VPhone zvpPhone;






/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ( int argc, char *argv[] )
{
  int     nRetVal;
  char    acCmd [1024];

  bool bAudioSend = false;
  bool bAudioReceive = false;
  bool bVideoSend = false;
  bool bVideoReceive = false;

	(void)argc;
	(void)argv;

  HLH_SockAddr zhsAddr;

  signal (SIGPIPE, SIG_IGN);

  if (argc != 1) {
    Printf ("Usage: %s\n");
  }

  do {

    Printf ("Wait for command:");
    scanf ("%s", acCmd);
    if ( !strncmp (acCmd, "q", 1) ) {
      // Quit
      Printf ("Quit\n");
      zvpPhone.Stop ();
      return 0;
    } else if ( !strncmp (acCmd, "c", 1) ) {
#if 0
      // Call start
      scanf ("%s", acCmd);
      nRetVal = zhsAddr.SetAddrIP (acCmd);
      if (nRetVal < 0) {
        Printf ("Invalidate IP\n");
        continue;
      }
#endif
      scanf ("%s", acCmd);
      if ( !strncmp (acCmd, "0", 1) ) {
				zhsAddr.SetAddrIP ("172.22.60.220");
      } else {
				zhsAddr.SetAddrIP ("172.22.60.225");
      }

      scanf ("%s", acCmd);
      if ( !strncmp (acCmd, "0", 1) ) {
        bAudioReceive = false;
      } else {
        bAudioReceive = true;
      }

      scanf ("%s", acCmd);
      if ( !strncmp (acCmd, "0", 1) ) {
        bAudioSend = false;
      } else {
        bAudioSend = true;
      }

      scanf ("%s", acCmd);
      if ( !strncmp (acCmd, "0", 1) ) {
        bVideoReceive = false;
      } else {
        bVideoReceive = true;
      }

      scanf ("%s", acCmd);
      if ( !strncmp (acCmd, "0", 1) ) {
        bVideoSend = false;
      } else {
        bVideoSend = true;
      }

      Printf ("start vphone\n");
      nRetVal = zvpPhone.Start (zhsAddr, 0, bAudioReceive, bAudioSend, bVideoReceive, bVideoSend);
      if (nRetVal < 0) {
        Printf ("start vphone failed\n");
      }

    } else if ( !strncmp (acCmd, "s", 1) ) {
      // Call stop
      Printf ("Stop the call\n");
      zvpPhone.Stop ();
    } else {
      // Unknown command
      Printf ("Unknown command\n");
    }
  } while (1);

	
	return 0;
}				/* ----------  end of function main  ---------- */
