/*******************************************************************************
 *    File Name : sock_test.cpp
 * 
 *       Author : Henry He
 * Created Time : Tue 03 Nov 2009 04:30:29 PM CST
 *  Description : 
 ******************************************************************************/


/*******************************************************************************
 * Desc : Includes Files
 ******************************************************************************/
#include	"utils/common.h"


/*******************************************************************************
 * Desc : Macro Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Type Definations
 ******************************************************************************/


/*******************************************************************************
 * Desc : Global Variables
 ******************************************************************************/


/*******************************************************************************
 * Desc : File Variables
 ******************************************************************************/


static char message [] = "0 - abcdefgh";


static HLH_UDPSock			zhuSock0;
static HLH_UDPSock			zhuSock1;

static HLH_Thread				zhtSendThread0;
static HLH_Thread				zhtSendThread1;


/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void OnUdpReceive (void *pvNULL,
		void *pvBuf, UINT32 unLen, HLH_SockAddr &zhsSockAddr)
{
	(void)pvNULL;
	(void)zhsSockAddr;

	Printf ("OnUdpReceive: receive %d bytes:\n", unLen);

	unLen = unLen > sizeof(message) ? sizeof(message) : unLen;
	((char*)pvBuf) [unLen] = '\0';

	Printf ("        [ %s ]\n", pvBuf);
}

/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void * SendThread0 (HLH_Thread &zhtThread, void *pvNULL)
{
	HLH_DEBUG ( HLH_DEBUG_MAIN, ("SendThread0 started") );
	zhtThread.ThreadStarted ();
	(void)pvNULL;
	do {
		HLH_Time::Wait ( HLH_Time(0, 500*1000) );
		message [0] = '0';
		zhuSock0.Send ( message, sizeof(message) );
	} while ( !zhtThread.IsStopping() );

	return NULL;
}

/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
void * SendThread1 (HLH_Thread &zhtThread, void *pvNULL)
{
	HLH_DEBUG ( HLH_DEBUG_MAIN, ("SendThread1 started") );
	zhtThread.ThreadStarted ();
	(void)pvNULL;
	do {
		HLH_Time::Wait ( HLH_Time(0, 700*1000) );
		message [0] = '1';
		zhuSock0.Send ( message, sizeof(message) );
	} while ( !zhtThread.IsStopping() );

	return NULL;
}


/******************************************************************************
 * Func : 
 * Desc : 
 * Args : 
 * Outs : 
 ******************************************************************************/
int main ( int argc, char *argv[] )
{
	HLH_SockAddr		zhsSockAddr;

	(void)argc;
	(void)argv;

	HLH_ENTER_FUNC (HLH_DEBUG_MAIN);

	zhsSockAddr.SetAddr (INADDR_ANY, 8000);
	zhuSock0.Create (zhsSockAddr);
	zhsSockAddr.SetAddr ("127.0.0.1", 8002);
	zhuSock0.Connect (zhsSockAddr);
	zhuSock0.AddRecvHandler (OnUdpReceive, NULL);

	zhsSockAddr.SetAddr (INADDR_ANY, 8002);
	zhuSock1.Create (zhsSockAddr);
	zhsSockAddr.SetAddr ("127.0.0.1", 8000);
	zhuSock1.Connect (zhsSockAddr);
	zhuSock1.AddRecvHandler (OnUdpReceive, NULL);

	zhtSendThread0.Start (SendThread0, NULL);
	zhtSendThread1.Start (SendThread1, NULL);

	HLH_Time::Wait (HLH_Time(50, 0));

	zhtSendThread0.Stop ();
	zhtSendThread1.Stop ();

	zhuSock0.Destroy ();
	zhuSock1.Destroy ();

	return 0;
}
